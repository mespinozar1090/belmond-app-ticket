//
//  GrupoTicketsViewController.swift
//  ticketvirtual
//
//  Created by usuario on 4/15/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class GrupoTicketsViewController: UIViewController {
    
    @IBOutlet weak var tblItems:UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var txtNombreGrupo: UILabel!
    var indexSelect:Int = 0
    var oGrupo:Grupo?
    
    var objectsArray: Results<Ticket>!
    var dataSearchArray: Results<Ticket>!
    var realm:Realm!
    var ticketInvalidos:Array<String>?
    
    //contador
    var cantidadTicket:Int = 0
    var contador:Int = 0
    
    lazy var refresh: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(cgColor: #colorLiteral(red: 0.5450980392, green: 0.4509803922, blue: 0.3294117647, alpha: 1))
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        hideKeyboardWhenTappedAround()
        setupNavigationBarItem()
        txtNombreGrupo.text = oGrupo?.nombre
        
        obtenerTickets()
        customView()
        
    }
    
    func customView(){
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .gray
        searchBar.delegate = self
        searchBar.endEditing(true)
        searchBar.placeholder = BelmondUtil.validarIdioma() ? "Nombre del pasajero" : "Passenger's name"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        let text = searchBar.value(forKey: "searchField") as? UITextField
        text?.textColor = .gray
        text?.font = UIFont.systemFont(ofSize: 14.0)
        headerView.clipsToBounds = true
        headerView.layer.cornerRadius = 20
        headerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        tblItems.refreshControl = refresh
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func requestData(){
        let deadLine = DispatchTime.now() + .milliseconds(2000)
        DispatchQueue.main.asyncAfter(deadline: deadLine){
            self.obtenerTickets()
            self.refresh.endRefreshing()
        }
    }
    
    private func obtenerTickets(){
        contador = 0
        objectsArray = realm.objects(Ticket.self).filter("grupo == %@",oGrupo!.id)
        print("objectsArray x : \(objectsArray.count) contador")
        dataSearchArray = objectsArray
        if(objectsArray.count > 0){
            cantidadTicket = objectsArray.count
            print("cantidadTicket x : \(cantidadTicket) contador")
            for index in 0...objectsArray.count - 1 {
                validarTicket(numeroTicket: objectsArray[index].numeroTicket!, indexPath: index)
            }
        }
    }
    
    private func actualizarTicketValidar(){
        objectsArray = realm.objects(Ticket.self).filter("grupo == %@",oGrupo!.id)
        dataSearchArray = objectsArray
        self.refreshTable()
    }
    
    private func validarTicket(numeroTicket:String, indexPath:Int){
        let ticketViewModel = TicketViewModel.init(parent: self)
        ticketViewModel.validarTicket(numeroTicket: numeroTicket, callback: {(success,data,error) in
            if(success!){
                self.contador = self.contador + 1
                switch data?.estadoTicket {
                case Constante.MODIFICADO:
                    self.actualizarTicket(numeroTicket: data!.numeroTicket!, indexPath: indexPath)
                case Constante.INVALIDO:
                    guard let oTicket = self.realm.objects(Ticket.self).filter("numeroTicket == %@",numeroTicket).filter("grupo == %@",self.oGrupo!.id).first else {
                        return
                    }
                    
                    if let rutaArchivoOnboarding = oTicket.rutaArchivoOnboarding,rutaArchivoOnboarding != ""{
                        BelmondUtil.eliminarDocumentoFile(documentoPath: rutaArchivoOnboarding)
                    }
                    
                    if let rutaArchivoDocumentoElectronico = oTicket.rutaArchivoDocumentoElectronico,rutaArchivoDocumentoElectronico != ""{
                        BelmondUtil.eliminarDocumentoFile(documentoPath: rutaArchivoDocumentoElectronico)
                    }
                    
                    try! self.realm.write{
                        self.realm.delete(oTicket)
                    }
                    
                case Constante.VALIDO:
                    print("Ticket valido : ",data!.numeroTicket!)
                default:
                    print("default")
                }
                
                if(self.cantidadTicket == self.contador){
                    print("entroo??")
                    self.actualizarTicketValidar()
                }
            }
        })
    }
    
    private func actualizarTicket(numeroTicket:String,indexPath:Int){
        let ticketViewModel = TicketViewModel.init(parent: self)
        ticketViewModel.buscarPorTicket(numeroTicket: numeroTicket, callback: {(success,data,error) in
            if(success!){
                if let item = self.realm.objects(Ticket.self).filter("numeroTicket == %@",numeroTicket).filter("grupo == %@",self.oGrupo!.id).first {
                    
                    if let rutaArchivoOnboarding = item.rutaArchivoOnboarding{
                        BelmondUtil.eliminarDocumentoFile(documentoPath: rutaArchivoOnboarding)
                    }
                    
                    if let rutaArchivoDocumentoElectronico = item.rutaArchivoDocumentoElectronico{
                        BelmondUtil.eliminarDocumentoFile(documentoPath: rutaArchivoDocumentoElectronico)
                    }
                    
                    try! self.realm.write{
                        self.realm.delete(item)
                        self.actualizarTicketValidar()
                    }
                    
                    // creamos nuevo ticket
                    var oTicket = Ticket()
                    for index in 0...data!.count - 1 {
                        let response = TicketResponse()
                        response.grupo = self.oGrupo!.id
                        response.nombrePasajero = data![index].nombrePasajero
                        response.numeroTicket = data![index].numeroTicket
                        response.ruta = data![index].ruta
                        response.tipoTren = data![index].tipoTren
                        response.vagon = data![index].vagon
                        response.numeroAsiento = data![index].numeroAsiento
                        response.fechaSalida = data![index].fechaSalida
                        response.horaSalida = data![index].horaSalida
                        response.codigoHash =  data![index].codigoHash
                        response.esModifi = data![index].esModifi
                        oTicket = BelmondUtil.castTicketReponseToTicket(oticket: response)
                        
                        try! self.realm.write{
                            self.realm.add(oTicket)
                        }
                        
                        if let rutaArchivoOnboarding = data![index].rutaArchivoOnboarding,rutaArchivoOnboarding != "" {
                            self.downloadFileOnboarding(urlPDF: rutaArchivoOnboarding, numeroTicket: data![index].numeroTicket!)
                        }
                        
                        if let rutaArchivoDocumentoElectronico = data![index].rutaArchivoDocumentoElectronico,rutaArchivoDocumentoElectronico != "" {
                            self.downloadFileOnboarding(urlPDF: rutaArchivoDocumentoElectronico, numeroTicket: data![index].numeroTicket!)
                        }
                    }
                }
            }
        })
    }
    
    func downloadFileOnboarding(urlPDF:String,numeroTicket:String){
        let nombreOnboarding = "Onboarding-grupo\(self.oGrupo!.id)-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreOnboarding)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in }.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download Onboarding : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",self.oGrupo!.id).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoOnboarding = nombreOnboarding
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento onboarding :",nombreOnboarding)
            }
        }
    }
    
    func downloadFileDocumentoContable(urlPDF:String,numeroTicket:String){
        let nombreDocumentoContable = "Documento-grupo\(self.oGrupo!.id)-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreDocumentoContable)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in}.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download documento contantable : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",self.oGrupo!.id).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoDocumentoElectronico = nombreDocumentoContable
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento Contable :",nombreDocumentoContable)
            }
        }
    }
    
    func setupNavigationBarItem(){
        let title:String = BelmondUtil.validarIdioma() ? "Agregar" : "Add"
        let consultaButton =  UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(buscarTickets))
        consultaButton.tintColor = .white
        navigationItem.rightBarButtonItem = consultaButton
    }
    
    @objc func buscarTickets(){
        self.performSegue(withIdentifier: "sgBuscarController", sender: self)
    }
    
    private func refreshTable(){
        DispatchQueue.main.async {
            self.tblItems.reloadData()
        }
    }
    
    @objc func accionDetalle(sender: UIButton) {
        let index = sender.tag
        indexSelect = index
        self.performSegue(withIdentifier: "sgDetalleTicketGrupo", sender: self)
    }
    
    @objc func accionEliminar(sender: UIButton) {
        let index = sender.tag
        showAlertEliminar(index: index)
    }
    
    func showAlertEliminar(index:Int){
    }
    
    @objc func accionQR(sender: UIButton) {
        let index = sender.tag
        indexSelect = index
        self.performSegue(withIdentifier: "sqCodigoQR2", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgBuscarController") {
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! BuscarViewController
            destino.ticketSuccessDelegate = self
            destino.oGrupo = oGrupo
        }else if(segue.identifier == "sgDetalleTicketGrupo"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! DetalleTicketViewController
            // let destination = segue.destination as! DetalleTicketViewController
            destination.ticketReponse = BelmondUtil.castTicketToTicketReponse(oticket:objectsArray[indexSelect])
        }else if(segue.identifier == "sqCodigoQR2"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! VisorQRViewController
            destination.codigoHash = objectsArray[indexSelect].codigoHash
        }else if(segue.identifier == "sgModalnvalido"){
            let destination = segue.destination as! TicketInvalidoViewController
            destination.dataItem = ticketInvalidos
        }
    }
}

extension GrupoTicketsViewController:TicketSuccessDelegate{
    func success(success: Bool) {
        if(success){
            actualizarTicketValidar()
        }
    }
    
    func sendData(array: Array<String>) {
        
        if (array.count > 0){
            ticketInvalidos = array
            print("ticket malotes \(array)")
            self.performSegue(withIdentifier: "sgModalnvalido", sender: self)
        }else{
            print("No tiene boletos invalidos")
        }
    }
}

//MARK: - configuracion table
extension GrupoTicketsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GrupoTicketsTableViewCell", for: indexPath) as! GrupoTicketsTableViewCell
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                cell.lblRuta.text = self.objectsArray[indexPath.row].ruta
                cell.lblPasajero.text = self.objectsArray[indexPath.row].nombrePasajero
                cell.lblNroTicket.text = self.objectsArray[indexPath.row].numeroTicket
                cell.lblFecha.text = self.objectsArray[indexPath.row].fechaSalida
                cell.lblHora.text = self.objectsArray[indexPath.row].horaSalida
                
                if let codigoHash = self.objectsArray[indexPath.row].codigoHash,codigoHash != "" {
                    cell.cardQR.isHidden = false
                    cell.imgQr.image = BelmondUtil.generateQRCode(from: self.objectsArray[indexPath.row].codigoHash!)
                }else{
                    cell.cardQR.isHidden = true
                }
            }
        }
        
        cell.btnDetalle.tag = indexPath.row
        cell.btnDetalle.addTarget(self, action: #selector(accionDetalle(sender:)), for: .touchUpInside)
        cell.btnEliminar.tag = indexPath.row
        cell.btnEliminar.addTarget(self, action: #selector(accionEliminar(sender:)), for: .touchUpInside)
        cell.btnQR.tag = indexPath.row
        cell.btnQR.addTarget(self, action: #selector(accionQR(sender:)), for: .touchUpInside)
        
        if let estado = objectsArray[indexPath.row].esModifi, estado == Constante.ISMODIFICADO {
            cell.lblUpdate.textColor = UIColor(cgColor: #colorLiteral(red: 0.5450980392, green: 0.4509803922, blue: 0.3294117647, alpha: 1))
        }else{
            cell.lblUpdate.text = ""
        }
        return cell
    }
}

//MARK: - configuracion searchBar
extension GrupoTicketsViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            guard let dataSearch = dataSearchArray else{
                return
            }
            objectsArray = dataSearch
            tblItems.reloadData()
            return
        }
        
        var text:String
        text = searchBar.text ?? ""
        guard let dataSearch = dataSearchArray else{
            return
        }
        objectsArray = dataSearch.filter("nombrePasajero contains[c] %@","\(text)")
        tblItems.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

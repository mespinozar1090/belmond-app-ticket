//
//  HomeViewController.swift
//  ticketvirtual
//
//  Created by usuario on 3/17/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var imgViajes: UIImageView!
    @IBOutlet weak var imgGrupo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        autenticacion();
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true,animated:true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }
    

    private func setupView(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let tapGestureRecognizerGrupo = UITapGestureRecognizer(target: self, action: #selector(imageTappedGrupo))
        imgViajes.isUserInteractionEnabled = true
        imgViajes.addGestureRecognizer(tapGestureRecognizer)
        imgGrupo.isUserInteractionEnabled = true
        imgGrupo.addGestureRecognizer(tapGestureRecognizerGrupo)
    }
    
    @objc func imageTapped(){
        self.performSegue(withIdentifier: "sgMisTickets", sender: nil)
    }
    
    @objc func imageTappedGrupo(){
        self.performSegue(withIdentifier: "sgGrupos", sender: nil)
    }
    
    private func setNavigationBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    private func setupNavigationBarItem(){
        self.title = "Belmond"
        let infoImage = UIImage(named: "ico_grupo")
        let button:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 20, height: 20))
        button.setBackgroundImage(infoImage, for: .normal)
        button.addTarget(self, action: #selector(acctionShowGrupo), for: UIControl.Event.touchUpInside)
        
        let editButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = editButton
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = BelmondUtil.hexStringToUIColor(hex: "011435")
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.tintColor = UIColor.white
    }
    
    @objc func acctionShowGrupo() -> Void {
        self.performSegue(withIdentifier: "sgGrupos", sender: nil)
    }
    
    
    private func autenticacion(){
        let ticketViewModel = TicketViewModel.init(parent: self)
        ticketViewModel.autenticacion(uid: "", callback:{(success,data,error) in
            if(success!){
                print("tokenJwt :",data ?? "")
            }else{
                print("Ocurrio un error al generar token")
            }
        })
    }
}

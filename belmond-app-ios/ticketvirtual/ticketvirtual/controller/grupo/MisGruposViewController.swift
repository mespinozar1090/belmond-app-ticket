//
//  MisGruposViewController.swift
//  ticketvirtual
//
//  Created by usuario on 4/15/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import RealmSwift

class MisGruposViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var tblGrupo:UITableView!
    @IBOutlet weak var cardheader: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var indexSelect:Int = 0
    var objectsArray:Results<Grupo>!
    var dataSearchArray : Results<Grupo>!
    
    var realm:Realm!
    var ticketArray: Results<Ticket>!
    var deteleTicket:Array<Ticket> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        realm = try! Realm()
        setupNavigationBarItem()
        
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .gray
        searchBar.delegate = self
        searchBar.endEditing(true)
        searchBar.placeholder = BelmondUtil.validarIdioma() ? "Busca nombre del grupo" : "Search group name"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        let text = searchBar.value(forKey: "searchField") as? UITextField
        text?.textColor = .gray
        text?.font = UIFont.systemFont(ofSize: 14.0)
        
        cardheader.clipsToBounds = true
        cardheader.layer.cornerRadius = 20
        cardheader.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        objectsArray = realm.objects(Grupo.self)
        dataSearchArray = objectsArray;
        refreshTable()
    }
    
    func setupNavigationBarItem(){
        let title:String = BelmondUtil.validarIdioma() ? "Crear" : "Create"
        let consultaButton =  UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(addNewGrupo))
        consultaButton.tintColor = .white
        
        self.navigationItem.rightBarButtonItem = consultaButton
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        self.navigationController?.navigationBar.barTintColor = UIColor(#colorLiteral(red: 0.04705882353, green: 0.1098039216, blue: 0.231372549, alpha: 1))
    }
    
    @objc func addNewGrupo(){
        showCrearGrupo()
    }
    
    private func refreshTable(){
        DispatchQueue.main.async {
            self.tblGrupo.reloadData()
        }
    }
    
    private func showCrearGrupo(){
        var nombreGrupo:String?
        let textTitle:String = BelmondUtil.validarIdioma() ? "Mi grupo" : "My Group"
        let textMessage:String = BelmondUtil.validarIdioma() ? "Ingrese fecha y un nombre para este grupo" : "Enter a date and a name for this group"
        let crearText = BelmondUtil.validarIdioma() ? "Crear" : "Create"
        let cancelarText = BelmondUtil.validarIdioma() ? "Cancelar" : "Cancel"
        
        let alert = UIAlertController(title: textTitle, message: textMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelarText, style: .destructive, handler: { (action) -> Void in}))
        let saveAction = UIAlertAction(title:crearText, style: .default, handler: { (action) -> Void in
            
            let grupos = self.realm.objects(Grupo.self).count
            let contador = grupos + 1
            let item = Grupo()
            item.id = contador
            item.nombre = nombreGrupo
            
            try! self.realm.write{
                self.realm.add(item)
                self.refreshTable()
            }
        })
        saveAction.isEnabled = false
        alert.addAction(saveAction)
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = BelmondUtil.validarIdioma() ? "Ingrese dd/mm/aaaa y nombre" : "Insert mm/dd/yyyy and name"
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                let currentString: NSString = textField.text! as NSString
                if(!self.validarCampo(enteredEmail: textField.text ?? "")){
                    nombreGrupo = textField.text!
                    saveAction.isEnabled = currentString.length  > 0
                    textField.textColor = BelmondUtil.hexStringToUIColor(hex: "000000")
                }else{
                    print("Contiene caractener especiales")
                    saveAction.isEnabled = false
                    textField.textColor = UIColor.red
                }
            }
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func validarCampo(enteredEmail:String) -> Bool {
        let emailFormat = ".*[^A-Za-z0-9/\\s].*"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
}

extension MisGruposViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GrupoTableViewCell", for: indexPath) as! GrupoTableViewCell
        cell.lblNombre.text = objectsArray[indexPath.row].nombre
        cell.btnDetalle.tag = indexPath.row
        cell.btnDetalle.addTarget(self, action: #selector(accionDetalle(sender:)), for: .touchUpInside)
        cell.btnEliminar.tag = objectsArray[indexPath.row].id
        cell.btnEliminar.addTarget(self, action: #selector(accionEliminar(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func accionDetalle(sender: UIButton) {
        let index = sender.tag
        indexSelect = index
        self.performSegue(withIdentifier: "sgDetalleGrupo", sender: self)
        
    }
    
    @objc func accionEliminar(sender: UIButton) {
        let idGrupo = sender.tag
        guard let grupoRealm = self.realm.objects(Grupo.self).filter("id == %@",idGrupo).first else {
            print("Ocurrio un error")
            return
        }
        
        let textTitle:String = BelmondUtil.validarIdioma() ? "Eliminar" : "Delete"
        let textMessage:String =  BelmondUtil.validarIdioma() ? "¿Está seguro de eliminar este grupo?" : "Are you sure you want to delete this group?"
        let textGrupo:String = BelmondUtil.validarIdioma() ? "Grupo" : "Group"
        let siguienteText = BelmondUtil.validarIdioma() ? "Eliminar" : "Delete"
        let cancelarText = BelmondUtil.validarIdioma() ? "Cancelar" : "Cancel"
        
        let message:String = "\(textMessage) \n \(textGrupo) : \(grupoRealm.nombre ?? "")"
        let alert = UIAlertController(title: textTitle, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: siguienteText, style: .default, handler: { (action) -> Void in
            let objectTicket = self.realm.objects(Ticket.self).filter("grupo == %@",grupoRealm.id)
            let grupoRealm = self.realm.objects(Grupo.self).filter("id == %@",grupoRealm.id)
            if(objectTicket.count>0){
                for index in 0...objectTicket.count-1{
                    if let rutaArchivoDocumentoElectronico = objectTicket[index].rutaArchivoDocumentoElectronico, rutaArchivoDocumentoElectronico != "" {
                        BelmondUtil.eliminarDocumentoFile(documentoPath:rutaArchivoDocumentoElectronico)
                    }
                    
                    if let rutaArchivoOnboarding = objectTicket[index].rutaArchivoOnboarding, rutaArchivoOnboarding != "" {
                        BelmondUtil.eliminarDocumentoFile(documentoPath:rutaArchivoOnboarding)
                    }
                }
            }
            
            try! self.realm.write{
                self.realm.delete(objectTicket)
                self.realm.delete(grupoRealm)
            }
            self.refreshTable()
        })
        alert.addAction(ok)
        alert.addAction(UIAlertAction(title: cancelarText, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgDetalleGrupo"){
            let destination = segue.destination as! GrupoTicketsViewController
            destination.oGrupo = objectsArray[indexSelect]
        }
    }
}

extension MisGruposViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            guard let dataSearch = dataSearchArray else {
                return
            }
            objectsArray = dataSearch
            tblGrupo.reloadData()
            return
        }
        
        var text:String
        text = searchBar.text ?? ""
        guard let dataSearch = dataSearchArray else {
            return
        }
        objectsArray = dataSearch.filter("nombre contains[c] %@","\(text)")
        tblGrupo.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}


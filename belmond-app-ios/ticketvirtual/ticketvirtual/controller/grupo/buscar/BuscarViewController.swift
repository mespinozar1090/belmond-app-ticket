//
//  BuscarViewController.swift
//  ticketvirtual
//
//  Created by usuario on 4/15/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

protocol TicketSuccessDelegate {
    func success(success:Bool)
    func sendData(array:Array<String>)
}

class BuscarViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var buscarPorDocumentoView: UIView!
    @IBOutlet weak var buscarPorTicketView: UIView!
    
    var oGrupo:Grupo?
    var ticketSuccessDelegate:TicketSuccessDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupNavigationBarItem()
        segmentedControl.selectedSegmentIndex = 0
        buscarPorDocumentoView.alpha = 1
        buscarPorTicketView.alpha = 0
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        self.navigationController?.navigationBar.barTintColor = UIColor(#colorLiteral(red: 0.04705882353, green: 0.1098039216, blue: 0.231372549, alpha: 1))
        //segmentedControl.tintColor = .gray
        // Do any additional setup after loading the view.
    }
    
    @IBAction func showController(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            buscarPorDocumentoView.alpha = 1
            buscarPorTicketView.alpha = 0
        case 1:
            buscarPorDocumentoView.alpha = 0
            buscarPorTicketView.alpha = 1
        default:
            print("Default")
        }
    }
    
    private func setupNavigationBarItem(){
       let txtCancel:String = BelmondUtil.validarIdioma() ? "Cancelar" : "Cancel"
        let cancelButton2 =  UIBarButtonItem(title: txtCancel, style: .plain, target: self, action: #selector(cancelButton))
        cancelButton2.tintColor = .white
        navigationItem.leftBarButtonItem = cancelButton2
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        navigationController!.navigationBar.isTranslucent = true
    }
    
    @objc func cancelButton(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "sgContainerTipoDocumento") {
            let destination = segue.destination as! BuscarPorDocumentoViewController
            destination.ticketGrupoDelegate = self
            destination.oGrupo = oGrupo
        }else if(segue.identifier == "sgContainerTicket"){
            let destination = segue.destination as! BuscarPorTicketViewController
            destination.delegate = self
            destination.oGrupo = oGrupo
        }
    }
    
    
}

extension BuscarViewController: TicketGrupoDelegate{
    func updateData(success: Bool) {
        if(success){
            ticketSuccessDelegate?.success(success: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showModal(show: Bool) {
        if(show){
            BelmondUtil.showActivityIndicatorV2(uiView: (self.view), title:BelmondUtil.validarIdioma() ? "validando...":"validating...")
        }else{
            BelmondUtil.hideActivityIndicator(uiView: (self.view)!)
        }
    }
}

extension BuscarViewController : BuscarTicketNumeroDelegate{
    func sendData(array: Array<String>) {
        ticketSuccessDelegate?.sendData(array: array)
    }
    
    func showModalTicket(show: Bool) {
        if(show){
            BelmondUtil.showActivityIndicatorV2(uiView: (self.view), title:BelmondUtil.validarIdioma() ? "validando...":"validating...")
        }else{
            BelmondUtil.hideActivityIndicator(uiView: (self.view)!)
        }
    }
    
    func updateDataTicket(success: Bool) {
        if(success){
            ticketSuccessDelegate?.success(success: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

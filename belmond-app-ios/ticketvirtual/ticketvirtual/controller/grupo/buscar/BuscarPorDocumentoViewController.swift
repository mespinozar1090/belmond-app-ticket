//
//  BuscarPorDocumentoViewController.swift
//  ticketvirtual
//
//  Created by usuario on 4/15/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

protocol TicketGrupoDelegate{
    func showModal(show:Bool)
    func updateData(success:Bool)
}

class BuscarPorDocumentoViewController: UIViewController {
    
    @IBOutlet weak var btnTipoDocumento: ButtonWithImage!
    @IBOutlet weak var btnNacionalidad: ButtonWithImage!
    @IBOutlet weak var txtNroDocumento: UITextField!
    
    var ticketGrupoDelegate:TicketGrupoDelegate?
    
    var idTipoDocumento:String?
    var idNacionalidad:String?
    var urlDocumentoPdf:String?
    
    var oGrupo:Grupo?
    var realm : Realm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        realm = try! Realm()
        setupButton(button: btnNacionalidad)
        setupButton(button: btnTipoDocumento)
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    @IBAction func accionNacionalidad(_ sender: Any) {
        self.performSegue(withIdentifier: "sgNacionalidad2", sender: self)
    }
    
    @IBAction func accionTipoDocumento(_ sender: UIButton) {
        self.performSegue(withIdentifier: "sgTipoDocumento2", sender: self)
    }
    
    @IBAction func accionConsultar(_ sender: UIButton) {
        if(validarCampos()){
            ticketGrupoDelegate?.showModal(show: true)
            let params:NSDictionary = [
                "codPais": idNacionalidad!, "tipDoc": idTipoDocumento!,"docIde":txtNroDocumento.text!]
            
            let ticketViewModel = TicketViewModel.init(parent: self)
            ticketViewModel.buscarPorDocumento(params: params, callback: {(succces,data,code,error) in
                if(succces!){
                    if let data = data {
                        for index in 0...data.count-1{
                            if let object = self.realm.objects(Ticket.self).filter("numeroTicket = %@", data[index].numeroTicket! as String).filter("grupo == %@",self.oGrupo!.id).first {
                                print("Ticket ya exite en el grupo : \(object.numeroTicket! as String)")
                            }else{
                                let ticket = Ticket()
                                ticket.grupo = self.oGrupo!.id
                                ticket.nombrePasajero = data[index].nombrePasajero
                                ticket.numeroTicket =  data[index].numeroTicket
                                ticket.ruta =  data[index].ruta
                                ticket.tipoTren =  data[index].tipoTren
                                ticket.vagon =  data[index].vagon
                                ticket.numeroAsiento = data[index].numeroAsiento
                                ticket.fechaSalida = data[index].fechaSalida
                                ticket.horaSalida = data[index].horaSalida
                                ticket.codigoHash = data[index].codigoHash
                                ticket.esModifi = data[index].esModifi
                                try! self.realm.write{
                                    self.realm.add(ticket)
                                }
                                
                                if let rutaArchivoOnboarding = data[index].rutaArchivoOnboarding,rutaArchivoOnboarding != ""{
                                    self.downloadFileOnboarding(urlPDF: rutaArchivoOnboarding, numeroTicket: data[index].numeroTicket!)
                                }
                                
                                if let rutaArchivoDocumentoElectronico = data[index].rutaArchivoDocumentoElectronico,rutaArchivoDocumentoElectronico != ""{
                                    self.downloadFileDocumentoContable(urlPDF: rutaArchivoDocumentoElectronico, numeroTicket: data[index].numeroTicket!)
                                }
                            }
                        }
                    }
                    self.ticketGrupoDelegate?.updateData(success: true)
                    self.ticketGrupoDelegate?.showModal(show: false)
                }else{
                    self.ticketGrupoDelegate?.showModal(show: false)
                    let title:String = BelmondUtil.validarIdioma() ? "Información" : "Information"
                    let message:String = BelmondUtil.validarIdioma() ? "Documento inválido" : "Invalid Document"
                    let aceptar:String = BelmondUtil.validarIdioma() ? "Aceptar" : "Done"
                    let alert = BelmondUtil.getAlert(title: title, message: message, titleAction: aceptar)
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
    
    private func validarCampos()-> Bool{
        
        let txtTitle:String = BelmondUtil.validarIdioma() ? "Información" : "Information"
        let txtNacionalidad:String = BelmondUtil.validarIdioma() ? "Seleccionar Nacionalidad" : "Select Nationality"
        let txtTipoDocumento:String = BelmondUtil.validarIdioma() ? "Seleccionar tipo de documento" : "Select document type"
        let txtCaracterEspecial:String = BelmondUtil.validarIdioma() ? "No se permite caracteres especiales" : "Special characters are not allowed"
        let txtAceptar:String = BelmondUtil.validarIdioma() ? "Aceptar" : "Done"
        let txtCampoObligatorio:String = BelmondUtil.validarIdioma() ? "Campo obligatorio" : "Obligatory field"
        
        if(nil != idTipoDocumento && !idTipoDocumento!.isEmpty){
            if(!txtNroDocumento.text!.isEmpty){
                if(!validarCampoRegex(enteredEmail: txtNroDocumento.text!)){
                    if(nil != idNacionalidad && !idNacionalidad!.isEmpty){
                        BelmondUtil.tfClearError(field: txtNroDocumento)
                        return true
                    }else{
                        let alert = BelmondUtil.getAlert(title: txtTitle, message: txtNacionalidad, titleAction: txtAceptar)
                        self.present(alert,animated: true,completion: nil)
                    }
                }else{
                    let alert = BelmondUtil.getAlert(title: txtTitle, message: txtCaracterEspecial, titleAction: txtAceptar)
                    self.present(alert,animated: true,completion: nil)
                }
            }else{
                BelmondUtil.tfSetError(field: txtNroDocumento, placeholder:txtCampoObligatorio)
            }
        }else{
            let alert = BelmondUtil.getAlert(title: txtTitle, message: txtTipoDocumento, titleAction: txtAceptar)
            self.present(alert,animated: true,completion: nil)
        }
        return false
    }
    
    func validarCampoRegex(enteredEmail:String) -> Bool {
        let emailFormat = ".*[^A-Za-z0-9].*"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    func downloadFileDocumentoContable(urlPDF:String,numeroTicket:String){
        let nombreDocumentoContable = "Documento-grupo\(self.oGrupo!.id)-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreDocumentoContable)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in}.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download documento contantable : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",self.oGrupo!.id).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoDocumentoElectronico = nombreDocumentoContable
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento Contable :",nombreDocumentoContable)
            }
        }
    }
    
    func downloadFileOnboarding(urlPDF:String,numeroTicket:String){
        let nombreOnboarding = "Onboarding-grupo\(self.oGrupo!.id)-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreOnboarding)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in }.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download Onboarding : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",self.oGrupo!.id).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoOnboarding = nombreOnboarding
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento onboarding :",nombreOnboarding)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgTipoDocumento2") {
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! TipoDocumentoViewController
            destino.tipoDocumentoDelegate = self
        }else if(segue.identifier == "sgNacionalidad2") {
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! NacionalidadViewController
            destino.nacionalidaDelegate = self
        }
    }
    
}

extension BuscarPorDocumentoViewController : TipoDocumentoDelegate,NacionalidadDelegate{
    func setTipoDocumento(object: TipoDocumento) {
        btnTipoDocumento.setTitle(object.nombre, for: .normal)
        idTipoDocumento = object.id
    }
    
    func setNacionalidad(object: Nacionalidad) {
        btnNacionalidad.setTitle(object.nombre, for: .normal)
        idNacionalidad = object.id
    }
}

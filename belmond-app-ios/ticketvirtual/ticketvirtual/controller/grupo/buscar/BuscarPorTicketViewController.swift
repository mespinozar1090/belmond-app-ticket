//
//  BuscarPorTicketViewController.swift
//  ticketvirtual
//
//  Created by usuario on 4/15/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

protocol BuscarTicketNumeroDelegate {
    func showModalTicket(show:Bool)
    func updateDataTicket(success:Bool)
    func sendData(array:Array<String>)
}

class BuscarPorTicketViewController: UIViewController {
    
    @IBOutlet weak var tblItem:UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    
    var objectArray:Array<String> = ["1"]
    var ticketArray:Array<String> = []
    var ticketsObservados:Array<String> = []
    
    let xPos : CGFloat = 20
    var yPos : CGFloat = 0
    var contador:Int = 0
    var caracterEspecial:Int = 0
    var contadorTickets:Int = 0
    var boletoNovalido: String = BelmondUtil.validarIdioma() ? "Boleto incorrecto" : "Wrong ticket number"
    var boletoRegistrado: String = BelmondUtil.validarIdioma() ? "Boleto ya registrado" : "Ticket already entered"
    var txtCaracterEspecial:String = BelmondUtil.validarIdioma() ? "No se permite caracteres especiales" : "Special characters are not allowed"
    let txtTitle:String = BelmondUtil.validarIdioma() ? "Información" : "Information"
    let txtAceptar:String = BelmondUtil.validarIdioma() ? "Aceptar" : "Done"
    
    var realm:Realm!
    var oGrupo:Grupo?
    var delegate:BuscarTicketNumeroDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        realm = try! Realm()
        btnAdd.layer.cornerRadius = btnAdd.frame.width/2
    }
    
    
    @IBAction func btnAdd(_ sender: UIButton) {
        let txtCampoObligatorio:String = BelmondUtil.validarIdioma() ? "Campo obligatorio" : "Obligatory field"
        contador = 0
        caracterEspecial = 0
        ticketArray = []
        for index in 0...objectArray.count-1{
            let indexPath = IndexPath(row: index, section: 0)
            let cell: BuscarPortTickeTableViewCell = self.tblItem.cellForRow(at: indexPath) as! BuscarPortTickeTableViewCell
            
            if let nroTicket = cell.tfNroTicket.text, !nroTicket.isEmpty {
                 ticketArray.append(nroTicket)
            }else{
                contador = contador + 1
                BelmondUtil.tfSetError(field: cell.tfNroTicket, placeholder:txtCampoObligatorio)
            }
            
            if let nroTicket = cell.tfNroTicket.text, validarNumeroAndFactura(enteredEmail: nroTicket) {
                caracterEspecial = caracterEspecial + 1
                contador = contador + 1
                BelmondUtil.tfSetError(field: cell.tfNroTicket, placeholder:txtCaracterEspecial)
            }
        }
        
        if(caracterEspecial > 0){
            let alert = BelmondUtil.getAlert(title: txtTitle, message: txtCaracterEspecial, titleAction: txtAceptar)
            self.present(alert,animated: true,completion: nil)
        }
        
        if(contador == 0){
            consultarTicket(ticket: ticketArray)
        }
    }

    func valiarCaracterFactura(searchTerm:String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-")
        if searchTerm.rangeOfCharacter(from: characterset.inverted) != nil {
            print("string contains special characters")
            return false
        }else{
            print("string np contains special characters")
        }
        return true
    }
    
    func validarNumeroAndFactura(enteredEmail:String) -> Bool {
           let emailFormat = ".*[^A-Za-z0-9\\-].*"
           let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
           return emailPredicate.evaluate(with: enteredEmail)
    }
    
    func validarCampo(enteredEmail:String) -> Bool {
           let emailFormat = ".*[^A-Za-z0-9].*"
           let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
           return emailPredicate.evaluate(with: enteredEmail)
    }
    
    private func consultarTicket(ticket:Array<String>){
        delegate?.showModalTicket(show: true)
        
        for index in 0...ticket.count-1{
            if(valiarCaracterFactura(searchTerm: ticket[index])){
                var nroTicket  = ticket[index]
                print("numero sin split \(nroTicket)")
                if(nroTicket.contains("/")){
                  nroTicket = BelmondUtil.splitString(nombre: nroTicket)
                }
                 print("numero con split \(nroTicket)")
                let ticketViewModel = TicketViewModel.init(parent: self)
                ticketViewModel.buscarPorTicket(numeroTicket: nroTicket, callback: {(success,data,error) in
                    self.contadorTickets = self.contadorTickets + 1
                    if(success!){
                        if let data = data{
                            for index in 0...data.count - 1 {
                                
                                if let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", data[index].numeroTicket! as String).filter("grupo == %@",self.oGrupo!.id).first {
                                    print("ticket ya se encuentra registrado en el grupo : \(tickets.numeroTicket ?? "")")
                                    self.ticketsObservados.append("\(data[index].numeroTicket!) : \(self.boletoRegistrado)")
                                }else{
                                    var oTicket = Ticket()
                                    let response = TicketResponse()
                                    response.grupo = self.oGrupo!.id
                                    response.nombrePasajero = data[index].nombrePasajero
                                    response.numeroTicket = data[index].numeroTicket
                                    response.ruta = data[index].ruta
                                    response.tipoTren = data[index].tipoTren
                                    response.vagon = data[index].vagon
                                    response.numeroAsiento = data[index].numeroAsiento
                                    response.fechaSalida = data[index].fechaSalida
                                    response.horaSalida = data[index].horaSalida
                                    response.codigoHash = data[index].codigoHash
                                    response.esModifi = data[index].esModifi
                                    oTicket = BelmondUtil.castTicketReponseToTicket(oticket: response)
                                    
                                    try! self.realm.write{
                                        self.realm.add(oTicket)
                                    }
                                    
                                    if let rutaArchivoOnboarding = data[index].rutaArchivoOnboarding,rutaArchivoOnboarding != ""{
                                        print("rutaArchivoOnboarding : \(rutaArchivoOnboarding)")
                                        self.downloadFileOnboarding(urlPDF: rutaArchivoOnboarding, numeroTicket: data[index].numeroTicket!)
                                    }
                                    
                                    if let rutaArchivoDocumentoElectronico = data[index].rutaArchivoDocumentoElectronico,rutaArchivoDocumentoElectronico != ""{
                                        print("rutaArchivoDocumentoElectronico : \(rutaArchivoDocumentoElectronico)")
                                        self.downloadFileDocumentoContable(urlPDF: rutaArchivoDocumentoElectronico, numeroTicket: data[index].numeroTicket!)
                                    }
                                }
                            }
                        }
                    }else{
                        print("ticket no valido : ",ticket[index])
                        self.ticketsObservados.append("\(ticket[index]) : \(self.boletoNovalido)")
                    }
                    if(self.contadorTickets ==  ticket.count){
                        print("Termino de contar los tickets")
                        self.delegate?.showModalTicket(show: false)
                        self.delegate?.updateDataTicket(success: true)
                        self.delegate?.sendData(array: self.ticketsObservados)
                    }
                })
            }else{
                self.contadorTickets = self.contadorTickets + 1
                if(self.contadorTickets ==  ticket.count){
                    print("Termino de contar los tickets")
                    self.delegate?.showModalTicket(show: false)
                    self.delegate?.updateDataTicket(success: true)
                    self.delegate?.sendData(array: self.ticketsObservados)
                }
                print("tiene caracteres especiales")
            }
        }
    }
    
    func downloadFileOnboarding(urlPDF:String,numeroTicket:String){
        let nombreOnboarding = "Onboarding-grupo\(self.oGrupo!.id)-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreOnboarding)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in }.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download Onboarding : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",self.oGrupo!.id).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoOnboarding = nombreOnboarding
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento onboarding :",nombreOnboarding)
            }
        }
    }
    
    func downloadFileDocumentoContable(urlPDF:String,numeroTicket:String){
        let nombreDocumentoContable = "Documento-grupo\(self.oGrupo!.id)-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreDocumentoContable)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in}.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download documento contantable : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",self.oGrupo!.id).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoDocumentoElectronico = nombreDocumentoContable
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento Contable :",nombreDocumentoContable)
            }
        }
    }
    
    @IBAction func accionAdd(_ sender: UIButton) {
        objectArray.append("1")
        let indexPath = IndexPath(row: objectArray.count-1, section: 0)
        tblItem.beginUpdates()
        tblItem.insertRows(at: [indexPath], with: .automatic)
        tblItem.endUpdates()
        view.endEditing(true)
    }
    
}

extension BuscarPorTicketViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let txtNumberTicket:String = BelmondUtil.validarIdioma() ? "Inserte el número de boleto" : "Enter the ticket number"
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuscarPortTickeTableViewCell", for: indexPath) as! BuscarPortTickeTableViewCell
        cell.configure(text: "", placeholder: txtNumberTicket)
        cell.btnDelete.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            objectArray.remove(at: indexPath.row)
            tblItem.beginUpdates()
            tblItem.deleteRows(at: [indexPath], with: .automatic)
            tblItem.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

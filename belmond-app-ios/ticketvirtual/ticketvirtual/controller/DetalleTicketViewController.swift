//
//  DetalleTicketViewController.swift
//  ticketvirtual
//
//  Created by usuario on 3/19/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class DetalleTicketViewController: UIViewController {
    
    
    @IBOutlet weak var btnQR: UIButton!
    @IBOutlet weak var cardQR: UIView!
    @IBOutlet weak var cardtwo: CardViewOne!
    @IBOutlet weak var cardone: CardViewOne!
    @IBOutlet weak var cardRadius: UIView!
    @IBOutlet weak var lblpasajero: UILabel!
    @IBOutlet weak var lblRuta: UILabel!
    @IBOutlet weak var imgQr: UIImageView!
    @IBOutlet weak var lblNroTicket: UILabel!
    @IBOutlet weak var lblTipoTren: UILabel!
    @IBOutlet weak var lblFechaSalida: UILabel!
    @IBOutlet weak var lblHoraSalida: UILabel!
    @IBOutlet weak var lblVagon: UILabel!
    @IBOutlet weak var lblAsiendo: UILabel!
    
    var ticketReponse:TicketResponse?
    var urlDocumentoPdf:String?
    var titleDocumento:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar();
        setupView()
        updateData()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private func setNavigationBar(){
        title = BelmondUtil.validarIdioma() ? "Información" : "Information"
        
        let title:String = BelmondUtil.validarIdioma() ? "Atrás" : "Back"
        let searchButton =  UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(cancelarAccion))
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
       self.navigationItem.leftBarButtonItem = searchButton
    }
    
    @objc func cancelarAccion(){
        dismiss(animated: true, completion: nil)
    }
    
    private func setupView(){
        cardtwo.layer.cornerRadius = 5
        cardtwo.layer.borderWidth = 1
        cardtwo.layer.borderColor = UIColor.gray.cgColor
        cardtwo.clipsToBounds = true
        
        cardone.layer.cornerRadius = 5
        cardone.layer.borderWidth = 1
        cardone.layer.borderColor = UIColor.gray.cgColor
        cardone.clipsToBounds = true
    }
    
    private func updateData(){
        if let  ticketReponse = ticketReponse {
            lblpasajero.text = ticketReponse.nombrePasajero
            lblRuta.text = ticketReponse.ruta
            lblNroTicket.text = ticketReponse.numeroTicket
            lblTipoTren.text = ticketReponse.tipoTren
            lblFechaSalida.text = ticketReponse.fechaSalida
            lblHoraSalida.text = ticketReponse.horaSalida
            lblVagon.text = ticketReponse.vagon
            lblAsiendo.text = ticketReponse.numeroAsiento
            
            if let codigoHash = ticketReponse.codigoHash, codigoHash != ""{
                imgQr.image = BelmondUtil.generateQRCode(from: codigoHash)
                imgQr.isHidden = false
            }else{
                cardQR.isHidden = true
            }
        }
    }
    
    @IBAction func showQR(_ sender: UIButton) {
        //self.performSegue(withIdentifier: "sqCodigoQR3", sender: self)
    }
    
    @IBAction func showDocumento(_ sender: ButtonRadius) {
        if let urlDocumento = ticketReponse?.rutaArchivoDocumentoElectronico,urlDocumento != "" {
            urlDocumentoPdf = BelmondUtil.getDirectory()+"/"+ticketReponse!.rutaArchivoDocumentoElectronico!
            titleDocumento = BelmondUtil.validarIdioma() ? "Comprobante de pago" : "Payment receipt"
            self.performSegue(withIdentifier: "sgVisorPdf", sender: nil)
        }else{
            let titulo = BelmondUtil.validarIdioma() ? "Información" : "Information"
            let mensaje = BelmondUtil.validarIdioma() ? "Si su boleto fue emitido antes del 01/01/2020, no es necesaria la factura. Encontrará el monto de la compra en su boleto de viaje" : "If your ticket was issue before  01/01/2020, the bill is not necessary. Find your payment amount in your boarding ticket"
            let titleAction = BelmondUtil.validarIdioma() ? "Aceptar" : "Done"
            let alert = BelmondUtil.getAlert(title: titulo, message: mensaje, titleAction: titleAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func showBoarding(_ sender: ButtonRadius) {
        
        if let urlDocumento = ticketReponse?.rutaArchivoOnboarding, urlDocumento != ""{
            urlDocumentoPdf = BelmondUtil.getDirectory()+"/"+ticketReponse!.rutaArchivoOnboarding!
            titleDocumento = BelmondUtil.validarIdioma() ? "Boleto" : "Ticket"
            self.performSegue(withIdentifier: "sgVisorPdf", sender: nil)
        }else{
            let title:String = BelmondUtil.validarIdioma() ? "No se encontró Tarjeta de embarque" : "No boarding pass found"
            let message:String = BelmondUtil.validarIdioma() ? "Valida tu boleto en un Punto de Venta" : "Approach to a Point of Sale to validate your ticket"
            let titleAction = BelmondUtil.validarIdioma() ? "Aceptar" : "Done"
            let alert = BelmondUtil.getAlert(title: title, message: message, titleAction: titleAction)
            self.present(alert,animated: true,completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgVisorPdf" ){
            let destination = segue.destination as! VisorPdfViewController
            destination.urlDocumentoPdf = urlDocumentoPdf
            destination.textTitle = titleDocumento
            //  self.performSegue(withIdentifier: "sgVisorPdf", sender: nil)
        }else if(segue.identifier == "sqCodigoQR2"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! VisorQRViewController
            destination.codigoHash = ticketReponse?.codigoHash
        }
    }
}

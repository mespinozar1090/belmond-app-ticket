//
//  BienvenidoViewController.swift
//  ticketvirtual
//
//  Created by usuario on 3/19/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class BienvenidoViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnContinuar(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialVC = storyboard.instantiateViewController(withIdentifier: "MyTabBarViewController") as! MyTabBarViewController
        self.present(initialVC, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

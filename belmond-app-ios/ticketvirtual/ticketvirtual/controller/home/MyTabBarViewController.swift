//
//  MyTabBarViewController.swift
//  ticketvirtual
//
//  Created by usuario on 3/17/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class MyTabBarViewController: UITabBarController,UITabBarControllerDelegate  {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        initTabBar()
    }
    
    func initTabBar(){
    
       let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        let homeIcon = UITabBarItem(title: BelmondUtil.validarIdioma() ? "Inicio" : "Home", image: UIImage(named: "ico_home"), selectedImage: UIImage(named: "ico_home"))
        homeVC!.tabBarItem =  homeIcon
        
       let grupoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MisGruposViewController") as? MisGruposViewController
      let grupoIcon = UITabBarItem(title: BelmondUtil.validarIdioma() ? "Agencias" : "Agency", image: UIImage(named: "ico_agency"), selectedImage: UIImage(named: "ico_agency"))
       grupoVC!.tabBarItem =  grupoIcon
        
     let destinoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MisTicketsViewController") as? MisTicketsViewController
        let destinoIcon = UITabBarItem(title: BelmondUtil.validarIdioma() ? "Pasajero" : "Passenger", image: UIImage(named: "ico_people"), selectedImage: UIImage(named: "ico_people"))
       destinoVC!.tabBarItem =  destinoIcon
        
        let homeController = UINavigationController(rootViewController: homeVC!)
        let grupoController = UINavigationController(rootViewController: grupoVC!)
        let destinoController = UINavigationController(rootViewController: destinoVC!)
        
        self.viewControllers = [homeController,grupoController,destinoController]
    }

}

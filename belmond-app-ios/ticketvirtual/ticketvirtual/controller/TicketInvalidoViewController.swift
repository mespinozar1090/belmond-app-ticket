//
//  TicketInvalidoViewController.swift
//  ticketvirtual
//
//  Created by usuario on 8/16/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class TicketInvalidoViewController: UIViewController {

    @IBOutlet weak var tblItem: UITableView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var btnCerrar: ButtonRadius!
    
    var dataItem:Array<String>?
    override func viewDidLoad() {
        super.viewDidLoad()
        //btnCerrar.setTitle(title, for: .normal)
        cardView.clipsToBounds = true
        cardView.layer.cornerRadius = 10
        cardView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMinYCorner]
    }

    @IBAction func accionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)
        //navigationController?.popViewController(animated: true)
    }
}

extension TicketInvalidoViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItem!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TicketInvalidoTableViewCell", for: indexPath) as! TicketInvalidoTableViewCell
       
        if let nombre = dataItem?[indexPath.row]{
            if(nombre.contains("Boleto incorrecto") || nombre.contains("Wrong ticket number")){
                cell.txtNombre.textColor = .red
            }
            cell.txtNombre.text = nombre
        }
        return cell
    }
}

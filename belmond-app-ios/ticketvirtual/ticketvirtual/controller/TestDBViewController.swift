//
//  TestDBViewController.swift
//  ticketvirtual
//
//  Created by usuario on 3/19/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import RealmSwift

class TestDBViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblItem:UITableView!
    
    var addButton : UIBarButtonItem!
    
    var realm : Realm!
    
    var  objectsArray :Results<Item>{
        get{
            return realm.objects(Item.self)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realm = try! Realm()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup(){
        view.backgroundColor = .white
        navigationItem.title = "Ticket"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        addButton = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(addNewItem))
        navigationItem.rightBarButtonItem = addButton
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    
    @objc func addNewItem(){
        let alertVC = UIAlertController(title: "Add New Item", message: "What do you want yo do?", preferredStyle: .alert)
        alertVC.addTextField(configurationHandler: nil)
        let cancelAccion = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        let addAccion = UIAlertAction(title: "Add", style: .default) {(UIAlertAction) in
            let myTextField = (alertVC.textFields?.first)! as UITextField
            
            let item = Item()
            item.name = myTextField.text!
            
            try! self.realm.write{
                self.realm.add(item)
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
            
        }
        
        alertVC.addAction(cancelAccion)
        alertVC.addAction(addAccion)
        present(alertVC,animated: true,completion: nil)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
        let item = objectsArray[indexPath.row]
        cell.txtNomre.text = item.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let item = objectsArray[indexPath.row]
            try! self.realm.write{
                self.realm.delete(item)
            }
        }
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

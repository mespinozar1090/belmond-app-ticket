//
//  VisorPdfViewController.swift
//  ticketvirtual
//
//  Created by usuario on 4/9/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import PDFKit

class VisorPdfViewController: UIViewController {
    
    @IBOutlet weak var pdfView:PDFView!
    
    var urlDocumentoPdf:String?
    var textTitle:String?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
    
    }
    
    private func setupNavigationBarItem(){
          self.title = textTitle
          let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
          navigationController?.navigationBar.titleTextAttributes = textAttributes
          navigationController?.navigationBar.barTintColor = BelmondUtil.hexStringToUIColor(hex: "011435")
          navigationController?.navigationBar.isTranslucent = false
          navigationController?.view.tintColor = UIColor(cgColor: #colorLiteral(red: 0.04705882353, green: 0.1098039216, blue: 0.231372549, alpha: 1))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(urlDocumentoPdf != ""){
            if let pdfDocument = PDFDocument(url: URL(fileURLWithPath: urlDocumentoPdf!)) {
                    pdfView.displayMode = .singlePageContinuous
                    pdfView.autoScales = true
                    pdfView.displayDirection = .vertical
                    pdfView.document = pdfDocument
            }
        }else{
            print("urlDocumentoPdf es null")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//
//  MisTicketsViewController.swift
//  ticketvirtual
//
//  Created by usuario on 3/19/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class MisTicketsViewController: UIViewController{
    
    @IBOutlet weak var tblTicket:UITableView!
    @IBOutlet weak var header: UIView!
    
    
    //MARK: - DB and table
    var realm : Realm!
    var objectsArray : Results<Ticket>!
    var olistaTicket:Array<Ticket> = []
    
    // var dataSearchTicket:Array<Ticket> = []
    var dataSearchTicket : Results<Ticket>!
    // var dataItem:Array<Ticket> = []
    
    //MARK: - variables
    var indexSelect:Int = 0
    var isValidarTicket:Bool = true
    var cantidadTicket:Int = 0
    var contador:Int = 0
    
    lazy var refresh: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(cgColor: #colorLiteral(red: 0.5450980392, green: 0.4509803922, blue: 0.3294117647, alpha: 1))
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        realm = try! Realm()
        setupNavigationBarItem()
        objectsArray = realm.objects(Ticket.self).filter("grupo == %@",0)
        dataSearchTicket = objectsArray
        cantidadTicket = objectsArray.count
        if(cantidadTicket > 0){
            for index in 0...objectsArray.count - 1 {
                var oTicket = Ticket()
                oTicket = objectsArray[index]
                olistaTicket.append(oTicket)
            }
            validarListaTicket(tickets: olistaTicket)
        }
        
        header.clipsToBounds = true
        header.layer.cornerRadius = 20
        header.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        tblTicket.refreshControl = refresh
    }
    
    func actualizarTicketValidar(){
        olistaTicket = []
        //  dataItem = []
        objectsArray = realm.objects(Ticket.self).filter("grupo == %@",0)
        dataSearchTicket = objectsArray
        cantidadTicket = objectsArray.count
        if(cantidadTicket > 0){
            for index in 0...objectsArray.count - 1 {
                var oTicket = Ticket()
                oTicket = objectsArray[index]
                olistaTicket.append(oTicket)
                // dataItem.append(oTicket)
            }
        }
        self.refreshTable()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        self.navigationController?.navigationBar.barTintColor = UIColor(#colorLiteral(red: 0.04705882353, green: 0.1098039216, blue: 0.231372549, alpha: 1))
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func refreshTable(){
        DispatchQueue.main.async {
            self.tblTicket.reloadData()
        }
    }
    
    @objc func requestData(){
        let deadLine = DispatchTime.now() + .milliseconds(2000)
        print("olistaTicket \(olistaTicket.count) contador")
        DispatchQueue.main.asyncAfter(deadline: deadLine){
            if(self.cantidadTicket > 0){
                self.validarListaTicket(tickets: self.olistaTicket)
            }
            self.refresh.endRefreshing()
        }
    }
    
    func setupNavigationBarItem(){
        let title:String = BelmondUtil.validarIdioma() ? "Agregar" : "Add"
        let consultaButton =  UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(addNewTicket))
        consultaButton.tintColor = .white
        navigationItem.rightBarButtonItem = consultaButton
    }
    
    @objc func addNewTicket(){
        isValidarTicket = false
        self.performSegue(withIdentifier: "sgModalBuscarDocumento", sender: self)
    }
    
    private func validarListaTicket(tickets:Array<Ticket>){
        for index in 0...tickets.count-1 {
            validarTicket(numeroTicket: tickets[index].numeroTicket!, indexPath: index)
        }
    }
    
    private func validarTicket(numeroTicket:String, indexPath:Int){
        let ticketViewModel = TicketViewModel.init(parent: self)
        ticketViewModel.validarTicket(numeroTicket: numeroTicket, callback: {(success,data,error) in
            if(success!){
                self.contador = self.contador + 1
                switch data?.estadoTicket {
                case Constante.MODIFICADO:
                    print("Ticket a Actualizar : ",numeroTicket)
                    self.actualizarTicket(numeroTicket: numeroTicket, indexPath: indexPath)
                case Constante.INVALIDO:
                    let oTicket = self.realm.objects(Ticket.self).filter("numeroTicket == %@",numeroTicket).filter("grupo == %@",0).first
                    BelmondUtil.eliminarDocumentoFile(documentoPath: oTicket!.rutaArchivoOnboarding!)
                    BelmondUtil.eliminarDocumentoFile(documentoPath: oTicket!.rutaArchivoDocumentoElectronico!)
                    try! self.realm.write{
                        self.realm.delete(oTicket!)
                    }
                case Constante.VALIDO:
                    print("Ticket valido : ",numeroTicket)
                default:
                    print("default")
                }
                if(self.cantidadTicket == self.contador){
                    self.objectsArray = self.realm.objects(Ticket.self).filter("grupo == %@",0)
                    self.refreshTable()
                }
            }
        })
    }
    
    private func actualizarTicket(numeroTicket:String,indexPath:Int){
        let ticketViewModel = TicketViewModel.init(parent: self)
        ticketViewModel.buscarPorTicket(numeroTicket: numeroTicket, callback: {(success,data,error) in
            if(success!){
                // Eliminamos ticket anterior
                let item = self.objectsArray[indexPath]
                //   self.tblTicket.deleteRows(at: [indexPath], with: .fade)
                BelmondUtil.eliminarDocumentoFile(documentoPath: item.rutaArchivoOnboarding!)
                BelmondUtil.eliminarDocumentoFile(documentoPath: item.rutaArchivoDocumentoElectronico!)
                try! self.realm.write{
                    self.realm.delete(item)
                }
                // creamos nuevo ticket
                var oTicket = Ticket()
                for index in 0...data!.count - 1 {
                    let response = TicketResponse()
                    response.grupo = 0
                    response.nombrePasajero = data![index].nombrePasajero
                    response.numeroTicket = data![index].numeroTicket
                    response.ruta = data![index].ruta
                    response.tipoTren = data![index].tipoTren
                    response.vagon = data![index].vagon
                    response.numeroAsiento = data![index].numeroAsiento
                    response.fechaSalida = data![index].fechaSalida
                    response.horaSalida = data![index].horaSalida
                    response.codigoHash =  data![index].codigoHash
                    response.esModifi = data![index].esModifi
                    oTicket = BelmondUtil.castTicketReponseToTicket(oticket: response)
                }
                try! self.realm.write{
                    self.realm.add(oTicket)
                }
                
                self.downloadFileOnboarding(urlPDF: oTicket.rutaArchivoOnboarding!, numeroTicket: oTicket.numeroTicket!)
                self.downloadFileDocumentoContable(urlPDF: oTicket.rutaArchivoDocumentoElectronico!, numeroTicket: oTicket.numeroTicket!)
            }
        })
    }
    
    func downloadFileOnboarding(urlPDF:String,numeroTicket:String){
        let nombreOnboarding = "Onboarding-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreOnboarding)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in }.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download Onboarding : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",0).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoOnboarding = nombreOnboarding
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento onboarding :",nombreOnboarding)
            }
        }
    }
    
    func downloadFileDocumentoContable(urlPDF:String,numeroTicket:String){
        let nombreDocumentoContable = "Documento-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreDocumentoContable)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in}.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download documento contantable : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",0).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoDocumentoElectronico = nombreDocumentoContable
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento Contable :",nombreDocumentoContable)
            }
        }
    }
    
    @objc func accionDetalle(sender: UIButton) {
        let index = sender.tag
        indexSelect = index
        self.performSegue(withIdentifier: "sgDetalleTicket", sender: self)
    }
    
    @objc func accionEliminar(sender: UIButton) {
        let idObject = sender.tag
        showAlertEliminar(idObject: idObject)
    }
    
    @objc func accionQR(sender: UIButton) {
        let index = sender.tag
        indexSelect = index
        self.performSegue(withIdentifier: "sqCodigoQR", sender: self)
    }
    
    func showAlertEliminar(idObject:Int){
        if let object = self.realm.objects(Ticket.self).filter("id = %@", idObject as Int).filter("grupo == %@",0).first {
            let textTitle:String = BelmondUtil.validarIdioma() ? "Eliminar" : "Delete"
            let textMessage:String =  BelmondUtil.validarIdioma() ? "¿Desea eliminar el boleto seleccionado?" : "Do you want to delete the selected ticket?"
            let textNumeroBoleto:String = BelmondUtil.validarIdioma() ? "Número de boleto" : "Ticket number"
            let deleteText = BelmondUtil.validarIdioma() ? "Eliminar" : "Delete"
            let cancelarText = BelmondUtil.validarIdioma() ? "Cancelar" : "Cancel"
            
            let message:String = "\(textMessage) \n \(textNumeroBoleto) : \(object.numeroTicket ?? "")"
            let alert = UIAlertController(title: textTitle, message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title:deleteText, style: .default, handler: { (action) -> Void in
                
                if let rutaArchivoDocumentoElectronico = object.rutaArchivoDocumentoElectronico,rutaArchivoDocumentoElectronico != "" {
                    BelmondUtil.eliminarDocumentoFile(documentoPath: rutaArchivoDocumentoElectronico)
                }
                
                if let rutaArchivoOnboarding = object.rutaArchivoOnboarding,rutaArchivoOnboarding != ""{
                    BelmondUtil.eliminarDocumentoFile(documentoPath: rutaArchivoOnboarding)
                }
                
                try! self.realm.write{
                    self.realm.delete(object)
                    self.actualizarTicketValidar()
                }
            })
            alert.addAction(ok)
            alert.addAction(UIAlertAction(title: cancelarText, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgDetalleTicket"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! DetalleTicketViewController
            // let destination = segue.destination as! DetalleTicketViewController
            destination.ticketReponse = BelmondUtil.castTicketToTicketReponse(oticket:objectsArray[indexSelect])
        }else if(segue.identifier == "sgModalBuscarDocumento"){
            let destination = segue.destination as! DialogConsultaTicketViewController
            destination.ticketResponseDelegate = self
        }else if(segue.identifier == "sqCodigoQR"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! VisorQRViewController
            destination.codigoHash = objectsArray[indexSelect].codigoHash
            // destination.codigoHash = cdataItem[indexSelect].codigoHash
        }
    }
}

extension MisTicketsViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTicketCell", for: indexPath) as! ListTicketTableViewCell
        
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                cell.lblPasajero.text = self.objectsArray[indexPath.row].nombrePasajero
                cell.lblRuta.text = self.objectsArray[indexPath.row].ruta
                cell.lblNroTicket.text = self.objectsArray[indexPath.row].numeroTicket
                cell.lblFecha.text = self.objectsArray[indexPath.row].fechaSalida
                cell.lblHora.text = self.objectsArray[indexPath.row].horaSalida
                if let codigoHash = self.objectsArray[indexPath.row].codigoHash,codigoHash != "" {
                    cell.cardQR.isHidden = false
                    cell.imgQr.image = self.generateQRCode(from: self.objectsArray[indexPath.row].codigoHash!)
                }else{
                    cell.cardQR.isHidden = true
                }
            }}
        
        cell.btnDetalle.tag = indexPath.row
        cell.btnDetalle.addTarget(self, action: #selector(accionDetalle(sender:)), for: .touchUpInside)
        cell.btnEliminar.tag = objectsArray[indexPath.row].id
        cell.btnEliminar.addTarget(self, action: #selector(accionEliminar(sender:)), for: .touchUpInside)
        cell.btnQR.tag = indexPath.row
        cell.btnQR.addTarget(self, action: #selector(accionQR(sender:)), for: .touchUpInside)
        
        
        
        if let estado = objectsArray[indexPath.row].esModifi, estado == Constante.ISMODIFICADO {
            cell.lblUpdate.textColor = UIColor(cgColor: #colorLiteral(red: 0.5450980392, green: 0.4509803922, blue: 0.3294117647, alpha: 1))
        }else{
            cell.lblUpdate.text = ""
        }
        return cell
    }
}


extension MisTicketsViewController : TicketResponseDelegate{
    func updateData(success: Int) {
        if(success == 1){
            actualizarTicketValidar()
        }
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

//extension MisTicketsViewController: UISearchBarDelegate{
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        guard !searchText.isEmpty else {
//            guard let dataSearch = dataSearchTicket else {
//                return
//            }
//            objectsArray = dataSearch
//            tblTicket.reloadData()
//            return
//        }
//
//        var text:String
//        text = searchBar.text ?? ""
//        guard let dataSearch = dataSearchTicket else {
//            return
//        }
//        objectsArray = dataSearch.filter("nombrePasajero contains[c] %@","\(text)")
//        tblTicket.reloadData()
//
//    }
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.endEditing(true)
//    }
//}

//
//  DialogConsultaTicketViewController.swift
//  ticketvirtual
//
//  Created by usuario on 3/19/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

protocol TicketResponseDelegate {
    func updateData(success:Int)
}


class DialogConsultaTicketViewController: UIViewController {
    
    @IBOutlet weak var btnTipoDocumento: ButtonWithImage!
    @IBOutlet weak var btnNacionalidad: ButtonWithImage!
    @IBOutlet weak var edtNroDocumento: UITextField!
    
    var ticketResponseDelegate:TicketResponseDelegate?
    var contador:Int?
    var idTipoDocumento:String?
    var idNacionalidad:String?
    var urlDocumentoPdf:String?
    
    var realm : Realm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        realm = try! Realm()
        setupButton(button: btnNacionalidad)
        setupButton(button: btnTipoDocumento)
        contador=0;
        //BelmondUtil.tfClearError(field: edtNroDocumento)
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    
    @IBAction func accionNacionalidad(_ sender: ButtonWithImage) {
        self.performSegue(withIdentifier: "sgNacionalidad", sender: self)
    }
    @IBAction func accionTipoDocumento(_ sender: ButtonWithImage) {
        self.performSegue(withIdentifier: "sgTipoDocumento", sender: self)
    }
    
    @IBAction func accionCerrar(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accionConsultar(_ sender: ButtonRadius) {
        if(validarCampos()){
            BelmondUtil.showActivityIndicatorV2(uiView: (self.view), title: BelmondUtil.validarIdioma() ? "validando...":"validating...")
            let params:NSDictionary = [
                "codPais": idNacionalidad!, "tipDoc": idTipoDocumento!,"docIde":edtNroDocumento.text!]
            
            let ticketViewModel = TicketViewModel.init(parent: self)
            ticketViewModel.buscarPorDocumento(params: params, callback: {(succces,data,code,error) in
                if(succces!){
                    if let data = data {
                        for index in 0...data.count-1{
                            if let object = self.realm.objects(Ticket.self).filter("numeroTicket = %@", data[index].numeroTicket! as String).filter("grupo == %@",0).first {
                                print("Ticket ya exite : \(object.numeroTicket! as String)")
                            }else{
                                let tickets = self.realm.objects(Ticket.self).count
                                let contador = tickets + 1
                                let ticket = Ticket()
                                ticket.id = contador
                                ticket.grupo = 0
                                ticket.nombrePasajero = data[index].nombrePasajero
                                ticket.numeroTicket =  data[index].numeroTicket
                                ticket.ruta =  data[index].ruta
                                ticket.tipoTren =  data[index].tipoTren
                                ticket.vagon =  data[index].vagon
                                ticket.numeroAsiento = data[index].numeroAsiento
                                ticket.fechaSalida = data[index].fechaSalida
                                ticket.horaSalida = data[index].horaSalida
                                ticket.codigoHash = data[index].codigoHash
                                ticket.esModifi =  data[index].esModifi
                                try! self.realm.write{
                                    self.realm.add(ticket)
                                }
                                
                                if let rutaArchivoOnboarding = data[index].rutaArchivoOnboarding {
                                    self.downloadFileOnboarding(urlPDF: rutaArchivoOnboarding, numeroTicket: data[index].numeroTicket!)
                                }
                                
                                if let rutaArchivoDocumentoElectronico = data[index].rutaArchivoDocumentoElectronico {
                                    self.downloadFileDocumentoContable(urlPDF: rutaArchivoDocumentoElectronico, numeroTicket: data[index].numeroTicket!)
                                }
                            }
                        }
                        BelmondUtil.hideActivityIndicator(uiView: (self.view)!)
                        self.ticketResponseDelegate?.updateData(success: 1)
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    BelmondUtil.hideActivityIndicator(uiView: (self.view)!)
                    let title:String = BelmondUtil.validarIdioma() ? "Información" : "Information"
                    let message:String = BelmondUtil.validarIdioma() ? "Documento no válido" : "Invalid document"
                    let txtAceptar:String = BelmondUtil.validarIdioma() ? "Aceptar" : "Done"
                    let alert = BelmondUtil.getAlert(title: title, message: message, titleAction: txtAceptar)
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
    
    func downloadFileOnboarding(urlPDF:String,numeroTicket:String){
        let nombreOnboarding = "Onboarding-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreOnboarding)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in }.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download Onboarding : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",0).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoOnboarding = nombreOnboarding
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento onboarding :",nombreOnboarding)
            }
        }
    }
    
    func downloadFileDocumentoContable(urlPDF:String,numeroTicket:String){
        let nombreDocumentoContable = "Documento-\(BelmondUtil.splitString(nombre: numeroTicket)).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = BelmondUtil.crearDocumentoFile(nombre: nombreDocumentoContable)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlPDF, to: destination).downloadProgress { progress in}.response { response in
            if response.error == nil, let filePath = response.destinationURL?.path {
                print("Download documento contantable : \(filePath)")
                let tickets = self.realm.objects(Ticket.self).filter("numeroTicket = %@", numeroTicket).filter("grupo == %@",0).first
                if let ticket = tickets{
                    try! self.realm.write {
                        ticket.rutaArchivoDocumentoElectronico = nombreDocumentoContable
                    }
                }
            }else{
                print("Ocuerrio un error al descargar documento Contable :",nombreDocumentoContable)
            }
        }
    }
    
    
    private func validarCampos()-> Bool{
        let txtTitle:String = BelmondUtil.validarIdioma() ? "Información" : "Information"
        let txtNacionalidad:String = BelmondUtil.validarIdioma() ? "Seleccionar Nacionalidad" : "Select Nationality"
        let txtTipoDocumento:String = BelmondUtil.validarIdioma() ? "Seleccionar Documento de viaje" : "Select Travel document"
        let txtCaracterEspecial:String = BelmondUtil.validarIdioma() ? "No se permite caracteres especiales" : "Special characters are not allowed"
        let txtAceptar:String = BelmondUtil.validarIdioma() ? "Aceptar" : "Done"
        let txtCampoObligatorio:String = BelmondUtil.validarIdioma() ? "Campo obligatorio" : "Obligatory field"
        
        if(nil != idTipoDocumento && !idTipoDocumento!.isEmpty){
            if(!edtNroDocumento.text!.isEmpty){
                if(!validarCampoRegex(enteredEmail: edtNroDocumento.text!)){
                    if(nil != idNacionalidad && !idNacionalidad!.isEmpty){
                        BelmondUtil.tfClearError(field: edtNroDocumento)
                        return true
                    }else{
                        let alert = BelmondUtil.getAlert(title: txtTitle, message: txtNacionalidad, titleAction: txtAceptar)
                        self.present(alert,animated: true,completion: nil)
                    }
                }else{
                    let alert = BelmondUtil.getAlert(title: txtTitle, message: txtCaracterEspecial, titleAction: txtAceptar)
                    self.present(alert,animated: true,completion: nil)
                }
            }else{
                BelmondUtil.tfSetError(field: edtNroDocumento, placeholder:txtCampoObligatorio)
            }
        }else{
            let alert = BelmondUtil.getAlert(title: txtTitle, message: txtTipoDocumento, titleAction: txtAceptar)
            self.present(alert,animated: true,completion: nil)
        }
        return false
    }
    
    func validarCampoRegex(enteredEmail:String) -> Bool {
        let emailFormat = ".*[^A-Za-z0-9].*"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgTipoDocumento") {
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! TipoDocumentoViewController
            destino.tipoDocumentoDelegate = self
        }else if(segue.identifier == "sgNacionalidad") {
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! NacionalidadViewController
            destino.nacionalidaDelegate = self
        }
    }
}


extension DialogConsultaTicketViewController : TipoDocumentoDelegate , NacionalidadDelegate {
    func setNacionalidad(object: Nacionalidad) {
        btnNacionalidad.setTitle(object.nombre, for: .normal)
        idNacionalidad = object.id
    }
    
    func setTipoDocumento(object: TipoDocumento) {
        btnTipoDocumento.setTitle(object.nombre, for: .normal)
        idTipoDocumento = object.id
    }
}


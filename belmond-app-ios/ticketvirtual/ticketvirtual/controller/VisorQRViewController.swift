//
//  VisorQRViewController.swift
//  ticketvirtual
//
//  Created by usuario on 7/11/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class VisorQRViewController: UIViewController {
    
    @IBOutlet weak var imgQR: UIImageView!
    var codigoHash:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let qr = codigoHash {
            setupNavigationBarItem()
            imgQR.image = BelmondUtil.generateQRCode(from:qr)
        }
    }
    
      func setupNavigationBarItem(){
            let cerrarText:String = BelmondUtil.validarIdioma() ? "Cerrar" : "Close"
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.setNavigationBarHidden(false,animated:true)
            self.navigationController?.navigationBar.barTintColor = UIColor(#colorLiteral(red: 0.04705882353, green: 0.1098039216, blue: 0.231372549, alpha: 1))
            self.navigationController?.navigationBar.topItem?.title = "QR"
            let crearButton =  UIBarButtonItem(title: cerrarText, style: .plain, target: self, action: #selector(cerrarModal))
            crearButton.tintColor = .white
        

            
            navigationItem.leftBarButtonItem = crearButton
        }
    
    @objc func cerrarModal(){
         //navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}

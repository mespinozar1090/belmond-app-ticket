//
//  SplashViewController.swift
//  ticketvirtual
//
//  Created by usuario on 8/25/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.perform(#selector(showMain), with: nil, afterDelay: 3)
    }
    
    @objc func showMain() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialVC = storyboard.instantiateViewController(withIdentifier: "MyTabBarViewController") as! MyTabBarViewController
        self.present(initialVC, animated: true, completion: nil)
    }
}

//
//  NacionalidadViewController.swift
//  ticketvirtual
//
//  Created by usuario on 4/10/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

protocol NacionalidadDelegate {
    func setNacionalidad(object:Nacionalidad)
}

class NacionalidadViewController: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var tblItem:UITableView!
    lazy var searchBar:UISearchBar = UISearchBar()
    
    var dataItem:Array<Nacionalidad> = []
    var dataSearch:Array<Nacionalidad> = []
    var nacionalidaDelegate : NacionalidadDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        setUpsearchBar()
        setupData()
    }
    
    func setupNavigationBarItem(){
        navigationController!.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(#colorLiteral(red: 0.04705882353, green: 0.1098039216, blue: 0.231372549, alpha: 1))
    }
    
    private func setupData(){
        dataItem = (Locale.current.languageCode?.contains("es"))! ? BelmondUtil.getNacionalidad() : BelmondUtil.getNacionalidadEn()
        dataSearch = dataItem
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
    private func setUpsearchBar(){
        searchBar.delegate = self
        searchBar.endEditing(true)
        searchBar.placeholder = BelmondUtil.validarIdioma() ? "Buscar" : "Search"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        let text = searchBar.value(forKey: "searchField") as? UITextField
        text?.textColor = .gray
        text?.font = UIFont.systemFont(ofSize: 14.0)
        navigationItem.titleView = searchBar
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            dataItem = dataSearch
            tblItem.reloadData()
            return
        }
        dataItem = dataSearch.filter({ (DocumentoModel) -> Bool in
            guard let text = searchBar.text else {return false}
            return (DocumentoModel.nombre?.localizedCaseInsensitiveContains(text))!
        })
        tblItem.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - CONFIGURACION TABLE
extension NacionalidadViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NacionalidadTableViewCell",for: indexPath) as! NacionalidadTableViewCell
        cell.lblNacionalidad.text = dataItem[indexPath.row].nombre
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.nacionalidaDelegate?.setNacionalidad(object: dataItem[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
}

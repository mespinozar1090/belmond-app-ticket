//
//  AppDelegate.swift
//  ticketvirtual
//
//  Created by usuario on 2/24/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes =  [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().barTintColor = UIColor(#colorLiteral(red: 0.04705882353, green: 0.1098039216, blue: 0.231372549, alpha: 1))
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font:UIFont(name: "Kawak-Regular", size: 14)!], for: .normal)
      //  Thread.sleep(forTimeInterval: 3.0)
        return true
    }

    // MARK: UISceneSession Lifecycle


}


//
//  Ticket.swift
//  ticketvirtual
//
//  Created by usuario on 4/6/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation
import RealmSwift

class Ticket : Object{
    
    @objc dynamic var id = 0
    @objc dynamic var grupo = 0
    @objc dynamic var numeroTicket :String?
    @objc dynamic var nombrePasajero:String?
    @objc dynamic var ruta :String?
    @objc dynamic var tipoTren :String?
    @objc dynamic var vagon :String?
    @objc dynamic var numeroAsiento :String?
    @objc dynamic var fechaSalida :String?
    @objc dynamic var horaSalida :String?
    @objc dynamic var rutaArchivoOnboarding :String?
    @objc dynamic var rutaArchivoDocumentoElectronico :String?
    @objc dynamic var codigoHash :String?
    @objc dynamic var esModifi :String?
}

//
//  Grupo.swift
//  ticketvirtual
//
//  Created by usuario on 4/6/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation
import RealmSwift

class Grupo: Object {
    @objc dynamic var id = 0
    @objc dynamic var nombre:String?
}

//
//  TicketModel.swift
//  ticketvirtual
//
//  Created by usuario on 2/26/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation

class TicketModel {
    
       var id:Int?
       var idGrupo:Int?
       var numeroTicket:String?
       var nombrePasajero:String?
       var ruta:String?
       var tipoTren:String?
       var vagon:String?
       var numeroAsiento:String?
       var fechaSalida:String?
       var horaSalida:String?
       var estadoTicket:String?
       var rutaArchivoOnboarding:String?
       var rutaArchivoDocumentoElectronico:String?
       var codigoHash:String?
    
}

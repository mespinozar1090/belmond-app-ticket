//
//  BaseView.swift
//  ticketvirtual
//
//  Created by usuario on 2/24/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit
@IBDesignable class BaseView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configure()
    }
    
    func configure(){
        
    }
}

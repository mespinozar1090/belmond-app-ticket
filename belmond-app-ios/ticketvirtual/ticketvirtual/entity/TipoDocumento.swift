//
//  TipoDocumento.swift
//  ticketvirtual
//
//  Created by usuario on 4/10/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation

class TipoDocumento{
    var id:String?
    var nombre:String?
    
    init(id:String,nombre:String) {
        self.id = id
        self.nombre = nombre
    }
    
}

//
//  Nacionalidad.swift
//  ticketvirtual
//
//  Created by usuario on 4/10/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation

class Nacionalidad {
    var id:String?
    var nombre:String?
    
    init(id:String,nombre:String) {
        self.id = id
        self.nombre = nombre
    }
}

//
//  TicketViewModel.swift
//  ticketvirtual
//
//  Created by usuario on 4/6/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class TicketViewModel{
    
    var parent: UIViewController? = nil
    
    init() {
    }
    
    init(parent: UIViewController) {
        self.parent = parent
    }
    
    func buscarPorDocumento(params: NSDictionary, callback: @escaping (Bool?,Array<TicketResponse>?, Int?,NSError?) -> ()) {
        TicketNetwork.getInstance(parent: self.parent!).buscarPorDocumento(params: params, callback: {(succes,code,error)in
            if let result = succes{
                if(result.count > 0) {
                    var response:Array<TicketResponse> = []
                    for index in 0...result.count-1 {
                        let oTicket = TicketResponse()
                        oTicket.numeroTicket = result[index].value(forKey: "numTicket") as? String
                        oTicket.nombrePasajero = result[index].value(forKey: "nomPasaj") as? String
                        oTicket.ruta = result[index].value(forKey: "ruta") as? String
                        oTicket.tipoTren = result[index].value(forKey: "tipTren") as? String
                        oTicket.vagon = result[index].value(forKey: "vagon") as? String
                        oTicket.numeroAsiento = result[index].value(forKey: "asiento") as? String
                        oTicket.fechaSalida = result[index].value(forKey: "fecSalida") as? String
                        oTicket.horaSalida = result[index].value(forKey: "horSalida") as? String
                        oTicket.esModifi = result[index].value(forKey: "esModifi") as? String
                        oTicket.rutaArchivoOnboarding = result[index].value(forKey: "urlTicket") as? String
                        oTicket.rutaArchivoDocumentoElectronico = result[index].value(forKey: "urlDocu") as? String
                        oTicket.codigoHash = result[index].value(forKey: "codHash") as? String
                        response.append(oTicket)
                    }
                    callback(true,response,code,nil)
                }else{
                    print("Error no tiende data")
                    callback(false,nil,code,nil)
                }
            }else{
                callback(false,nil,code,nil)
            }
        })
    }
    
    
    func buscarPorTicket(numeroTicket: String, callback: @escaping (Bool?,Array<TicketResponse>?,NSError?) -> ()) {
           TicketNetwork.getInstance(parent: self.parent!).buscarPorTicket(numeroTicket: numeroTicket ,callback: {(succes,error)in
               if let result = succes{
                   if(result.count > 0) {
                       print("succes tiene data")
                       var response:Array<TicketResponse> = []
                       for index in 0...result.count-1 {
                           let oTicket = TicketResponse()
                           oTicket.numeroTicket = result[index].value(forKey: "numTicket") as? String
                           oTicket.nombrePasajero = result[index].value(forKey: "nomPasaj") as? String
                           oTicket.ruta = result[index].value(forKey: "ruta") as? String
                           oTicket.tipoTren = result[index].value(forKey: "tipTren") as? String
                           oTicket.vagon = result[index].value(forKey: "vagon") as? String
                           oTicket.numeroAsiento = result[index].value(forKey: "asiento") as? String
                           oTicket.fechaSalida = result[index].value(forKey: "fecSalida") as? String
                           oTicket.horaSalida = result[index].value(forKey: "horSalida") as? String
                           oTicket.esModifi = result[index].value(forKey: "esModifi") as? String
                           oTicket.rutaArchivoOnboarding = result[index].value(forKey: "urlTicket") as? String
                           oTicket.rutaArchivoDocumentoElectronico = result[index].value(forKey: "urlDocu") as? String
                           oTicket.codigoHash = result[index].value(forKey: "codHash") as? String
                           response.append(oTicket)
                       }
                       callback(true,response,nil)
                   }else{
                       print("Error no tiende data")
                       callback(false,nil,error)
                   }
               }else{
                   callback(false,nil,error)
               }
           })
       }
    
    func validarTicket(numeroTicket: String, callback: @escaping (Bool?,TicketResponse?,NSError?) -> ()) {
            TicketNetwork.getInstance(parent: self.parent!).validarTicket(numeroTicket: numeroTicket ,callback: {(succes,error)in
                  if let result = succes{
                      if(result.count > 0) {
                          print("succes tiene data")
                            let oTicket = TicketResponse()
                            oTicket.numeroTicket = result.value(forKey: "numTicket") as? String
                            oTicket.estadoTicket = result.value(forKey: "estadoTicket") as? String
                          callback(true,oTicket,nil)
                      }else{
                          print("Error no tiende data")
                          callback(false,nil,nil)
                      }
                  }else{
                      callback(false,nil,nil)
                  }
              })
          }
    
    func autenticacion(uid: String, callback: @escaping (Bool?,String?,NSError?) -> ()) {
        TicketNetwork.getInstance(parent: self.parent!).autenticacion(uid: uid ,callback: {(succes,error)in
              if let result = succes{
                let tokenJwt:String = result.value(forKey: "token") as? String ?? ""
                if(tokenJwt != ""){
                    let defaults = UserDefaults.standard
                    defaults.set(tokenJwt, forKey: "tokenJwt")
                    defaults.synchronize()
                     callback(true,tokenJwt,nil)
                }else{
                 callback(false,tokenJwt,nil)
                }
              }else{
                  callback(false,nil,nil)
              }
          })
      }
}


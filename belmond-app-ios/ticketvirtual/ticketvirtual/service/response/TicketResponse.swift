//
//  TicketResponse.swift
//  ticketvirtual
//
//  Created by usuario on 4/6/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation

class TicketResponse{
    
    var grupo :Int?
    var numeroTicket :String?
    var nombrePasajero:String?
    var ruta :String?
    var tipoTren :String?
    var vagon :String?
    var numeroAsiento :String?
    var fechaSalida :String?
    var horaSalida :String?
    var esModifi :String?
    var rutaArchivoOnboarding :String?
    var rutaArchivoDocumentoElectronico :String?
    var codigoHash :String?
    var estadoTicket :String?
    
}

//
//  TicketNetwork.swift
//  ticketvirtual
//
//  Created by usuario on 4/6/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation
import Alamofire

class TicketNetwork {
    
    static let instance = TicketNetwork()
    static var parent: UIViewController? = nil
    
    init() {
    }
    
    static func getInstance(parent: UIViewController) -> TicketNetwork {
        if(self.parent == nil) {
            self.parent = parent
        }
        return self.instance
    }
    
    func buscarPorDocumento(params: NSDictionary, callback: @escaping (Array<NSDictionary>?,Int?, NSError?) -> ()) {
        let codPais = params.value(forKey: "codPais") as! String
        let tipDoc = params.value(forKey: "tipDoc") as! String
        let docIde = params.value(forKey: "docIde") as! String
        
        let tokenJwt = UserDefaults.standard.value(forKey: "tokenJwt") as! String
        let headers : HTTPHeaders  = [
            "jwt":tokenJwt
        ]
        
        let queryUrl:String = BelmondUtil.queryBuscarPorDocumento(codPais: codPais, tipDoc: tipDoc, docIde: docIde)
        Alamofire.request(queryUrl, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>,response.response?.statusCode,nil)
                break
            case .failure(let error):
                callback(nil, response.response?.statusCode,error as NSError?)
                break
            }
        }
    }
    
    func buscarPorTicket(numeroTicket: String, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        let queryUrl:String = BelmondUtil.queryBuscarPorTicket(numTic: numeroTicket)
        let tokenJwt = UserDefaults.standard.value(forKey: "tokenJwt") as! String
        let headers : HTTPHeaders  = [
            "jwt":tokenJwt
        ]
        Alamofire.request(queryUrl, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>,nil)
                break
            case .failure(let error):
                callback(nil,error as NSError?)
                break
            }
        }
    }
    
    func validarTicket(numeroTicket: String, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        let queryUrl:String = BelmondUtil.queryValidarTicket(numTic: numeroTicket)
        let tokenJwt = UserDefaults.standard.value(forKey: "tokenJwt") as! String
        let headers : HTTPHeaders  = [
            "jwt":tokenJwt
        ]
        Alamofire.request(queryUrl, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary,nil)
                break
            case .failure(let error):
                callback(nil,error as NSError?)
                break
            }
        }
    }
    
    func autenticacion(uid: String, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        let queryUrl:String = BelmondUtil.queryAutenticar(uid: uid)
        Alamofire.request(queryUrl, method: .get, parameters: nil, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary,nil)
                break
            case .failure(let error):
                callback(nil,error as NSError?)
                break
            }
        }
    }
    
}

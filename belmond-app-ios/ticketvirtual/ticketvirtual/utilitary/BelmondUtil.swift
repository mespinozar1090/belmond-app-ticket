//
//  BelmondUtil.swift
//  ticketvirtual
//
//  Created by usuario on 4/6/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation
import UIKit

class BelmondUtil{
    
    static let URL_BASE:String = "https://pax3.perurail.com/vyr_apirest" //PRODUCCION
   // static let URL_BASE:String = "https://pax.perurail.com/desapr/hlozano/REIMPRE" // PRUEBAS
    static var container: UIView = UIView()
    static var loadingView: UIView = UIView()
    static var labelIndicator:UILabel = UILabel()
    static var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    static let mainPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    static let destinationPath = mainPath+"/BelmondApp/tickets"
    
    static func queryBuscarPorDocumento(codPais:String,tipDoc:String,docIde:String) -> String{
        let queryUrl :String = "\(URL_BASE)/Api/DetalleTickets?codPais=\(codPais)&tipDoc=\(tipDoc)&docIde=\(docIde)&numTic=&tipAcc=C&Uid=belmond"
        return queryUrl
    }
    
    static func queryBuscarPorTicket(numTic:String) -> String{
        let queryUrl :String = "\(URL_BASE)/Api/DetalleTickets?codPais=&tipDoc=&docIde=&numTic=\(numTic)&tipAcc=C&Uid=belmond"
        return queryUrl
    }
    
    static func queryValidarTicket(numTic:String) -> String{
        let queryUrl:String = "\(URL_BASE)/Api/DetalleTickets?codPais=&tipDoc=&docIde=&numTic=\(numTic)&tipAcc=V&Uid=belmond"
        return queryUrl
    }
    
    static func queryAutenticar(uid:String) -> String{
        let queryUrl:String = "\(URL_BASE)/Api/generarToken?Uid=belmond"
        return queryUrl
    }
    
    static func castTicketToTicketReponse(oticket: Ticket) -> TicketResponse {
        let response = TicketResponse()
        response.grupo = oticket.grupo
        response.nombrePasajero = oticket.nombrePasajero
        response.numeroTicket = oticket.numeroTicket
        response.ruta = oticket.ruta
        response.tipoTren = oticket.tipoTren
        response.vagon = oticket.vagon
        response.numeroAsiento = oticket.numeroAsiento
        response.fechaSalida = oticket.fechaSalida
        response.horaSalida = oticket.horaSalida
        response.rutaArchivoOnboarding = oticket.rutaArchivoOnboarding
        response.rutaArchivoDocumentoElectronico = oticket.rutaArchivoDocumentoElectronico
        response.codigoHash =  oticket.codigoHash
        response.esModifi = oticket.esModifi
        return response
    }
    
    static func castTicketReponseToTicket(oticket: TicketResponse) -> Ticket {
        let response = Ticket()
        response.grupo = oticket.grupo ?? 0
        response.nombrePasajero = oticket.nombrePasajero
        response.numeroTicket = oticket.numeroTicket
        response.ruta = oticket.ruta
        response.tipoTren = oticket.tipoTren
        response.vagon = oticket.vagon
        response.numeroAsiento = oticket.numeroAsiento
        response.fechaSalida = oticket.fechaSalida
        response.horaSalida = oticket.horaSalida
        response.rutaArchivoOnboarding = oticket.rutaArchivoOnboarding
        response.rutaArchivoDocumentoElectronico = oticket.rutaArchivoDocumentoElectronico
        response.codigoHash =  oticket.codigoHash
        response.esModifi = oticket.esModifi
        return response
    }
    
    static func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    static func getTipoDocumento() -> Array<TipoDocumento> {
        var dataItem:Array<TipoDocumento> = []
        dataItem.append(TipoDocumento(id: "DNI", nombre: "Documento Nacional de Indentidad"))
        dataItem.append(TipoDocumento(id: "PAS", nombre: "Pasaporte"))
        dataItem.append(TipoDocumento(id: "CEX", nombre: "Carnet de Extranjería"))
        dataItem.append(TipoDocumento(id: "PNA", nombre: "Partida de Nacimiento"))
        dataItem.append(TipoDocumento(id: "CID", nombre: "Carnet de Identidad"))
        dataItem.append(TipoDocumento(id: "RUC", nombre: "RUC"))
        return dataItem
    }
    
    static func getTipoDocumentoEn() -> Array<TipoDocumento> {
        var dataItem:Array<TipoDocumento> = []
        dataItem.append(TipoDocumento(id: "DNI", nombre: "National identity document"))
        dataItem.append(TipoDocumento(id: "PAS", nombre: "Passport"))
        dataItem.append(TipoDocumento(id: "CEX", nombre: "Immigration Card"))
        dataItem.append(TipoDocumento(id: "PNA", nombre: "Birth Certificate"))
        dataItem.append(TipoDocumento(id: "CID", nombre: "Identity card"))
        dataItem.append(TipoDocumento(id: "RUC", nombre: "RUC"))
        return dataItem
    }
    
    static func getNacionalidad() -> Array<Nacionalidad>{
        var dataItem:Array<Nacionalidad> = []
        dataItem.append(Nacionalidad(id: "AFG", nombre: "Afganistan"))
        dataItem.append(Nacionalidad(id: "ALB", nombre: "Albania"))
        dataItem.append(Nacionalidad(id: "ALE", nombre: "Alemania"))
        dataItem.append(Nacionalidad(id: "AND", nombre: "Andorra"))
        dataItem.append(Nacionalidad(id: "ANG", nombre: "Angola"))
        dataItem.append(Nacionalidad(id: "AIA", nombre: "Anguila"))
        dataItem.append(Nacionalidad(id: "ATA", nombre: "Antártica"))
        dataItem.append(Nacionalidad(id: "ATG", nombre: "Antigua y Barbuda"))
        dataItem.append(Nacionalidad(id: "AN", nombre: "Antillas Neerlandesas"))
        dataItem.append(Nacionalidad(id: "ARA", nombre: "Arabia Saudi"))
        dataItem.append(Nacionalidad(id: "DZA", nombre: "Argelia"))
        dataItem.append(Nacionalidad(id: "ARG", nombre: "Argentina"))
        dataItem.append(Nacionalidad(id: "ARM", nombre: "Armenia"))
        dataItem.append(Nacionalidad(id: "ARU", nombre: "Aruba"))
        dataItem.append(Nacionalidad(id: "AUS", nombre: "Australia"))
        dataItem.append(Nacionalidad(id: "AUT", nombre: "Austria"))
        dataItem.append(Nacionalidad(id: "AZE", nombre: "Azerbaiyán"))
        dataItem.append(Nacionalidad(id: "BAH", nombre: "Bahamas"))
        dataItem.append(Nacionalidad(id: "BAN", nombre: "Bangladesh"))
        dataItem.append(Nacionalidad(id: "BAR", nombre: "Barbados"))
        dataItem.append(Nacionalidad(id: "BHR", nombre: "Baréin"))
        dataItem.append(Nacionalidad(id: "BEL", nombre: "Bélgica"))
        dataItem.append(Nacionalidad(id: "BLZ", nombre: "Belize"))
        dataItem.append(Nacionalidad(id: "BEN", nombre: "Benin"))
        dataItem.append(Nacionalidad(id: "BER", nombre: "Bermudas"))
        dataItem.append(Nacionalidad(id: "BIE", nombre: "Bielorrusia"))
        dataItem.append(Nacionalidad(id: "BOL", nombre: "Bolivia"))
        dataItem.append(Nacionalidad(id: "BIH", nombre: "Bosnia y Herzegowina"))
        dataItem.append(Nacionalidad(id: "BWA", nombre: "BotsUana"))
        dataItem.append(Nacionalidad(id: "BRA", nombre: "Brasil"))
        dataItem.append(Nacionalidad(id: "BRN", nombre: "Brunéi Darussalam"))
        dataItem.append(Nacionalidad(id: "BUL", nombre: "Bulgaria"))
        dataItem.append(Nacionalidad(id: "BFA", nombre: "Burkina Faso"))
        dataItem.append(Nacionalidad(id: "MMR", nombre: "Burma"))
        dataItem.append(Nacionalidad(id: "BDI", nombre: "Burundi"))
        dataItem.append(Nacionalidad(id: "BTN", nombre: "Bután"))
        dataItem.append(Nacionalidad(id: "CPV", nombre: "Cabo Verde"))
        dataItem.append(Nacionalidad(id: "KHM", nombre: "Camboya"))
        dataItem.append(Nacionalidad(id: "CMR", nombre: "Camerún"))
        dataItem.append(Nacionalidad(id: "CAN", nombre: "Canadá"))
        dataItem.append(Nacionalidad(id: "TCD", nombre: "Chad"))
        dataItem.append(Nacionalidad(id: "CHL", nombre: "Chile"))
        dataItem.append(Nacionalidad(id: "CHI", nombre: "China"))
        dataItem.append(Nacionalidad(id: "CHP", nombre: "Chipre"))
        dataItem.append(Nacionalidad(id: "COL", nombre: "Colombia"))
        dataItem.append(Nacionalidad(id: "COM", nombre: "Comoras"))
        dataItem.append(Nacionalidad(id: "COG", nombre: "Congo"))
        dataItem.append(Nacionalidad(id: "CRN", nombre: "Corea del Norte"))
        dataItem.append(Nacionalidad(id: "CRS", nombre: "Corea del Sur"))
        dataItem.append(Nacionalidad(id: "CIV", nombre: "Costa de Marfil"))
        dataItem.append(Nacionalidad(id: "COS", nombre: "Costa Rica"))
        dataItem.append(Nacionalidad(id: "CRO", nombre: "Croacia"))
        dataItem.append(Nacionalidad(id: "CUB", nombre: "Cuba"))
        dataItem.append(Nacionalidad(id: "DIN", nombre: "Dinamarca"))
        dataItem.append(Nacionalidad(id: "DOM", nombre: "Dominica"))
        dataItem.append(Nacionalidad(id: "ECU", nombre: "Ecuador"))
        dataItem.append(Nacionalidad(id: "EGI", nombre: "Egipto"))
        dataItem.append(Nacionalidad(id: "ELS", nombre: "El Salvador"))
        dataItem.append(Nacionalidad(id: "EMI", nombre: "Emiratos Arabes Unidos"))
        dataItem.append(Nacionalidad(id: "ERI", nombre: "Eritrea"))
        dataItem.append(Nacionalidad(id: "ESQ", nombre: "Eslovaquia"))
        dataItem.append(Nacionalidad(id: "ESL", nombre: "Eslovenia"))
        dataItem.append(Nacionalidad(id: "ESP", nombre: "Egipto"))
        dataItem.append(Nacionalidad(id: "EUA", nombre: "Estados Unidos"))
        dataItem.append(Nacionalidad(id: "EST", nombre: "Estonia"))
        dataItem.append(Nacionalidad(id: "ETI", nombre: "Etiopía"))
        dataItem.append(Nacionalidad(id: "FIL", nombre: "Filipinas"))
        dataItem.append(Nacionalidad(id: "FI", nombre: "Finlandia"))
        dataItem.append(Nacionalidad(id: "FIY", nombre: "Fiyi"))
        dataItem.append(Nacionalidad(id: "FRA", nombre: "Francia"))
        dataItem.append(Nacionalidad(id: "GAB", nombre: "Gabón"))
        dataItem.append(Nacionalidad(id: "GAM", nombre: "Gambia"))
        dataItem.append(Nacionalidad(id: "GEO", nombre: "Georgia"))
        dataItem.append(Nacionalidad(id: "GHA", nombre: "Ghana"))
        dataItem.append(Nacionalidad(id: "GIB", nombre: "Gibraltar"))
        dataItem.append(Nacionalidad(id: "GRA", nombre: "Gran Bretaña"))
        dataItem.append(Nacionalidad(id: "GRD", nombre: "Granada"))
        dataItem.append(Nacionalidad(id: "GRE", nombre: "Grecia"))
        dataItem.append(Nacionalidad(id: "GRL", nombre: "Groenlandia"))
        dataItem.append(Nacionalidad(id: "GLP", nombre: "Guadalupe"))
        dataItem.append(Nacionalidad(id: "GUM", nombre: "Guam"))
        dataItem.append(Nacionalidad(id: "GUA", nombre: "Guatemala"))
        dataItem.append(Nacionalidad(id: "GNA", nombre: "Guinea"))
        dataItem.append(Nacionalidad(id: "GEC", nombre: "Guinea Ecuatorial"))
        dataItem.append(Nacionalidad(id: "GUI", nombre: "Guinea-Bissau"))
        dataItem.append(Nacionalidad(id: "GUY", nombre: "Guayana"))
        
        dataItem.append(Nacionalidad(id: "GYF", nombre: "Guayana Francesa"))
        dataItem.append(Nacionalidad(id: "HAI", nombre: "Haití"))
        dataItem.append(Nacionalidad(id: "HOL", nombre: "Holanda"))
        dataItem.append(Nacionalidad(id: "HON", nombre: "Honduras"))
        dataItem.append(Nacionalidad(id: "HKO", nombre: "Hong Kong"))
        dataItem.append(Nacionalidad(id: "HUN", nombre: "Hungría"))
        dataItem.append(Nacionalidad(id: "ID", nombre: "India"))
        dataItem.append(Nacionalidad(id: "IDN", nombre: "Indonesia"))
        dataItem.append(Nacionalidad(id: "ING", nombre: "Inglaterra"))
        dataItem.append(Nacionalidad(id: "IRN", nombre: "Iran"))
        dataItem.append(Nacionalidad(id: "IRQ", nombre: "Iraq"))
        dataItem.append(Nacionalidad(id: "IRL", nombre: "Irlanda"))
        dataItem.append(Nacionalidad(id: "BVT", nombre: "Isla Bouvet"))
        dataItem.append(Nacionalidad(id: "CXR", nombre: "Isla de Navidad"))
        dataItem.append(Nacionalidad(id: "NIU", nombre: "Isla Niue"))
        dataItem.append(Nacionalidad(id: "NFK", nombre: "Isla Norfolk"))
        dataItem.append(Nacionalidad(id: "PCN", nombre: "Isla Pitcairn"))
        dataItem.append(Nacionalidad(id: "ISL", nombre: "Islandia"))
        
        dataItem.append(Nacionalidad(id:"CYM", nombre: "Islas Caimán"))
        dataItem.append(Nacionalidad(id:"CCK", nombre: "Islas Cocos (Keeling)"))
        dataItem.append(Nacionalidad(id:"COK", nombre: "Islas Cook"))
        dataItem.append(Nacionalidad(id:"FRO", nombre: "Faroe"))
        dataItem.append(Nacionalidad(id:"HMD", nombre: "Islas Heard y Mcdonald"))
        dataItem.append(Nacionalidad(id:"FLK", nombre: "Islas Malvinas (Falkland)"))
        dataItem.append(Nacionalidad(id:"MNP", nombre: "Islas Malvinas del Norte"))
        dataItem.append(Nacionalidad(id:"UMI", nombre: "Islas Perifericas Menores de Estados Unidos"))
        dataItem.append(Nacionalidad(id:"SJM", nombre: "Islas Svalbard y Jan Mayen"))
        dataItem.append(Nacionalidad(id:"TCA", nombre: "Islas turcas y Caicos"))
        dataItem.append(Nacionalidad(id:"VGB", nombre: "Islas Vírgenes Británicas"))
        dataItem.append(Nacionalidad(id:"VIR", nombre: "Islas Vírgenes de los Estados Unidos"))
        dataItem.append(Nacionalidad(id:"WLF", nombre: "Islas Wallis y Futuna"))
        dataItem.append(Nacionalidad(id:"ISR", nombre: "Israel"))
        dataItem.append(Nacionalidad(id:"ITA", nombre: "Italia"))
        dataItem.append(Nacionalidad(id:"JAM", nombre: "Jamaica"))
        dataItem.append(Nacionalidad(id:"JAP", nombre: "Japón"))
        dataItem.append(Nacionalidad(id:"JOR", nombre: "Jordania"))
        dataItem.append(Nacionalidad(id:"KAZ", nombre: "Kazajstán"))
        dataItem.append(Nacionalidad(id:"KEN",nombre :"Kenia"));
        dataItem.append(Nacionalidad(id:"KRG",nombre :"Kirguistán"));
        dataItem.append(Nacionalidad(id:"KRB",nombre :"Kiribati"));
        dataItem.append(Nacionalidad(id:"KOR",nombre :"Korea"));
        dataItem.append(Nacionalidad(id:"KUW",nombre :"Kuwait"));
        dataItem.append(Nacionalidad(id:"COD",nombre :"La República Democrática del Congo"));
        dataItem.append(Nacionalidad(id:"lAO",nombre :"Laos"));
        dataItem.append(Nacionalidad(id:"LAT",nombre :"Latvia"));
        dataItem.append(Nacionalidad(id:"LES",nombre :"Leshoto"));
        dataItem.append(Nacionalidad(id:"LET",nombre :"Letonia"));
        dataItem.append(Nacionalidad(id:"LIB",nombre :"Líbano"));
        dataItem.append(Nacionalidad(id:"LBR",nombre :"Liberia"));
        dataItem.append(Nacionalidad(id:"LBA",nombre :"Libia"));
        dataItem.append(Nacionalidad(id:"LIE",nombre :"Liechtenstein"));
        dataItem.append(Nacionalidad(id:"LIT",nombre :"Lituania"));
        dataItem.append(Nacionalidad(id:"LUX",nombre :"Luxemburgo"));
        dataItem.append(Nacionalidad(id:"MAC",nombre :"Macao"));
        dataItem.append(Nacionalidad(id:"MCD",nombre :"Macedonia"));
        dataItem.append(Nacionalidad(id:"MAD",nombre :"Madagascar"));
        dataItem.append(Nacionalidad(id:"MAL",nombre :"Malasia"));
        dataItem.append(Nacionalidad(id:"MLW",nombre :"Malawi"));
        dataItem.append(Nacionalidad(id:"MLD",nombre :"Maldivas"));
        dataItem.append(Nacionalidad(id:"MLI",nombre :"Mali"));
        dataItem.append(Nacionalidad(id:"MLT",nombre :"Malta"));
        dataItem.append(Nacionalidad(id:"MRR",nombre :"Marruecos"));
        dataItem.append(Nacionalidad(id:"MRS",nombre :"Marshall Islands"));
        dataItem.append(Nacionalidad(id:"MAR",nombre :"Martinica"));
        dataItem.append(Nacionalidad(id:"MRC",nombre :"Mauricio"));
        dataItem.append(Nacionalidad(id:"MRT",nombre :"Mauritania"));
        dataItem.append(Nacionalidad(id:"MAY",nombre :"Mayotte"));
        dataItem.append(Nacionalidad(id:"MEX",nombre :"Mexico"));
        dataItem.append(Nacionalidad(id:"MIC",nombre :"Micronesia"));
        dataItem.append(Nacionalidad(id:"MOL",nombre :"Moldavia"));
        dataItem.append(Nacionalidad(id:"MON",nombre :"Mónaco"));
        dataItem.append(Nacionalidad(id:"MNG",nombre :"Mongolia"));
        dataItem.append(Nacionalidad(id:"MSR",nombre :"Monserrat"));
        dataItem.append(Nacionalidad(id:"MOZ",nombre :"Mozambique"));
        dataItem.append(Nacionalidad(id:"NAM",nombre :"Namibia"));
        dataItem.append(Nacionalidad(id:"NRU",nombre :"Nauru"));
        dataItem.append(Nacionalidad(id:"NEP",nombre :"Nepal"));
        dataItem.append(Nacionalidad(id:"NGA",nombre :"Nigeria"));
        dataItem.append(Nacionalidad(id:"NOR",nombre :"Noruega"));
        dataItem.append(Nacionalidad(id:"NCL",nombre :"Nueva Caledonia"));
        dataItem.append(Nacionalidad(id:"NUE",nombre :"Nueva Zelanda"));
        dataItem.append(Nacionalidad(id:"OMN",nombre :"Omán"));
        dataItem.append(Nacionalidad(id:"HOL",nombre :"Países Bajos/Holanda"));
        dataItem.append(Nacionalidad(id:"PAK",nombre :"Pakistan"));
        dataItem.append(Nacionalidad(id:"PAL",nombre :"Palau"));
        dataItem.append(Nacionalidad(id:"PLE",nombre :"Palestina"));
        dataItem.append(Nacionalidad(id:"PAN",nombre :"Panamá"));
        dataItem.append(Nacionalidad(id:"PNG",nombre :"Papúa Nueva Guinea"));
        dataItem.append(Nacionalidad(id:"PAR",nombre :"Paraguay"));
        dataItem.append(Nacionalidad(id:"PER",nombre :"Perú"));
        dataItem.append(Nacionalidad(id:"PYF",nombre :"Polinesia Francesa"));
        dataItem.append(Nacionalidad(id:"POL",nombre :"Polonia"));
        dataItem.append(Nacionalidad(id:"POR",nombre :"Portugal"));
        dataItem.append(Nacionalidad(id:"PRI",nombre :"Puerto Rico"));
        dataItem.append(Nacionalidad(id:"QAT",nombre :"Qatar"));
        dataItem.append(Nacionalidad(id:"SYR",nombre :"República Árabe Siria"));
        dataItem.append(Nacionalidad(id:"CAF",nombre :"República Centroafricana"));
        dataItem.append(Nacionalidad(id:"RCH",nombre :"Republica checa"));
        dataItem.append(Nacionalidad(id:"RDO",nombre :"Republica Dominicana"));
        dataItem.append(Nacionalidad(id:"TZA",nombre :"República Unida de Tanzanía"));
        dataItem.append(Nacionalidad(id:"EU",nombre :"Reunión"));
        dataItem.append(Nacionalidad(id:"RWA",nombre :"Ruanda"));
        dataItem.append(Nacionalidad(id:"RUM",nombre :"Rumania"));
        dataItem.append(Nacionalidad(id:"RUS",nombre :"Rusia"));
        dataItem.append(Nacionalidad(id:"ESH",nombre :"Sáhara Occidental"));
        dataItem.append(Nacionalidad(id:"KNA",nombre :"Saint Kitts y Nevis"));
        dataItem.append(Nacionalidad(id:"WSM",nombre :"Samoa"));
        dataItem.append(Nacionalidad(id:"ASM",nombre :"Samoa Americana"));
        dataItem.append(Nacionalidad(id:"RSM",nombre :"San Marino"));
        dataItem.append(Nacionalidad(id:"SMP",nombre :"San Pedro y Miquelón"));
        dataItem.append(Nacionalidad(id:"VCT",nombre :"San Vicente y Las Granadinas"));
        dataItem.append(Nacionalidad(id:"SHN",nombre :"Santa Helena"));
        dataItem.append(Nacionalidad(id:"LCA",nombre :"Santa Lucía"));
        dataItem.append(Nacionalidad(id:"STP",nombre :"Santo Tomé y Príncipe"));
        dataItem.append(Nacionalidad(id:"SEN",nombre :"Senegal"));
        dataItem.append(Nacionalidad(id:"SER",nombre :"Serbia"));
        dataItem.append(Nacionalidad(id:"SLE",nombre :"Sierra Leone"));
        dataItem.append(Nacionalidad(id:"SLE",nombre :"Sierra Leone"));
        dataItem.append(Nacionalidad(id:"SIN",nombre :"Singapur"));
        dataItem.append(Nacionalidad(id:"SOM",nombre :"Somalia"));
        dataItem.append(Nacionalidad(id:"SRI",nombre :"Sri Lanka"));
        dataItem.append(Nacionalidad(id:"SWZ",nombre :"Suazilandia "));
        dataItem.append(Nacionalidad(id:"SDA",nombre :"Sudáfrica"));
        dataItem.append(Nacionalidad(id:"ZAF",nombre :"Sudáfrica"));
        dataItem.append(Nacionalidad(id:"SUE",nombre :"Suecia"));
        dataItem.append(Nacionalidad(id:"SUI",nombre :"Suiza"));
        dataItem.append(Nacionalidad(id:"CHE",nombre :"Suiza"));
        dataItem.append(Nacionalidad(id:"TAI",nombre :"Tailandia"));
        dataItem.append(Nacionalidad(id:"TAW",nombre :"Taiwán"));
        dataItem.append(Nacionalidad(id:"TJK",nombre :"Tayikistán"));
        dataItem.append(Nacionalidad(id:"IOT",nombre :"Territorio Británico del Océano Índico"));
        dataItem.append(Nacionalidad(id:"TLS",nombre :"Timor Oriental"));
        dataItem.append(Nacionalidad(id:"TGO",nombre :"Togo"));
        dataItem.append(Nacionalidad(id:"TKL",nombre :"Tokelau"));
        dataItem.append(Nacionalidad(id:"TON",nombre :"Tonga"));
        dataItem.append(Nacionalidad(id:"TYT",nombre :"Trinidad y Tobago"));
        dataItem.append(Nacionalidad(id:"TUN",nombre :"Túnez"));
        dataItem.append(Nacionalidad(id:"TKM",nombre :"Turkmenistán"));
        dataItem.append(Nacionalidad(id:"TUR",nombre :"Turquía"));
        dataItem.append(Nacionalidad(id:"TUV",nombre :"Tuvalu"));
        dataItem.append(Nacionalidad(id:"UCR",nombre :"Ucrania"));
        dataItem.append(Nacionalidad(id:"UGA",nombre :"Uganda"));
        dataItem.append(Nacionalidad(id:"URU",nombre :"Uruguay"));
        dataItem.append(Nacionalidad(id:"UZB",nombre :"Uzbekistán"));
        dataItem.append(Nacionalidad(id:"VUT",nombre :"Vanuatu"));
        dataItem.append(Nacionalidad(id:"VAT",nombre :"Vaticano"));
        dataItem.append(Nacionalidad(id:"VEN",nombre :"Venezuela"));
        dataItem.append(Nacionalidad(id:"VIE",nombre :"Vietnam"));
        dataItem.append(Nacionalidad(id:"YEM",nombre :"Yemen"));
        dataItem.append(Nacionalidad(id:"YIB",nombre :"Yibuti"));
        dataItem.append(Nacionalidad(id:"YUG",nombre :"Yugoslavia"));
        dataItem.append(Nacionalidad(id:"ZAM",nombre :"Zambia"));
        dataItem.append(Nacionalidad(id:"ZIM",nombre :"Zimbabue"));
        return dataItem
    }
    
    static func getNacionalidadEn() -> Array<Nacionalidad>{
         var dataItem:Array<Nacionalidad> = []
        dataItem.append(Nacionalidad(id:"AFG",nombre :"Afghanistan"));
        dataItem.append(Nacionalidad(id:"ALB",nombre :"Albania"));
        dataItem.append(Nacionalidad(id:"DZA",nombre :"Algeria"));
        dataItem.append(Nacionalidad(id:"ASM",nombre :"American Samoa"));
        dataItem.append(Nacionalidad(id:"AND",nombre :"Andorra"));
        dataItem.append(Nacionalidad(id:"ANG",nombre :"Angola"));
        dataItem.append(Nacionalidad(id:"AIA",nombre :"Anguilla "));
        dataItem.append(Nacionalidad(id:"ATA",nombre :"Antartica "));
        dataItem.append(Nacionalidad(id:"ATG",nombre :"Antigua and Barbuda "));
        dataItem.append(Nacionalidad(id:"ARG",nombre :"Argentina"));
        dataItem.append(Nacionalidad(id:"ARM",nombre :"Armenia"));
        dataItem.append(Nacionalidad(id:"ARU",nombre :"Aruba"));
        dataItem.append(Nacionalidad(id:"AUS",nombre :"Australia"));
        dataItem.append(Nacionalidad(id:"AUT",nombre :"Austria"));
        dataItem.append(Nacionalidad(id:"AZE",nombre :"Azerbaijan"));
        dataItem.append(Nacionalidad(id:"BAH",nombre :"Bahamas"));
        dataItem.append(Nacionalidad(id:"BHR",nombre :"Bahrain "));
        dataItem.append(Nacionalidad(id:"BAN",nombre :"Bangladesh"));
        dataItem.append(Nacionalidad(id:"BAR",nombre :"Barbados"));
        dataItem.append(Nacionalidad(id:"BIE",nombre :"Belarus"));
        dataItem.append(Nacionalidad(id:"BEL",nombre :"Belgium"));
        dataItem.append(Nacionalidad(id:"BLZ",nombre :"Belize"));
        dataItem.append(Nacionalidad(id:"BEN",nombre :"Benin"));
        dataItem.append(Nacionalidad(id:"BER",nombre :"Bermuda"));
        dataItem.append(Nacionalidad(id:"BTN",nombre :"Bhutan"));
        dataItem.append(Nacionalidad(id:"BOL",nombre :"Bolivia"));
        dataItem.append(Nacionalidad(id:"BIH",nombre :"Bosnia and Herzegowina "));
        dataItem.append(Nacionalidad(id:"BWA",nombre :"Botswana"));
        dataItem.append(Nacionalidad(id:"BVT",nombre :"Bouvet Island "));
        dataItem.append(Nacionalidad(id:"BRA",nombre :"Brasil"));
        dataItem.append(Nacionalidad(id:"IOT",nombre :"British Indian Ocean Territory "));
        dataItem.append(Nacionalidad(id:"VGB",nombre :"British Virgin Islands "));
        dataItem.append(Nacionalidad(id:"BRN",nombre :"Brunei Darussalam "));
        dataItem.append(Nacionalidad(id:"BUL",nombre :"Bulgaria"));
        dataItem.append(Nacionalidad(id:"BFA",nombre :"Burkina Faso "));
        dataItem.append(Nacionalidad(id:"BDI",nombre :"Burundi "));
        dataItem.append(Nacionalidad(id:"KHM",nombre :"Cambodia "));
        dataItem.append(Nacionalidad(id:"CMR",nombre :"Cameroon "));
        dataItem.append(Nacionalidad(id:"CAN",nombre :"Canada"));
        dataItem.append(Nacionalidad(id:"CPV",nombre :"Cape Verde "));
        dataItem.append(Nacionalidad(id:"CYM",nombre :"Cayman Islands "));
        dataItem.append(Nacionalidad(id:"CAF",nombre :"Central African Republic "));
        dataItem.append(Nacionalidad(id:"TCD",nombre :"Chad "));
        dataItem.append(Nacionalidad(id:"CHL",nombre :"Chile"));
        dataItem.append(Nacionalidad(id:"CHI",nombre :"China"));
        dataItem.append(Nacionalidad(id:"CXR",nombre :"Christmas Island "));
        dataItem.append(Nacionalidad(id:"CCK",nombre :"Cocos (Keeling) Islands "));
        dataItem.append(Nacionalidad(id:"COL",nombre :"Colombia"));
        dataItem.append(Nacionalidad(id:"COM",nombre :"Comoros "));
        dataItem.append(Nacionalidad(id:"COG",nombre :"Congo "));
        dataItem.append(Nacionalidad(id:"COK",nombre :"Cook Islands "));
        dataItem.append(Nacionalidad(id:"COS",nombre :"Costa Rica"));
        dataItem.append(Nacionalidad(id:"CRO",nombre :"Croatia"));
        dataItem.append(Nacionalidad(id:"CUB",nombre :"Cuba"));
        dataItem.append(Nacionalidad(id:"CHP",nombre :"Cyprus"));
        dataItem.append(Nacionalidad(id:"RCH",nombre :"Czech Republic"));
        dataItem.append(Nacionalidad(id:"COD",nombre :"Democratic Republic of the Congo"));
        dataItem.append(Nacionalidad(id:"DIN",nombre :"Denmark"));
        dataItem.append(Nacionalidad(id:"YIB",nombre :"Djibouti"));
        dataItem.append(Nacionalidad(id:"DOM",nombre :"Dominica"));
        dataItem.append(Nacionalidad(id:"TLS",nombre :"East Timor "));
        dataItem.append(Nacionalidad(id:"ECU",nombre :"Ecuador"));
        dataItem.append(Nacionalidad(id:"EGI",nombre :"Egypt"));
        dataItem.append(Nacionalidad(id:"ELS",nombre :"El Salvador"));
        dataItem.append(Nacionalidad(id:"GEC",nombre :"Equatorial Guinea"));
        dataItem.append(Nacionalidad(id:"ERI",nombre :"Eritrea "));
        dataItem.append(Nacionalidad(id:"EST",nombre :"Estonia"));
        dataItem.append(Nacionalidad(id:"ETI",nombre :"Ethiopia"));
        dataItem.append(Nacionalidad(id:"FLK",nombre :"Falkland Islands (Malvinas) "));
        dataItem.append(Nacionalidad(id:"FRO",nombre :"Faroe Islands "));
        dataItem.append(Nacionalidad(id:"FIY",nombre :"Fiji"));
        dataItem.append(Nacionalidad(id:"FI",nombre :"Finland"));
        dataItem.append(Nacionalidad(id:"FRA",nombre :"France"));
        dataItem.append(Nacionalidad(id:"GYF",nombre :"French Guiana"));
        dataItem.append(Nacionalidad(id:"PYF",nombre :"French Polynesia "));
        dataItem.append(Nacionalidad(id:"GAB",nombre :"Gabon"));
        dataItem.append(Nacionalidad(id:"GAM",nombre :"Gambia"));
        dataItem.append(Nacionalidad(id:"GEO",nombre :"Georgia"));
        dataItem.append(Nacionalidad(id:"ALE",nombre :"Germany"));
        dataItem.append(Nacionalidad(id:"GHA",nombre :"Ghana"));
        dataItem.append(Nacionalidad(id:"GIB",nombre :"Gibraltar"));
        dataItem.append(Nacionalidad(id:"GRE",nombre :"Greece"));
        dataItem.append(Nacionalidad(id:"GRL",nombre :"Greenland "));
        dataItem.append(Nacionalidad(id:"GRD",nombre :"Grenada "));
        dataItem.append(Nacionalidad(id:"GLP",nombre :"Guadeloupe "));
        dataItem.append(Nacionalidad(id:"GUM",nombre :"Guam "));
        dataItem.append(Nacionalidad(id:"GUA",nombre :"Guatemala"));
        dataItem.append(Nacionalidad(id:"GUY",nombre :"Guiana"));
        dataItem.append(Nacionalidad(id:"GNA",nombre :"Guinea"));
        dataItem.append(Nacionalidad(id:"GUI",nombre :"Guinea-Bissau"));
        dataItem.append(Nacionalidad(id:"HAI",nombre :"Haiti"));
        dataItem.append(Nacionalidad(id:"HMD",nombre :"Heard and Mcdonald Islands "));
        dataItem.append(Nacionalidad(id:"HON",nombre :"Honduras"));
        dataItem.append(Nacionalidad(id:"HKO",nombre :"Hong Kong"));
        dataItem.append(Nacionalidad(id:"HUN",nombre :"Hungary"));
        dataItem.append(Nacionalidad(id:"ISL",nombre :"Iceland"));
        dataItem.append(Nacionalidad(id:"ID",nombre :"India"));
        dataItem.append(Nacionalidad(id:"IDN",nombre :"Indonesia"));
        dataItem.append(Nacionalidad(id:"IRN",nombre :"Iran"));
        dataItem.append(Nacionalidad(id:"IRQ",nombre :"Iraq"));
        dataItem.append(Nacionalidad(id:"IRL",nombre :"Ireland"));
        dataItem.append(Nacionalidad(id:"ISR",nombre :"Israel"));
        dataItem.append(Nacionalidad(id:"ITA",nombre :"Italy"));
        dataItem.append(Nacionalidad(id:"CIV",nombre :"Ivory Coast"));
        dataItem.append(Nacionalidad(id:"JAM",nombre :"Jamaica"));
        dataItem.append(Nacionalidad(id:"JAP",nombre :"Japan"));
        dataItem.append(Nacionalidad(id:"JOR",nombre :"Jordan"));
        dataItem.append(Nacionalidad(id:"KAZ",nombre :"Kazakhstan"));
        dataItem.append(Nacionalidad(id:"KEN",nombre :"Kenya"));
        dataItem.append(Nacionalidad(id:"KRB",nombre :"Kiribati"));
        dataItem.append(Nacionalidad(id:"KOR",nombre :"Korea"));
        dataItem.append(Nacionalidad(id:"KUW",nombre :"Kuwait"));
        dataItem.append(Nacionalidad(id:"KRG",nombre :"Kyrgyzstan"));
        dataItem.append(Nacionalidad(id:"LAO",nombre :"Laos"));
        dataItem.append(Nacionalidad(id:"LAT",nombre :"Latvia"));
        dataItem.append(Nacionalidad(id:"LIB",nombre :"Lebanon"));
        dataItem.append(Nacionalidad(id:"LES",nombre :"Lesotho"));
        dataItem.append(Nacionalidad(id:"LET",nombre :"Letonia"));
        dataItem.append(Nacionalidad(id:"LBR",nombre :"Liberia"));
        dataItem.append(Nacionalidad(id:"LBA",nombre :"Libya"));
        dataItem.append(Nacionalidad(id:"LIE",nombre :"Liechtenstein"));
        dataItem.append(Nacionalidad(id:"LIT",nombre :"Lithuania"));
        dataItem.append(Nacionalidad(id:"LUX",nombre :"Luxembourg"));
        dataItem.append(Nacionalidad(id:"MAC",nombre :"Macao"));
        dataItem.append(Nacionalidad(id:"MCD",nombre :"Macedonia"));
        dataItem.append(Nacionalidad(id:"MAD",nombre :"Madagascar"));
        dataItem.append(Nacionalidad(id:"MLW",nombre :"Malawi"));
        dataItem.append(Nacionalidad(id:"MAL",nombre :"Malaysia"));
        dataItem.append(Nacionalidad(id:"MLD",nombre :"Maldives"));
        dataItem.append(Nacionalidad(id:"MLI",nombre :"Mali"));
        dataItem.append(Nacionalidad(id:"MLT",nombre :"Malta"));
        dataItem.append(Nacionalidad(id:"MRS",nombre :"Marshall Islands"));
        dataItem.append(Nacionalidad(id:"MAR",nombre :"Martinique"));
        dataItem.append(Nacionalidad(id:"MRT",nombre :"Mauritania"));
        dataItem.append(Nacionalidad(id:"MRC",nombre :"Mauritius"));
        dataItem.append(Nacionalidad(id:"MAY",nombre :"Mayotte"));
        dataItem.append(Nacionalidad(id:"MEX",nombre :"Mexico"));
        dataItem.append(Nacionalidad(id:"MIC",nombre :"Micronesia"));
        dataItem.append(Nacionalidad(id:"MOL",nombre :"Moldova"));
        dataItem.append(Nacionalidad(id:"MON",nombre :"Monaco"));
        dataItem.append(Nacionalidad(id:"MNG",nombre :"Mongolia "));
        dataItem.append(Nacionalidad(id:"MSR",nombre :"Montserrat "));
        dataItem.append(Nacionalidad(id:"MRR",nombre :"Morocco"));
        dataItem.append(Nacionalidad(id:"MOZ",nombre :"Mozambique"));
        dataItem.append(Nacionalidad(id:"MMR",nombre :"Myanmar/Birmania"));
        dataItem.append(Nacionalidad(id:"NAM",nombre :"Namibia "));
        dataItem.append(Nacionalidad(id:"NRU",nombre :"Nauru "));
        dataItem.append(Nacionalidad(id:"NEP",nombre :"Nepal"));
        dataItem.append(Nacionalidad(id:"HOL",nombre :"Netherlands"));
        dataItem.append(Nacionalidad(id:"NLD",nombre :"Netherlands"));
        dataItem.append(Nacionalidad(id:"AN",nombre :"Netherlands Antilles "));
        dataItem.append(Nacionalidad(id:"NCL",nombre :"New Caledonia "));
        dataItem.append(Nacionalidad(id:"NUE",nombre :"New Zealand"));
        dataItem.append(Nacionalidad(id:"NIC",nombre :"Nicaragua"));
        dataItem.append(Nacionalidad(id:"NER",nombre :"Niger "));
        dataItem.append(Nacionalidad(id:"NGA",nombre :"Nigeria "));
        dataItem.append(Nacionalidad(id:"NIU",nombre :"Niue "));
        dataItem.append(Nacionalidad(id:"NFK",nombre :"Norfolk Island "));
        dataItem.append(Nacionalidad(id:"CRN",nombre :"North Korea"));
        dataItem.append(Nacionalidad(id:"MNP",nombre :"Northern Mariana Islands "));
        dataItem.append(Nacionalidad(id:"NOR",nombre :"Norway"));
        dataItem.append(Nacionalidad(id:"OMN",nombre :"Oman "));
        dataItem.append(Nacionalidad(id:"PAK",nombre :"Pakistan"));
        dataItem.append(Nacionalidad(id:"PAL",nombre :"Palau"));
        dataItem.append(Nacionalidad(id:"PLE",nombre :"Palestine"));
        dataItem.append(Nacionalidad(id:"PAN",nombre :"Panama"));
        dataItem.append(Nacionalidad(id:"PNG",nombre :"Papua New Guinea "));
        dataItem.append(Nacionalidad(id:"PAR",nombre :"Paraguay"));
        dataItem.append(Nacionalidad(id:"PER",nombre :"Peru"));
        dataItem.append(Nacionalidad(id:"FIL",nombre :"Philippines"));
        dataItem.append(Nacionalidad(id:"PCN",nombre :"Pitcairn "));
        dataItem.append(Nacionalidad(id:"POL",nombre :"Poland"));
        dataItem.append(Nacionalidad(id:"POR",nombre :"Portugal"));
        dataItem.append(Nacionalidad(id:"PRI",nombre :"Puerto Rico"));
        dataItem.append(Nacionalidad(id:"QAT",nombre :"Qatar "));
        dataItem.append(Nacionalidad(id:"RDO",nombre :"Republica Dominicana"));
        dataItem.append(Nacionalidad(id:"EU",nombre :"Reunion "));
        dataItem.append(Nacionalidad(id:"RUM",nombre :"Romania"));
        dataItem.append(Nacionalidad(id:"RUS",nombre :"Russia"));
        dataItem.append(Nacionalidad(id:"RWA",nombre :"Rwanda "));
        dataItem.append(Nacionalidad(id:"SHN",nombre :"Saint Helena "));
        dataItem.append(Nacionalidad(id:"KNA",nombre :"Saint Kitts and Nevis "));
        dataItem.append(Nacionalidad(id:"LCA",nombre :"Saint Lucia "));
        dataItem.append(Nacionalidad(id:"SMP",nombre :"Saint Pierre and Miquelon "));
        dataItem.append(Nacionalidad(id:"VCT",nombre :"Saint Vincent and The Grenadines "));
        dataItem.append(Nacionalidad(id:"WSM",nombre :"Samoa "));
        dataItem.append(Nacionalidad(id:"RSM",nombre :"San Marino"));
        dataItem.append(Nacionalidad(id:"STP",nombre :"Sao Tome and Principe "));
        dataItem.append(Nacionalidad(id:"ARA",nombre :"Saudi Arabia"));
        dataItem.append(Nacionalidad(id:"SEN",nombre :"Senegal"));
        dataItem.append(Nacionalidad(id:"SER",nombre :"Serbia"));
        dataItem.append(Nacionalidad(id:"SLE",nombre :"Sierra Leone "));
        dataItem.append(Nacionalidad(id:"SIN",nombre :"Singapore"));
        dataItem.append(Nacionalidad(id:"ESQ",nombre :"Slovakia"));
        dataItem.append(Nacionalidad(id:"ESL",nombre :"Slovenia"));
        dataItem.append(Nacionalidad(id:"SOM",nombre :"Somalia "));
        dataItem.append(Nacionalidad(id:"SDA",nombre :"South Africa"));
        dataItem.append(Nacionalidad(id:"ZAF",nombre :"South Africa "));
        dataItem.append(Nacionalidad(id:"CRS",nombre :"South Korea"));
        dataItem.append(Nacionalidad(id:"ESP",nombre :"Spain"));
        dataItem.append(Nacionalidad(id:"SRI",nombre :"Sri Lanka"));
        dataItem.append(Nacionalidad(id:"SDN",nombre :"Sudan "));
        dataItem.append(Nacionalidad(id:"SJM",nombre :"Svalbard and Jan Mayen Islands "));
        dataItem.append(Nacionalidad(id:"SWZ",nombre :"Swaziland "));
        dataItem.append(Nacionalidad(id:"SUE",nombre :"Sweden"));
        dataItem.append(Nacionalidad(id:"SUI",nombre :"Switzerland"));
        dataItem.append(Nacionalidad(id:"CHE",nombre :"Switzerland "));
        dataItem.append(Nacionalidad(id:"SYR",nombre :"Syrian Arab Republic "));
        dataItem.append(Nacionalidad(id:"TAW",nombre :"Taiwan"));
        dataItem.append(Nacionalidad(id:"TJK",nombre :"Tajikistan "));
        dataItem.append(Nacionalidad(id:"TAI",nombre :"Thailand"));
        dataItem.append(Nacionalidad(id:"TGO",nombre :"Togo "));
        dataItem.append(Nacionalidad(id:"TKL",nombre :"Tokelau "));
        dataItem.append(Nacionalidad(id:"TON",nombre :"Tonga "));
        dataItem.append(Nacionalidad(id:"TYT",nombre :"Trinidad and Tobago"));
        dataItem.append(Nacionalidad(id:"TUN",nombre :"Tunisia"));
        dataItem.append(Nacionalidad(id:"TUR",nombre :"Turkey"));
        dataItem.append(Nacionalidad(id:"TKM",nombre :"Turkmenistan "));
        dataItem.append(Nacionalidad(id:"TCA",nombre :"Turks and Caicos Islands "));
        dataItem.append(Nacionalidad(id:"TUV",nombre :"Tuvalu "));
        dataItem.append(Nacionalidad(id:"UGA",nombre :"Uganda"));
        dataItem.append(Nacionalidad(id:"UCR",nombre :"Ukraine"));
        dataItem.append(Nacionalidad(id:"EMI",nombre :"United Arab Emirates"));
        dataItem.append(Nacionalidad(id:"ING",nombre :"United Kingdom"));
        dataItem.append(Nacionalidad(id:"GRA",nombre :"United Kingdom"));
        dataItem.append(Nacionalidad(id:"TZA",nombre :"United Republic of Tanzania "));
        dataItem.append(Nacionalidad(id:"UMI",nombre :"United States Minor Outlaying Islands "));
        dataItem.append(Nacionalidad(id:"VIR",nombre :"United States Virgin Islands "));
        dataItem.append(Nacionalidad(id:"URU",nombre :"Uruguay"));
        dataItem.append(Nacionalidad(id:"EUA",nombre :"USA / United States"));
        dataItem.append(Nacionalidad(id:"UZB",nombre :"Uzbekistan "));
        dataItem.append(Nacionalidad(id:"VUT",nombre :"Vanuatu "));
        dataItem.append(Nacionalidad(id:"VAT",nombre :"Vatican City"));
        dataItem.append(Nacionalidad(id:"VEN",nombre :"Venezuela"));
        dataItem.append(Nacionalidad(id:"VIE",nombre :"Vietnam"));
        dataItem.append(Nacionalidad(id:"WLF",nombre :"Wallis and Futuna Islands "));
        dataItem.append(Nacionalidad(id:"ESH",nombre :"Western Sahara "));
        dataItem.append(Nacionalidad(id:"YEM",nombre :"Yemen"));
        dataItem.append(Nacionalidad(id:"YUG",nombre :"Yugoslavia"));
        dataItem.append(Nacionalidad(id:"ZAM",nombre :"Zambia"));
        dataItem.append(Nacionalidad(id:"ZIM",nombre :"Zimbabwe"));
        return dataItem
    }
    
    static func getAlert(title: String, message: String, titleAction: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: titleAction, style: UIAlertAction.Style.default, handler: nil))
        return alert
    }
    
    static func showAlertWithTitle(_ title:String?, message:String) -> UIAlertController{
        let alertController = UIAlertController(title: title ?? "", message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(okAction)
        return alertController
    }
    
    static func tfSetError(field: UITextField, placeholder: String, focus: Bool = true) {
        field.layer.borderColor = UIColor.red.cgColor
        field.layer.borderWidth = 1
        field.layer.cornerRadius = 5
        field.attributedPlaceholder = NSAttributedString(string: placeholder, attributes:[NSAttributedString.Key.foregroundColor: hexStringToUIColor(hex: "ED8277")])
        field.textColor = UIColor.red
        if(focus) {
            field.becomeFirstResponder()
        }
    }
    
    static func tfClearError(field: UITextField) {
        field.layer.borderWidth = 1
        field.clipsToBounds = true
        field.layer.cornerRadius = 5
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor.gray.cgColor
        field.textColor = hexStringToUIColor(hex: "000000")
        field.attributedPlaceholder = NSAttributedString(string: "Campo requerido", attributes:[NSAttributedString.Key.foregroundColor: hexStringToUIColor(hex: "E0E0E0")])
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func showActivityIndicatorV2(uiView: UIView, title: String) {
        container.frame = uiView.frame
        container.center = uiView.center
        container = UIView(frame: UIScreen.main.bounds)
        container.backgroundColor = UIColorFromHex(rgbValue: 0x000000, alpha: 0.6)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 280, height: 80)
        loadingView.center = uiView.center
        loadingView.clipsToBounds = true
        
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.center.x = loadingView.frame.size.width / 2
        
        labelIndicator.frame = CGRect(x: 0, y: 55, width: 280, height: 25)
        labelIndicator.text = title
        labelIndicator.textAlignment = .center
        labelIndicator.textColor = hexStringToUIColor(hex: "FFFFFF")
        
        loadingView.addSubview(activityIndicator)
        loadingView.addSubview(labelIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    static func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    static func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    static func crearDocumentoFile(nombre:String) -> URL {
        var ojecCtBool:ObjCBool = true
        let isExit = FileManager.default.fileExists(atPath: destinationPath,isDirectory:&ojecCtBool)
        if isExit == false {
            do{
                try FileManager.default.createDirectory(atPath: destinationPath, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Error al crear carpeta")
            }
        }
        let documentsURL = URL(fileURLWithPath: destinationPath, isDirectory: true)
        let fileURL = documentsURL.appendingPathComponent(nombre)
        return fileURL
    }
    
    static func splitString(nombre:String)->String{
        let fullName  = nombre
        let fullNameArr = fullName.components(separatedBy: "/")
        let name = fullNameArr[0]
        return name
    }
    
    static func eliminarDocumentoFile(documentoPath:String){
        let documentsURL = URL(fileURLWithPath: destinationPath, isDirectory: true)
        do {
            let fileManager = FileManager.default
            try fileManager.removeItem(atPath: documentsURL.appendingPathComponent(documentoPath).path)
        } catch let error as NSError {
            print("Ooops! Ha ocurrido un error: \(error)")
        } // catch
    }
    
    static func getDirectory() -> String {
        return destinationPath
    }
    
   static func validarIdioma()->Bool{
        return Locale.current.languageCode?.contains("es") ?? false
    }
}

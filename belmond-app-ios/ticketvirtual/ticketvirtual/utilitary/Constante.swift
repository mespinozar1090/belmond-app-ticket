//
//  Constante.swift
//  ticketvirtual
//
//  Created by usuario on 4/13/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import Foundation

class Constante {
    static var MODIFICADO : String = "M"
    static var INVALIDO : String = "I"
    static var VALIDO : String = "V"
    static var ISMODIFICADO : String =  "S"
}

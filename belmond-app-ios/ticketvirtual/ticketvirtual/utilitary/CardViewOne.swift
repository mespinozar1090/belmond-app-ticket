//
//  CardViewOne.swift
//  smartboleta suite
//
//  Created by INDIGITAL on 11/13/19.
//  Copyright © 2019 Indigital. All rights reserved.
//

import UIKit

@IBDesignable class CardViewOne: UIView {
    var cornerradius : CGFloat = 10
    var shadowOffSetWidth : CGFloat = 0.0
    var shadowOffSetHeight : CGFloat = 0.0
    var shadowColor : UIColor = UIColor.black
    var shadowOpacity : CGFloat = 0.0
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerradius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: shadowOffSetWidth, height: shadowOffSetHeight)
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerradius)
        
        layer.shadowPath = shadowPath.cgPath
        layer.shadowOpacity = Float(shadowOpacity)
    }
    
}

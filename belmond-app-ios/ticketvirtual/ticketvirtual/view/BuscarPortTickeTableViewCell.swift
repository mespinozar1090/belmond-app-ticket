//
//  BuscarPortTickeTableViewCell.swift
//  ticketvirtual
//
//  Created by usuario on 4/22/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class BuscarPortTickeTableViewCell: UITableViewCell {

    @IBOutlet weak var tfNroTicket: UITextField!
    @IBOutlet weak var btnDelete: UIButton!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        btnDelete.layer.cornerRadius = btnDelete.frame.width/2
    }
    
    public func configure(text: String?, placeholder: String) {
            tfNroTicket.text = text
            tfNroTicket.placeholder = placeholder
            tfNroTicket.accessibilityValue = text
            tfNroTicket.accessibilityLabel = placeholder
       }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

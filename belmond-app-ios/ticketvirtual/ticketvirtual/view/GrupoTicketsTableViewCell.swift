//
//  GrupoTicketsTableViewCell.swift
//  ticketvirtual
//
//  Created by usuario on 4/15/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class GrupoTicketsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPasajero: UILabel!
    @IBOutlet weak var lblRuta: UILabel!
    @IBOutlet weak var lblHora: UILabel!
    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblNroTicket: UILabel!
    @IBOutlet weak var btnEliminar: ButtonRadius!
    @IBOutlet weak var btnDetalle: ButtonRadius!
    @IBOutlet weak var vFranja: UIView!
    @IBOutlet weak var imgQr: UIImageView!
    @IBOutlet weak var lblUpdate: UILabel!
    @IBOutlet weak var btnQR: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardQR: UIView!
    
    let radius = 10.0
     override func awakeFromNib() {
            super.awakeFromNib()
            vFranja.layer.cornerRadius = 10
            vFranja.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
            cardView.layer.cornerRadius = 10
            cardView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            customButtom()
        }
        
        private func customButtom(){
            btnDetalle.contentMode = .center
            btnDetalle.imageView?.contentMode = .scaleAspectFit
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

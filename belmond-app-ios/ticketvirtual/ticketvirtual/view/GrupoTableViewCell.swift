//
//  GrupoTableViewCell.swift
//  ticketvirtual
//
//  Created by usuario on 4/14/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class GrupoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var btnEliminar: ButtonRadius!
    @IBOutlet weak var btnDetalle: ButtonRadius!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

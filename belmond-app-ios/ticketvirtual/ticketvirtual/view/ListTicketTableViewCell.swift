//
//  ListTicketTableViewCell.swift
//  ticketvirtual
//
//  Created by usuario on 2/26/20.
//  Copyright © 2020 usuario. All rights reserved.
//

import UIKit

class ListTicketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPasajero: UILabel!
    @IBOutlet weak var lblRuta: UILabel!
    @IBOutlet weak var lblHora: UILabel!
    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblNroTicket: UILabel!
    @IBOutlet weak var btnEliminar: ButtonRadius!
    @IBOutlet weak var btnDetalle: ButtonRadius!
    @IBOutlet weak var vFranja: UIView!
    @IBOutlet weak var imgQr: UIImageView!
    @IBOutlet weak var lblUpdate: UILabel!
    @IBOutlet weak var btnQR: UIButton!
    @IBOutlet weak var vCard: UIView!
    @IBOutlet weak var cardQR: UIView!
    
    let radius = 10.0
  
    override func awakeFromNib() {
        super.awakeFromNib()

        vFranja.layer.cornerRadius = 10
        vFranja.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        vCard.layer.cornerRadius = 10
        vCard.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        customButtom()
    }
    
    private func customButtom(){
        btnDetalle.contentMode = .center
        btnDetalle.imageView?.contentMode = .scaleAspectFit
//        btnEliminar.setImage(UIImage(named: "ico_buscar"), for: .normal)
//        btnEliminar.contentVerticalAlignment = .fill
//        btnEliminar.contentHorizontalAlignment = .fill
//        btnEliminar.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

package com.mdp.perurail.network.repository;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.mdp.perurail.network.response.AutentticacionResponse;
import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.utilitary.methods.Constans;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutenticacionRepository {
    private APIService apiService;

    public MutableLiveData<AutentticacionResponse> autenticacion(String Uid){
        final MutableLiveData<AutentticacionResponse> autentticacionResponseMutableLiveData = new MutableLiveData<>();
        apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<AutentticacionResponse> grupoPorLoginResponseCall = apiService.autenticacion(Uid);

        grupoPorLoginResponseCall.enqueue(new Callback<AutentticacionResponse>() {
            @Override
            public void onResponse(Call<AutentticacionResponse> call, Response<AutentticacionResponse> response) {
                if(response.isSuccessful() && response.body()!= null){
                    autentticacionResponseMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<AutentticacionResponse> call, Throwable t) {

            }
        });

        return  autentticacionResponseMutableLiveData;
    }
}


package com.mdp.perurail.ui.fragment.group;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.perurail.R;
import com.mdp.perurail.database.model.Grupo;
import com.mdp.perurail.database.model.Ticket;
import com.mdp.perurail.dto.GrupoDTO;
import com.mdp.perurail.dto.TicketDTO;
import com.mdp.perurail.ui.activity.HomeActivity;
import com.mdp.perurail.ui.adapter.GrupoTicketAdapter;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.viewModel.GrupoTicketViewModel;
import com.mdp.perurail.viewModel.MyTicketsViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GroupPassengersFragment extends Fragment {

    private GrupoTicketViewModel grupoTicketViewModel;
    private MyTicketsViewModel myTicketsViewModel;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private GrupoTicketAdapter adaptador;
    private FragmentManager fragmentManager;
    private List<GrupoDTO> lista;
    private EditText etdBuscar;
    private boolean isValidar = true;
    private TextView txtNoGrupo;


    public static GroupPassengersFragment newInstance() {
        return new GroupPassengersFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group_passengers, container, false);
        initView(view);
        setToolbar(view);
        ((HomeActivity) getActivity()).bottomBar.setVisibility(View.VISIBLE);
        return view;
    }

    private void initView(View view) {
        txtNoGrupo = view.findViewById(R.id.txtNoGrupo);
        recyclerview = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(linearLayout);
        etdBuscar = view.findViewById(R.id.etdBuscar);
        lista = new ArrayList<>();

        adaptador = new GrupoTicketAdapter(getContext(), lista);
        recyclerview.setAdapter(adaptador);

        etdBuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence editable, int i, int i1, int i2) {
                String aux = editable.toString().trim();
                Log.e("aux", " : " + aux);
                if (!aux.equals("")) {
                    ArrayList<GrupoDTO> newLista = new ArrayList<>();
                    for (GrupoDTO equiposMap : lista) {
                        String nombre = equiposMap.getName();
                        if (nombre.toLowerCase().contains(aux.toLowerCase())) {
                            newLista.add(equiposMap);
                        }
                    }
                    txtNoGrupo.setVisibility(newLista.size()==0 ? View.VISIBLE : View.GONE);
                    adaptador.setFilter(newLista);
                } else {
                    adaptador.setGrupos(lista);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.btnDetalle) {
                BelmondUtil.setFragment(getActivity(), new GroupDetailsTicketFragment().newInstance(adaptador.getGrupoAt(positionL)), null);
            } else if (vL.getId() == R.id.btnEliminar) {
                showModelDeleteGroup(adaptador.getGrupoAt(positionL));
            }
        });
    }


    private void showModelDeleteGroup(GrupoDTO grupoDTO) {
        View view = getLayoutInflater().inflate(R.layout.dialog_delete_group, null);
        AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        TextView txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        Button btnAceptar = (Button) view.findViewById(R.id.btnAceptar);
        Button btnCancelar = (Button) view.findViewById(R.id.btnCancelar);

        txtNombre.setText(grupoDTO.getName());

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (BelmondUtil.getDeviceMetrics(getContext()).heightPixels * 0.8);
        dialog.show();

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Ticket> olista = myTicketsViewModel.getTicketsByGrupo(grupoDTO.getId());
                for (Ticket ticket : olista) {
                    Log.e("ELIMINAR", " TICKET ELIMINADOS " + ticket.getNumeroTicket());
                    eliminarFichero(ticket);
                }
                grupoTicketViewModel.delete(BelmondUtil.castToGrupo(grupoDTO));
                myTicketsViewModel.deleteAllTicketByGrupo(grupoDTO.getId());
                dialog.dismiss();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void eliminarFichero(Ticket ticketDTO) {
        if (null != ticketDTO.getRutaArchivoOnboarding() && !ticketDTO.getRutaArchivoOnboarding().equals("")) {
            BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoOnboarding()));
        }

        if (null != ticketDTO.getRutaArchivoDocumentoElectronico() && !ticketDTO.getRutaArchivoDocumentoElectronico().equals("")) {
            BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoDocumentoElectronico()));
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myTicketsViewModel = ViewModelProviders.of(this).get(MyTicketsViewModel.class);
        grupoTicketViewModel = ViewModelProviders.of(this).get(GrupoTicketViewModel.class);
        grupoTicketViewModel.getAllGrupos().observe(this, new Observer<List<Grupo>>() {
            @Override
            public void onChanged(List<Grupo> grupos) {
                lista.clear();
                if (grupos.size() > 0) {
                    List<GrupoDTO> listaGrupo = BelmondUtil.castListGroupModel(grupos);
                    lista.addAll(listaGrupo);
                    adaptador.setGrupos(listaGrupo);
                    onclickTable();
                    txtNoGrupo.setVisibility(View.GONE);
                } else {//
                    List<GrupoDTO> lista = new ArrayList<>();
                    adaptador.setGrupos(lista);
                    showModalCreateGroup();
                    txtNoGrupo.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.tvDescription);
        title.setText(R.string.crear);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_atras, null);
            drawable = DrawableCompat.wrap(drawable);
            //  DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorNegro));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_crear, menu);
    }


    private void showModalCreateGroup() {
        View view = getLayoutInflater().inflate(R.layout.dialog_create_group, null);
        AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        TextInputLayout tilNombregrupo = view.findViewById(R.id.tilNombregrupo);
        TextInputEditText tietNombregrupo = view.findViewById(R.id.tietNombregrupo);
        Button btnCrear = view.findViewById(R.id.btnCrear);

        ImageButton btnclose = (ImageButton) view.findViewById(R.id.btnclose);


        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (BelmondUtil.getDeviceMetrics(getContext()).heightPixels * 0.8);
        dialog.show();//mostrar

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isValidar = false;
            }
        });

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tietNombregrupo.getText().toString().trim().isEmpty()) {
                    if(!BelmondUtil.validarNombreGrupo(tietNombregrupo.getText().toString())){
                        Log.e("GRUPO"," tienes expresiones regulares");
                        Grupo oGrupo = new Grupo();
                        oGrupo.setNombreGrupo(tietNombregrupo.getText().toString());
                        grupoTicketViewModel.insert(oGrupo); //
                        dialog.dismiss(); // cierra
                    }else{
                        tilNombregrupo.setError(getString(R.string.caracter_especial));
                        tietNombregrupo.requestFocus(); //focus
                    }
                } else {
                    tilNombregrupo.setError(getString(R.string.documeto_invaldo));
                    tietNombregrupo.requestFocus(); //focus
                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            case R.id.accion_crear:
                showModalCreateGroup();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

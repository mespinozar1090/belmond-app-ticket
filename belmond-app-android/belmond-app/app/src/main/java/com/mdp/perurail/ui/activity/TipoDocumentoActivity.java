package com.mdp.perurail.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.mdp.perurail.R;
import com.mdp.perurail.ui.fragment.group.GroupDetailsTicketFragment;
import com.mdp.perurail.ui.fragment.person.MyTicketsFragment;

import java.io.Serializable;
import java.util.ArrayList;

public class TipoDocumentoActivity extends AppCompatActivity {

    private TextView btnCerrar;
    private ListView lista;
    private SearchView searchView;
    private ArrayList<String> listaPersonas;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento);
        btnCerrar = (TextView) findViewById(R.id.btnclose);
        searchView = (SearchView) findViewById(R.id.searh);
        lista = (ListView) findViewById(R.id.lista);


        String[] tipodocumento = getResources().getStringArray(R.array.lista_tipo_documento);
        final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, tipodocumento );
        lista.setAdapter(adapter);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String texto) {
                adapter.getFilter().filter(texto);

                return false;
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(),(String)lista.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                enviarData((String) lista.getItemAtPosition(position),(int) lista.getItemIdAtPosition(position));
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void enviarData(String item, int position) {
        Intent resultIntent=new Intent(TipoDocumentoActivity.this, MyTicketsFragment.class);
        resultIntent.putExtra("item",item);
        resultIntent.putExtra("position",position);
        setResult(RESULT_OK,resultIntent);
        Toast.makeText(this, "enviando datos"+item+""+position, Toast.LENGTH_SHORT).show();
        finish();//finishing activity

    }

}



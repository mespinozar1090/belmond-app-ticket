package com.mdp.perurail.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.DestinoDTO;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DestinoRutasAdapter extends RecyclerView.Adapter<DestinoRutasAdapter.ViewHolderDestinos> {

    List<DestinoDTO> listaDestinos;

    public DestinoRutasAdapter(List<DestinoDTO> listaDestinos) {
        this.listaDestinos = listaDestinos;
    }

    @NonNull
    @Override
    public DestinoRutasAdapter.ViewHolderDestinos onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_destino_ruta,viewGroup,false);
        return new ViewHolderDestinos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DestinoRutasAdapter.ViewHolderDestinos holder, int position) {
        holder.txtdestino.setText(listaDestinos.get(position).getDestino());
        holder.txtruta.setText(listaDestinos.get(position).getRuta());
    }

    @Override
    public int getItemCount() {
        return listaDestinos.size();
    }

    public class ViewHolderDestinos extends RecyclerView.ViewHolder {
        TextView txtdestino, txtruta;

        public ViewHolderDestinos(@NonNull View itemView) {
            super(itemView);
            txtdestino = (TextView)itemView.findViewById(R.id.txtTitlePasajero);
            txtruta =  (TextView)itemView.findViewById(R.id.txtNombre);

        }
    }
}

package com.mdp.perurail.network.response;

public class AutentticacionResponse {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

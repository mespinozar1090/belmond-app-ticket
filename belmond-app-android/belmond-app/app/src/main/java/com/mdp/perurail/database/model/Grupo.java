package com.mdp.perurail.database.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "grupo_table")
public class Grupo {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String nombreGrupo;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }
}

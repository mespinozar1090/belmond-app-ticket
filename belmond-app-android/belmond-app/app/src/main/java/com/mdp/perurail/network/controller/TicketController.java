package com.mdp.perurail.network.controller;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mdp.perurail.network.repository.TicketRespositoryWs;
import com.mdp.perurail.network.response.TicketResponse;

import java.util.List;

public class TicketController extends ViewModel {

    public List<TicketResponse> listaTicketPorDocumento(String codPais, String tipDoc, String docIde, String tipAcc, Context context){
        return  TicketRespositoryWs.getInstance().listaTicketPorDocumento(codPais,tipDoc,docIde,tipAcc,context);
    }
}

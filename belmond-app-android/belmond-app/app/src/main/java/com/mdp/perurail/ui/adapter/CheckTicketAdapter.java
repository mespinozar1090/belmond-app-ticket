package com.mdp.perurail.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.TicketDTO;
import com.mdp.perurail.utilitary.methods.BelmondUtil;

import java.util.ArrayList;
import java.util.List;

public class CheckTicketAdapter extends  RecyclerView.Adapter<CheckTicketAdapter.ViewHolder> {
    private Context context;
    private List<TicketDTO> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public CheckTicketAdapter(Context context, List<TicketDTO> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_ticket, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(context, lista.get(position));
    }

    @Override
    public int getItemCount() {
      return lista.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView txtNombre,txtRuta,txtTicket,txtHora,txtSalida;
        private ImageButton btnDetalle,btnGuardar;
        private ImageView imgQR;
        private View estadoTicket;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtNombre = (TextView) view.findViewById(R.id.txtNombre);
            txtRuta = (TextView) view.findViewById(R.id.txtRuta);
            txtTicket = (TextView) view.findViewById(R.id.txtTicket);
            txtSalida = (TextView) view.findViewById(R.id.txtSalida);
            txtHora = (TextView) view.findViewById(R.id.txtHora);
            btnDetalle = (ImageButton) view.findViewById(R.id.btnDetalle);
            btnGuardar = (ImageButton)view.findViewById(R.id.btnGuardar);
            imgQR = (ImageView) view.findViewById(R.id.imgQR);
            estadoTicket = (View) view.findViewById(R.id.estadoTicket);
        }

        public void bind(Context context, TicketDTO item) {

            txtNombre.setText(item.getNombrePasajero());
            txtRuta.setText(item.getRuta());
            txtTicket.setText(item.getNumeroTicket());
            txtSalida.setText(item.getFechaSalida());
            txtHora.setText(item.getHoraSalida());


            if(item != null){
                if(item.getCodigoHash()!=null){
                    if(!item.getCodigoHash().isEmpty()) {
                        imgQR.setImageBitmap(BelmondUtil.createQR(item.getCodigoHash()));
                        imgQR.setOnClickListener(this);
                        imgQR.setVisibility(View.VISIBLE);
                    }else{
                        imgQR.setVisibility(View.INVISIBLE);
                    }
                }
            }

            btnDetalle.setOnClickListener(this);

            btnGuardar.setOnClickListener(this);

            if(!item.getEstadoModificacion().equals("") && item.getEstadoModificacion().equals("M")){
                estadoTicket.setBackgroundResource(R.color.colorVerde);
            }
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }
    public interface ClickListener {
        void onItemClick(int position, View v);
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        CheckTicketAdapter.clickListener = clickListener;
    }
}

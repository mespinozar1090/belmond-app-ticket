package com.mdp.perurail.ui.fragment.check;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.perurail.R;
import com.mdp.perurail.database.model.Ticket;
import com.mdp.perurail.dto.GrupoDTO;
import com.mdp.perurail.network.response.TicketResponse;
import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.ui.activity.CheckTicketActivity;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.utilitary.methods.Constans;
import com.mdp.perurail.viewModel.MyTicketsViewModel;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckTicketNumberFragment extends Fragment {
    private static final String OBJECT_GRUPODTO = "grupoDTO";

    private MyTicketsViewModel myTicketsViewModel;

    TextInputEditText tietnumerodocumento;
    TextInputLayout tilnumerodocumento;
    ImageButton add_field_button;
    ImageButton delete_button;
    Button btn_buscar;
    LinearLayout parentLinearLayout;
    List<String> listNumeroTicket = new ArrayList<>();
    List<Ticket> listTicket = new ArrayList<>();
    ArrayList<String> invalidos = new ArrayList<String>();
    String ticketInvalido = null;
    private Integer sizeLista = 0;
    private Integer contador = 0;

    private GrupoDTO grupoDTO;

    public static CheckTicketNumberFragment newInstance(GrupoDTO grupoDTO) {
        CheckTicketNumberFragment fragment = new CheckTicketNumberFragment();
        Bundle args = new Bundle();
        args.putSerializable(OBJECT_GRUPODTO, grupoDTO);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            grupoDTO = (GrupoDTO) getArguments().getSerializable(OBJECT_GRUPODTO);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_ticket_number, container, false);

        ImageButton add_field_button = (ImageButton) view.findViewById(R.id.add_field_button);
        Button btn_buscar = (Button) view.findViewById(R.id.btnBuscar);
        LinearLayout parenLinearLayout = (LinearLayout) view.findViewById(R.id.parenLinearLayout);

        add_field_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View addView = layoutInflater.inflate(R.layout.row, null);
                ImageButton delete_button = (ImageButton) addView.findViewById(R.id.delete_buttons);


                final View.OnClickListener thisListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((LinearLayout) addView.getParent()).removeView(addView);
                    }
                };

                delete_button.setOnClickListener(thisListener);

                parenLinearLayout.addView(addView);
            }
        });


        btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int childCount = parenLinearLayout.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View thisChild = parenLinearLayout.getChildAt(i);
                    TextInputLayout tilnumerodocumento = (TextInputLayout) thisChild.findViewById(R.id.tilnumerodocumento);
                    TextInputEditText tietnumerodocumento = (TextInputEditText) thisChild.findViewById(R.id.tietnumerodocumento);
                    if (!tietnumerodocumento.getText().toString().trim().isEmpty()) {
                        listNumeroTicket.add(tietnumerodocumento.getText().toString().trim());
                    } else {
                        tilnumerodocumento.setError(getString(R.string.campo_obligatorio));
                        tietnumerodocumento.requestFocus();
                    }
                }
                buscarTicketPorNumeros(listNumeroTicket);
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myTicketsViewModel = ViewModelProviders.of(this).get(MyTicketsViewModel.class);
    }

    private void buscarTicketPorNumeros(List<String> olista) {
        sizeLista = olista.size();
        if (olista.size() > 0) {
            BelmondUtil.showProgressDialogBelmond(getActivity(), getString(R.string.validando));
            for (String numeroTicket : olista) {
                consultarTicketWs(numeroTicket, Constans.CONSULTA);
            }
        }

    }

    public void consultarTicketWs(String numTic, String tipAcc) {
        APIService apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL, getContext()).create(APIService.class);
        Call<List<TicketResponse>> listCall = apiService.getTicketActualiado("", "", "", numTic, tipAcc, "belmond");
        listCall.enqueue(new Callback<List<TicketResponse>>() {
            @Override
            public void onResponse(Call<List<TicketResponse>> call, Response<List<TicketResponse>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    contador = contador + 1;
                    if (response.body().size() > 0) {
                        for (Ticket oTicket : BelmondUtil.castTicketResponseToTickeDB(response.body())) {
                            Ticket data = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), oTicket.getNumeroTicket());
                            if (null == data) {
                                oTicket.setIdGrupo(grupoDTO.getId());
                                myTicketsViewModel.insert(oTicket);
                                listTicket.add(oTicket); //lista contien tickets
                            } else {
                                Log.e("DATA", "EL numero ya se encuentra registrado no debe agregar : " + data.getNumeroTicket());
                            }
                        }
                        descargarTickets(listTicket);
                    } else {
                        Log.e("Error", "Numero de Ticket invalido" + response.body() + "  " + numTic);
                        invalidos.add(numTic);
                        Log.e("TEST", sizeLista + "es igual a " + contador);
                        if (sizeLista == contador) {
                            ((CheckTicketActivity) getActivity()).sendData(invalidos);
                            BelmondUtil.hideProgreesDialogBelmond();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TicketResponse>> call, Throwable t) {
                BelmondUtil.hideProgreesDialogBelmond();
                Toast.makeText(getActivity(), "Sin conexion con el servidor...", Toast.LENGTH_SHORT).show();
                Log.e("LOGGER", "listaTicketPorDocumento-onFailure" + t.getMessage());
            }
        });
    }

    private void descargarTickets(List<Ticket> numerosTickets) {
        if (numerosTickets.size() > 0) {
            for (Ticket oData : numerosTickets) {
                String nunticket = oData.getNumeroTicket();
                String[] ticketParts = nunticket.split("/");
                String numeroTicketSplit = ticketParts[0];
                if (oData.getRutaArchivoOnboarding() != null && !oData.getRutaArchivoOnboarding().equals("")) {
                    downloadFiledBoarding(numeroTicketSplit, oData);
                }
            }
            ((CheckTicketActivity) getActivity()).sendData(invalidos);
            BelmondUtil.hideProgreesDialogBelmond();
        }else{
            BelmondUtil.hideProgreesDialogBelmond();
        }
    }

    private void downloadFiledBoarding(String numeroTicketSplit, Ticket oTicket) {
        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoOnboarding());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Boarding-grupo" + grupoDTO.getId() + "-" + numeroTicketSplit);
                Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), oTicket.getNumeroTicket());
                oTicketDB.setRutaArchivoOnboarding(writtenToDisk);
                myTicketsViewModel.update(oTicketDB);
                if (null != oTicket.getRutaArchivoDocumentoElectronico() && !oTicket.getRutaArchivoDocumentoElectronico().equals("")) {
                    downloadFileDocumentoElectronico(numeroTicketSplit, oTicket);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
            }
        });
    }

    private void downloadFileDocumentoElectronico(String numeroTicketSplit, Ticket oTicket) {
        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoDocumentoElectronico());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Documento-grupo" + grupoDTO.getId() + "-" + numeroTicketSplit);
                    Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), oTicket.getNumeroTicket());
                    oTicketDB.setRutaArchivoDocumentoElectronico(writtenToDisk);
                    myTicketsViewModel.update(oTicketDB);
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
                Toast.makeText(getActivity(), "OCURRIO UN ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }

}

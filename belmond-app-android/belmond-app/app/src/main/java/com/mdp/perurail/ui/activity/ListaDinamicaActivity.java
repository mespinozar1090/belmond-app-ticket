package com.mdp.perurail.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.DinamicDTO;
import com.mdp.perurail.ui.adapter.ListaDinamicaAdapter;
import com.mdp.perurail.utilitary.methods.BelmondUtil;

import java.util.ArrayList;
import java.util.List;

public class ListaDinamicaActivity extends AppCompatActivity {

    private ListaDinamicaAdapter adaptador;
    private List<DinamicDTO> lista;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_dinamica);
        setToolbar();
        initView();

       int tipoAccion = getIntent().getIntExtra("tipoAccion",0);
       loadLista(tipoAccion);
    }

    private void initView(){
        recyclerview = findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getApplicationContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerview.getContext(),
                linearLayout.getOrientation());
        recyclerview.addItemDecoration(dividerItemDecoration);
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new ListaDinamicaAdapter(getApplicationContext(), lista);
        recyclerview.setAdapter(adaptador);
    }

    private void loadLista(Integer tipoAccion){
        if(tipoAccion == 1){
            adaptador.setLista(obtenerPais());
        }else if(tipoAccion == 2){
            adaptador.setLista(obtenerDocumento());
        }else{
            Toast.makeText(this, "Ocurrio un error", Toast.LENGTH_SHORT).show();
        }
        onclickTable();
    }

    private  List<DinamicDTO> obtenerPais(){
        List<DinamicDTO> dinamicDTOS = new ArrayList<>();
        String[] listapais = getResources().getStringArray(R.array.lista_paises);
        for (int x=0;x<listapais.length;x++){
           dinamicDTOS.add(new DinamicDTO(BelmondUtil.codPais(x+1),listapais[x]));
        }
        return dinamicDTOS;
    }

    private  List<DinamicDTO> obtenerDocumento(){
        List<DinamicDTO> dinamicDTOS = new ArrayList<>();
        String[] listadocumento = getResources().getStringArray(R.array.lista_tipo_documento);
        for (int x=0;x<listadocumento.length;x++){
            dinamicDTOS.add(new DinamicDTO(BelmondUtil.codTipoDocumento(x+1),listadocumento[x]));
        }
        return dinamicDTOS;
    }

    private void onclickTable(){
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.rootLayout) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("object",adaptador.getDiamic(positionL));
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });
    }


    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_atras, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

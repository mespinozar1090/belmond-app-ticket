package com.mdp.perurail.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.DinamicDTO;

import java.util.ArrayList;
import java.util.List;

public class ListaDinamicaAdapter extends  RecyclerView.Adapter<ListaDinamicaAdapter.ViewHolder>{

    private Context context;
    private List<DinamicDTO> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public ListaDinamicaAdapter(Context context, List<DinamicDTO> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dinamic,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<DinamicDTO> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public DinamicDTO getDiamic(int position){
        return  lista.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView txtNombre;
        LinearLayout rootLayout;

        public ViewHolder(@NonNull View v) {
                super(v);
            txtNombre = v.findViewById(R.id.txtNombre);
            rootLayout = v.findViewById(R.id.rootLayout);
        }

        public void bind(DinamicDTO item){
            txtNombre.setText(item.getNombre());
            rootLayout.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(),view);
        }
    }

    public interface ClickListener{
        void onItemClick(int postion,View v);
    }

    public void setOnItemClickListener(ClickListener clickListener){
        ListaDinamicaAdapter.clickListener = clickListener;
    }
}

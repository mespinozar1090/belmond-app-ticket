package com.mdp.perurail.ui.fragment.group;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.mdp.perurail.R;
import com.mdp.perurail.database.model.Ticket;
import com.mdp.perurail.dto.GrupoDTO;
import com.mdp.perurail.dto.TicketDTO;
import com.mdp.perurail.network.response.TicketResponse;
import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.ui.activity.CheckTicketActivity;
import com.mdp.perurail.ui.activity.HomeActivity;
import com.mdp.perurail.ui.adapter.DetalleGrupoTicketAdapter;
import com.mdp.perurail.ui.fragment.person.TicketDetailFragment;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.utilitary.methods.Constans;
import com.mdp.perurail.viewModel.MyTicketsViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class GroupDetailsTicketFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String OBJECT_GRUPODTO = "grupoDTO";
    public static final int REQUEST_CODE = 1;

    private MyTicketsViewModel myTicketsViewModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private DetalleGrupoTicketAdapter adaptador;
    private List<TicketDTO> lista;
    private GrupoDTO grupoDTO;
    private EditText etdBuscar;
    private TextView txtTitle;
    private boolean isValidar = true;

    public static GroupDetailsTicketFragment newInstance(GrupoDTO grupoDTO) {
        GroupDetailsTicketFragment fragment = new GroupDetailsTicketFragment();
        Bundle args = new Bundle();
        args.putSerializable(OBJECT_GRUPODTO, grupoDTO);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            grupoDTO = (GrupoDTO) getArguments().getSerializable(OBJECT_GRUPODTO);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detalle_grupo_ticket_fragment, container, false);
        setToolbar(view);
        initView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).bottomBar.setVisibility(View.VISIBLE );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void initView(View view) {
        txtTitle = view.findViewById(R.id.txtTitle);
        recyclerview = view.findViewById(R.id.recyclerview);
        etdBuscar = view.findViewById(R.id.etdBuscar);
        swipeRefreshLayout= view.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorBemondPrimary, R.color.azul);
        swipeRefreshLayout.setOnRefreshListener(this);

        linearLayout = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();

        txtTitle.setText(grupoDTO.getName());
        adaptador = new DetalleGrupoTicketAdapter(getContext(), lista);
        recyclerview.setAdapter(adaptador);

        etdBuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence editable, int i, int i1, int i2) {
                String aux = editable.toString().trim();
                if (!aux.equals("")) {
                    ArrayList<TicketDTO> newLista = new ArrayList<>();
                    for (TicketDTO equiposMap : lista) {
                        String nombre = equiposMap.getNombrePasajero();
                        if (nombre.toLowerCase().contains(aux.toLowerCase())) {
                            newLista.add(equiposMap);
                        }
                    }
                    adaptador.setFilter(newLista);
                }else{
                    adaptador.setTickets(lista);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myTicketsViewModel = ViewModelProviders.of(this).get(MyTicketsViewModel.class);
        myTicketsViewModel.getTicketByidGrupo(grupoDTO.getId()).observe(this, new Observer<List<Ticket>>() {
            @Override
            public void onChanged(List<Ticket> tickets) {
                 lista.clear();
                if (tickets.size() > 0) {
                    List<TicketDTO> listTickets = BelmondUtil.castListToTicketModel(tickets);
                    if (isValidar) {
                        Log.e("VALIDAR", "ENTRO A VALIDAR???");
                        isValidar = false;
                        for (TicketDTO oTicketDTO : listTickets) {
                            obtenerEstadoTicket(oTicketDTO);
                        }
                    } else {
                        Log.e("VALIDAR", "NO DEBE VALIDAR");
                    }
                    lista.addAll(listTickets);
                    adaptador.setTickets(listTickets);
                    onclickTable();
                } else {
                    List<TicketDTO> lista = new ArrayList<>();
                    adaptador.setTickets(lista);
                }
            }
        });
    }


    public void obtenerEstadoTicket(TicketDTO oTicketDTO) {
        APIService apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL,getContext()).create(APIService.class);
        Call<TicketResponse> ticketEstado = apiService.getTicketEstado("", "", "", oTicketDTO.getNumeroTicket(), "V","belmond");
        ticketEstado.enqueue(new Callback<TicketResponse>() {
            @Override
            public void onResponse(Call<TicketResponse> call, Response<TicketResponse> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        validarEstadoTicket(response.body(), oTicketDTO);
                    }
                }catch (Exception e){

                }
            }

            @Override
            public void onFailure(Call<TicketResponse> call, Throwable t) {

            }
        });
    }

    public void validarEstadoTicket(TicketResponse ticketResponse, TicketDTO oTicketDTO) {
        if (ticketResponse.getEstadoTicket().endsWith("M")) {
            obtenetTicketActualizado(ticketResponse.getNumTicket(), ticketResponse.getEstadoTicket(), getContext(), oTicketDTO);
        } else if (ticketResponse.getEstadoTicket().endsWith("I")) {
            eliminarFichero(oTicketDTO);
            myTicketsViewModel.delete(BelmondUtil.castToTicket(oTicketDTO));
        } else if (ticketResponse.getEstadoTicket().endsWith("V")) {
            Log.e("ESTADO", " - TICKET VALIDO ORIGINAL" + ticketResponse.getNumTicket());
        }
    }

    public void obtenetTicketActualizado(String numTic, String tipAcc, Context context, TicketDTO ticketDTO) {
        APIService apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL,getContext()).create(APIService.class);
        Call<List<TicketResponse>> listCall = apiService.getTicketActualiado("", "", "", numTic, "C","belmond");
        listCall.enqueue(new Callback<List<TicketResponse>>() {
            @Override
            public void onResponse(Call<List<TicketResponse>> call, Response<List<TicketResponse>> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        for (Ticket ticketResponse : BelmondUtil.castTicketResponseToTickeDB(response.body())) {
                            ticketResponse.setIdGrupo(grupoDTO.getId());
                            myTicketsViewModel.insert(ticketResponse);
                            descargarTickets(BelmondUtil.castTicketResponseToTickeDB(response.body()));

                        }
                        eliminarFichero(ticketDTO);
                        myTicketsViewModel.delete(BelmondUtil.castToTicket(ticketDTO));
                    }
                }catch (Exception e){
                }
            }

            @Override
            public void onFailure(Call<List<TicketResponse>> call, Throwable t) {

            }
        });
    }

    private void eliminarFichero(TicketDTO ticketDTO) {
        if (null != ticketDTO.getRutaArchivoOnboarding() && !ticketDTO.getRutaArchivoOnboarding().equals("")) {
            BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoOnboarding()));
        }
        if (null != ticketDTO.getRutaArchivoDocumentoElectronico() && !ticketDTO.getRutaArchivoDocumentoElectronico().equals("")) {
            BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoDocumentoElectronico()));
        }
    }

    private void descargarTickets(List<Ticket> numerosTickets) {
        for (Ticket oData : numerosTickets) {
            String nunticket = oData.getNumeroTicket();
            String[] ticketParts = nunticket.split("/");
            String numeroTicketSplit = ticketParts[0];
            if (oData.getRutaArchivoOnboarding() != null && !oData.getRutaArchivoOnboarding().equals("")) {
                downloadFiledBoarding(numeroTicketSplit, oData);
            }
        }
    }

    private void downloadFiledBoarding(String numeroTicketSplit, Ticket oTicket) {

        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoOnboarding());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               try {
                   String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Boarding-grupo"+grupoDTO.getId()+"+"+numeroTicketSplit);
                   Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), oTicket.getNumeroTicket());
                   oTicketDB.setRutaArchivoOnboarding(writtenToDisk);
                   myTicketsViewModel.update(oTicketDB);
                   if (null != oTicket.getRutaArchivoDocumentoElectronico() && !oTicket.getRutaArchivoDocumentoElectronico().equals("")) {
                       downloadFileDocumentoElectronico(numeroTicketSplit, oTicket);
                   }
               }catch (Exception e){

               }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
            }
        });
    }

    private void downloadFileDocumentoElectronico(String numeroTicketSplit, Ticket oTicket) {
        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoDocumentoElectronico());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Documento-grupo"+grupoDTO.getId()+"-"+numeroTicketSplit);
                    Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), oTicket.getNumeroTicket());
                    oTicketDB.setRutaArchivoDocumentoElectronico(writtenToDisk);
                    myTicketsViewModel.update(oTicketDB);
                }catch (Exception e){

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
                Toast.makeText(getActivity(), "OCURRIO UN ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.tvDescription);
        title.setText(getString(R.string.agregar));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_atras, null);
            drawable = DrawableCompat.wrap(drawable);
            //  DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorNegro));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.btnDetalle) {
                BelmondUtil.setFragment(getActivity(), new TicketDetailFragment().newInstance(adaptador.getTicketAt(positionL)), null);
            } else if (vL.getId() == R.id.imgQR) {
                BelmondUtil.showModalCodeQR(getActivity(), adaptador.getTicketAt(positionL).getCodigoHash());
            } else if (vL.getId() == R.id.btnEliminar) {
                showModelDeleteTicket(adaptador.getTicketAt(positionL));
            }
        });
    }

    private void showModelDeleteTicket(TicketDTO ticketDTO) {
        View view = getLayoutInflater().inflate(R.layout.dialog_detele_ticket, null);
        AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        TextView txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        TextView txtNroTicket = (TextView) view.findViewById(R.id.txtNroTicket);
        TextView txtRuta = (TextView) view.findViewById(R.id.txtRuta);
        Button btnAceptar = (Button) view.findViewById(R.id.btnAceptar);
        Button btnCancelar = (Button) view.findViewById(R.id.btnCancelar);


        txtNombre.setText(ticketDTO.getNombrePasajero());
        txtNroTicket.setText(ticketDTO.getNumeroTicket());
        txtRuta.setText(ticketDTO.getRuta());

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (BelmondUtil.getDeviceMetrics(getContext()).heightPixels * 0.8);
        dialog.show();

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTicketsViewModel.delete(BelmondUtil.castToTicket(ticketDTO));
                eliminarFichero(ticketDTO);
                dialog.dismiss();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            case R.id.accion_crear:
                isValidar = false;
                Intent intent = new Intent(getActivity(), CheckTicketActivity.class);
                intent.putExtra("grupoDTO", grupoDTO);
                startActivityForResult(intent, REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE  && resultCode  == RESULT_OK) {
            if(null != data.getStringArrayListExtra("tickets")){
               ArrayList<String> arrayList = data.getStringArrayListExtra("tickets");
               for (String tickets : arrayList){
                   Log.e("Ticket", " invalido :"+ tickets);
               }
               if(arrayList.size()>0){
                   showModalErrorTicket(arrayList);
               }

           }else{
               Log.e("ERROR", " LISTA DE TICKET INCORRECTO ES NULL");
           }
        } else {
            Log.e("ERROR", " Ocuerio un error en onActivityResult");
        }
    }


    private void showModalErrorTicket(ArrayList arrayLista) {
        View view = getLayoutInflater().inflate(R.layout.dialog_error_ticket, null);
        AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        alertDialog.setView(view);


        ListView listaticket = (ListView) view.findViewById(R.id.listatickets);
        Button btnAceptar = (Button) view.findViewById(R.id.btnAceptar);

        ArrayAdapter adaptador = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1,arrayLista);
        listaticket.setAdapter(adaptador);

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (BelmondUtil.getDeviceMetrics(getContext()).heightPixels * 0.8);
        dialog.show();


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_crear, menu);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                List<Ticket> oLista = myTicketsViewModel.getTicketsByGrupo(grupoDTO.getId());
                for (TicketDTO ticket : BelmondUtil.castListToTicketModel(oLista)){
                    obtenerEstadoTicket(ticket);
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }
}

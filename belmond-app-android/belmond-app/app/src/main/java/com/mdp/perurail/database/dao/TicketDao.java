package com.mdp.perurail.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import com.mdp.perurail.database.model.Ticket;
import java.util.List;

@Dao
public interface TicketDao {

    @Insert
    void insert(Ticket ticket);

    @Update
    void update(Ticket ticket);

    @Delete
    void delete(Ticket ticket);

    @Query("DELETE FROM ticket_table")
    void deleteAllTicket();

    @Query("SELECT * FROM ticket_table")
    LiveData<List<Ticket>> getAllTicket();

    @Query("SELECT * FROM ticket_table where numeroTicket =:numeroTicket")
    Ticket getTicket(String numeroTicket);

    @Query("SELECT * FROM ticket_table where idGrupo =:idGrupo")
    LiveData<List<Ticket>> getTicketByidGrupo(int idGrupo);

    @Query("SELECT * FROM ticket_table WHERE idGrupo =:idGrupo AND numeroTicket =:numeroTicket")
    Ticket getTicketByIdGrupoAndNumeroticket(int idGrupo,String numeroTicket);

    @Query("DELETE FROM ticket_table WHERE idGrupo =:idGrupo")
    void deleteAllTicketByGrupo(int idGrupo);

    @Query("SELECT * FROM ticket_table where idGrupo =:idGrupo")
    List<Ticket> getTicketsByidGrupo(int idGrupo);
}

package com.mdp.perurail.ui.fragment.person;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.TicketDTO;
import com.mdp.perurail.ui.activity.HomeActivity;
import com.mdp.perurail.ui.activity.VisorPDFActivity;
import com.mdp.perurail.ui.adapter.SlideHomeAdapter;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.viewModel.TicketDetailViewModel;

public class TicketDetailFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_TICKEDTO = "ticketObject";

    private TicketDetailViewModel mViewModel;
    private LinearLayout linearPuntos;//linearLayout de Puntos
    private TextView[] puntosSlide;
    private Button btnComprobante, btnTicket;
    private View view;
    private int[] layouts;
    private TicketDTO oTicketDTO;

    private TextView txtPasajero;
    private TextView txtRuta;
    private TextView txtTicket;
    private TextView txtTipo;
    private TextView txtFecha;
    private TextView txtHora;
    private TextView txtVagon;
    private TextView txtAsiento;
    private ImageView btncodigoQR;
    Bitmap bitmap;
    HomeActivity homeActivity;


    public static TicketDetailFragment newInstance(TicketDTO ticketDTO) {
        TicketDetailFragment fragment = new TicketDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TICKEDTO, ticketDTO);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            oTicketDTO = (TicketDTO) getArguments().getSerializable(ARG_TICKEDTO);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.ticket_detail_fragment, container, false);
        initialize(view);
        setToobar(view);
        ((HomeActivity)getActivity()).bottomBar.setVisibility(View.GONE);
        return view;
    }


    private void initialize(View view) {
        txtPasajero = view.findViewById(R.id.txtPasajero);
        txtRuta = view.findViewById(R.id.txtRuta);
        txtTicket = view.findViewById(R.id.txtTicket);
        txtTipo = view.findViewById(R.id.txtTipo);
        txtFecha = view.findViewById(R.id.txtFecha);
        txtHora = view.findViewById(R.id.txtHora);
        txtVagon = view.findViewById(R.id.txtVagon);
        txtAsiento = view.findViewById(R.id.txtAsiento);
        btnComprobante = view.findViewById(R.id.btnComprobante);
        btnTicket = view.findViewById(R.id.btnTicket);
        btncodigoQR = (ImageView) view.findViewById(R.id.btncodigoQR);
        btnComprobante.setOnClickListener(this);
        btnTicket.setOnClickListener(this);


        layouts = new int[]{R.layout.fragment_destino_one, R.layout.fragment_destino_two};
        SlideHomeAdapter slidePagerAdapter = new SlideHomeAdapter(getContext(), layouts);
       // ViewPager viewPager = view.findViewById(R.id.view_pager);
        //viewPager.setAdapter(slidePagerAdapter);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

     //   linearPuntos = (LinearLayout) view.findViewById(R.id.idLinearPuntos);
        //viewPager.addOnPageChangeListener(viewListener);
        //agregaIndicadorPuntos(0);

        cargarDatos();
    }

    private void cargarDatos() {
        txtPasajero.setText(oTicketDTO.getNombrePasajero());
        txtRuta.setText(oTicketDTO.getRuta());//getRuta
        txtTipo.setText(oTicketDTO.getTipoTren());
        txtFecha.setText(oTicketDTO.getFechaSalida());
        txtHora.setText(oTicketDTO.getHoraSalida());
        txtVagon.setText(oTicketDTO.getVagon());
        txtAsiento.setText(oTicketDTO.getNumeroAsiento());
        txtTicket.setText(oTicketDTO.getNumeroTicket());

        if(oTicketDTO!=null){
            if(oTicketDTO.getCodigoHash()!=null){
                if(!oTicketDTO.getCodigoHash().isEmpty()){

                    btncodigoQR.setImageBitmap(BelmondUtil.createQR(oTicketDTO.getCodigoHash()));

                    btncodigoQR.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BelmondUtil.showModalCodeQR(getActivity(), oTicketDTO.getCodigoHash());
                        }
                    });
                }else{
                    btncodigoQR.setVisibility(View.INVISIBLE);
                }
            }

        }



    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
    //        agregaIndicadorPuntos(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TicketDetailViewModel.class);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToobar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        //toolbar.setTitle(getString(R.string.detalle));
        TextView title = (TextView) toolbar.findViewById(R.id.tvDescription);
        title.setText(getString(R.string.detalle));

        //toolbar.setTitleTextColor(Color.WHITE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);

            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_atras, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnComprobante:
                showVisorPDF(getString(R.string.comprobante_de_pago), oTicketDTO.getRutaArchivoDocumentoElectronico());
                Log.e("",""+oTicketDTO.getRutaArchivoDocumentoElectronico());

                break;
            case R.id.btnTicket:
                showVisorPDF(getString(R.string.boleto), oTicketDTO.getRutaArchivoOnboarding());
                break;
        }
    }

    private void showVisorPDF(String name, String url) {
        if(null != url && !url.equals("")){
            Intent intent = new Intent(getActivity(), VisorPDFActivity.class);
            intent.putExtra("name", name);
            intent.putExtra("url", url);
            startActivity(intent);
        }else{
            Log.e("No Existe"," "+name);
            showModalErrorFactura();
        }

    }


    private void showModalErrorFactura() {

        View view = getLayoutInflater().inflate(R.layout.dialog_error_url_visor, null);
        AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        Button btnclose = (Button) view.findViewById(R.id.btnAceptar);

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (BelmondUtil.getDeviceMetrics(getContext()).heightPixels * 0.8);
        dialog.show();

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
}

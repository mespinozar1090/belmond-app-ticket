package com.mdp.perurail.ui.fragment.destino;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.DestinoDTO;
import com.mdp.perurail.ui.adapter.DestinoRutasAdapter;
import com.mdp.perurail.utilitary.methods.BelmondUtil;

import java.util.ArrayList;
import java.util.List;


public class DestinoRutasFragment extends Fragment {

    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private List<DestinoDTO> lista;
    private DestinoRutasAdapter adaptador;
    private WebView idWebview;

    List<DestinoDTO> destino_machuPicchu = null;
    List<DestinoDTO> destino_ollantaytambo = null;
    List<DestinoDTO> destino_cusco = null;
    List<DestinoDTO> destino_puno = null;
    List<DestinoDTO> destino_urubamba = null;
    List<DestinoDTO> destino_arequipa = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);

    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.destino_ruta_fragment, container, false);
        setToolbar(view);
       // getRutas();
        initView(view);

        return view;
    }

    private void  initView(View view ){
        idWebview = view.findViewById(R.id.idWebview);
        recyclerview = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new DestinoRutasAdapter(lista);
        recyclerview.setAdapter(adaptador);

        idWebview.getSettings().setLoadsImagesAutomatically(true);
        idWebview.getSettings().setJavaScriptEnabled(true);
        idWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        Spinner spinnerDestino = view.findViewById(R.id.spinnerView6);
        ArrayAdapter<DestinoDTO> adapter = new ArrayAdapter<DestinoDTO>(getActivity(),R.layout.spinner_text, BelmondUtil.listDestinos());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDestino.setAdapter(adapter);
        spinnerDestino.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DestinoDTO destinoDTO = (DestinoDTO) parent.getSelectedItem();
                cargarRutasPorDestino(destinoDTO.getId());

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void cargarRutasPorDestino(Integer idDestino){
      //  lista.clear();
        switch (idDestino){
            case 1:
               // lista.addAll(destino_machuPicchu);
                idWebview.loadUrl("https://www.perurail.com/es/timeline-machu-picchu-perurail/");
                break;
            case 2:
               idWebview.loadUrl("https://www.perurail.com/es/destinos/cusco-machu-picchu/");
                break;
            case 3:
                idWebview.loadUrl("https://www.perurail.com/es/destinos/puno-lago-titicaca/");
                break;
            case 4:
                idWebview.loadUrl("https://www.perurail.com/es/destinos/puno-lago-titicaca/");
                break;
            case 5:
                idWebview.loadUrl("https://www.perurail.com/es/ubicanos/");
                break;
            case 6:
                idWebview.loadUrl("https://elcomercio.pe/");
                break;
        }
      //  adaptador.notifyDataSetChanged();
    }

  /*  private  void getRutas(){
        destino_machuPicchu = new ArrayList<>();
        destino_machuPicchu.add(new DestinoDTO(12,"MACHU PICCHU","Cusco - Machu Picchu"));
        destino_machuPicchu.add(new DestinoDTO(12,"MACHU PICCHU","Ollantaytambo - Machu Picchu"));
        destino_machuPicchu.add(new DestinoDTO(12,"MACHU PICCHU","Urubamba - Machu Picchu"));

        destino_ollantaytambo = new ArrayList<>();
        destino_ollantaytambo.add(new DestinoDTO(46,"OLLANTAYTAMBO","Machu Picchu - Ollantaytambo"));

        destino_cusco = new ArrayList<>();
        destino_cusco.add(new DestinoDTO(31,"CUSCO","Puno - Cusco"));
        destino_cusco.add(new DestinoDTO(11,"CUSCO","Machu Picchu - Cusco"));
        destino_cusco.add(new DestinoDTO(11,"CUSCO","Machu Picchu - Cusco"));

        destino_puno = new ArrayList<>();
        destino_puno.add(new DestinoDTO(31,"PUNO","Cusco - Puno"));

        destino_urubamba = new ArrayList<>();
        destino_urubamba.add(new DestinoDTO(31,"URUBAMBA","Machu Picchu - Urubamba"));

        destino_arequipa = new ArrayList<>();
        destino_arequipa.add(new DestinoDTO(31,"AREQUIPA","Cusco - Puno - Arequipa"));
    }*/

    private void setToolbar(View view ) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbarhome);
        TextView title = (TextView)toolbar.findViewById(R.id.tvDescription);
        title.setText("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_atras, null);
            drawable = DrawableCompat.wrap(drawable);
            //  DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorNegro));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }





}

package com.mdp.perurail.network.response;

public class TicketResponse {

    private String nomPasaj;
    private String serTicket;
    private String numTicket;
    private String ruta;
    private String tipTren;
    private String vagon;
    private String asiento;
    private String fecSalida;
    private String horSalida;
    private String esModifi;
    private String urlTicket;
    private String urlDocu;
    private String codHash;
    private String estadoTicket;


    public String getEstadoTicket() {
        return estadoTicket;
    }

    public void setEstadoTicket(String estadoTicket) {
        this.estadoTicket = estadoTicket;
    }

    public String getNomPasaj() {
        return nomPasaj;
    }

    public void setNomPasaj(String nomPasaj) {
        this.nomPasaj = nomPasaj;
    }

    public String getSerTicket() {
        return serTicket;
    }

    public void setSerTicket(String serTicket) {
        this.serTicket = serTicket;
    }

    public String getNumTicket() {
        return numTicket;
    }

    public void setNumTicket(String numTicket) {
        this.numTicket = numTicket;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getTipTren() {
        return tipTren;
    }

    public void setTipTren(String tipTren) {
        this.tipTren = tipTren;
    }

    public String getVagon() {
        return vagon;
    }

    public void setVagon(String vagon) {
        this.vagon = vagon;
    }

    public String getAsiento() {
        return asiento;
    }

    public void setAsiento(String asiento) {
        this.asiento = asiento;
    }

    public String getFecSalida() {
        return fecSalida;
    }

    public void setFecSalida(String fecSalida) {
        this.fecSalida = fecSalida;
    }

    public String getHorSalida() {
        return horSalida;
    }

    public void setHorSalida(String horSalida) {
        this.horSalida = horSalida;
    }

    public String getEsModifi() {
        return esModifi;
    }

    public void setEsModifi(String esModifi) {
        this.esModifi = esModifi;
    }

    public String getUrlTicket() {
        return urlTicket;
    }

    public void setUrlTicket(String urlTicket) {
        this.urlTicket = urlTicket;
    }

    public String getUrlDocu() {
        return urlDocu;
    }

    public void setUrlDocu(String urlDocu) {
        this.urlDocu = urlDocu;
    }

    public String getCodHash() {
        return codHash;
    }

    public void setCodHash(String codHash) {
        this.codHash = codHash;
    }
}

package com.mdp.perurail.database.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "ticket_table")
public class Ticket {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int idGrupo;
    private String numeroTicket;
    private String nombrePasajero;
    private String ruta;
    private String tipoTren;
    private String vagon;
    private String numeroAsiento;
    private String fechaSalida;
    private String horaSalida;
    private String estadoModificacion;
    private String rutaArchivoOnboarding;
    private String rutaArchivoDocumentoElectronico;
    private String codigoHash;
    private String esModifi;

    @Ignore
    public Ticket() {
    }

    public Ticket(Integer idGrupo, String numeroTicket, String nombrePasajero, String ruta, String tipoTren, String vagon, String numeroAsiento, String fechaSalida, String horaSalida,
                  String estadoModificacion, String rutaArchivoOnboarding, String rutaArchivoDocumentoElectronico, String codigoHash) {
        this.idGrupo = idGrupo;
        this.numeroTicket = numeroTicket;
        this.nombrePasajero = nombrePasajero;
        this.ruta = ruta;
        this.tipoTren = tipoTren;
        this.vagon = vagon;
        this.numeroAsiento = numeroAsiento;
        this.fechaSalida = fechaSalida;
        this.horaSalida = horaSalida;
        this.estadoModificacion = estadoModificacion;
        this.rutaArchivoOnboarding = rutaArchivoOnboarding;
        this.rutaArchivoDocumentoElectronico = rutaArchivoDocumentoElectronico;
        this.codigoHash = codigoHash;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigoHash() {
        return codigoHash;
    }

    public void setCodigoHash(String codigoHash) {
        this.codigoHash = codigoHash;
    }

    public String getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(String numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    public String getNombrePasajero() {
        return nombrePasajero;
    }

    public void setNombrePasajero(String nombrePasajero) {
        this.nombrePasajero = nombrePasajero;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getTipoTren() {
        return tipoTren;
    }

    public void setTipoTren(String tipoTren) {
        this.tipoTren = tipoTren;
    }

    public String getVagon() {
        return vagon;
    }

    public void setVagon(String vagon) {
        this.vagon = vagon;
    }

    public String getNumeroAsiento() {
        return numeroAsiento;
    }

    public void setNumeroAsiento(String numeroAsiento) {
        this.numeroAsiento = numeroAsiento;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getEstadoModificacion() {
        return estadoModificacion;
    }

    public void setEstadoModificacion(String estadoModificacion) {
        this.estadoModificacion = estadoModificacion;
    }

    public String getRutaArchivoOnboarding() {
        return rutaArchivoOnboarding;
    }

    public void setRutaArchivoOnboarding(String rutaArchivoOnboarding) {
        this.rutaArchivoOnboarding = rutaArchivoOnboarding;
    }

    public String getRutaArchivoDocumentoElectronico() {
        return rutaArchivoDocumentoElectronico;
    }

    public void setRutaArchivoDocumentoElectronico(String rutaArchivoDocumentoElectronico) {
        this.rutaArchivoDocumentoElectronico = rutaArchivoDocumentoElectronico;
    }

    public String getEsModifi() {
        return esModifi;
    }

    public void setEsModifi(String esModifi) {
        this.esModifi = esModifi;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", idGrupo=" + idGrupo +
                ", numeroTicket='" + numeroTicket + '\'' +
                ", nombrePasajero='" + nombrePasajero + '\'' +
                ", ruta='" + ruta + '\'' +
                ", tipoTren='" + tipoTren + '\'' +
                ", vagon='" + vagon + '\'' +
                ", numeroAsiento='" + numeroAsiento + '\'' +
                ", fechaSalida='" + fechaSalida + '\'' +
                ", horaSalida='" + horaSalida + '\'' +
                ", estadoModificacion='" + estadoModificacion + '\'' +
                ", rutaArchivoOnboarding='" + rutaArchivoOnboarding + '\'' +
                ", rutaArchivoDocumentoElectronico='" + rutaArchivoDocumentoElectronico + '\'' +
                ", codigoHash='" + codigoHash + '\'' +
                '}';
    }
}

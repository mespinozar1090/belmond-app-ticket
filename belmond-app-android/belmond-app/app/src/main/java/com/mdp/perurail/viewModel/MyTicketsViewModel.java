package com.mdp.perurail.viewModel;

import android.app.Application;
import android.text.method.TimeKeyListener;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.mdp.perurail.database.model.Ticket;
import com.mdp.perurail.database.repository.TicketRepository;
import java.util.List;

public class MyTicketsViewModel extends AndroidViewModel {
    private TicketRepository repository;
    private LiveData<List<Ticket>> allTicket;

    public MyTicketsViewModel(@NonNull Application application) {
        super(application);
        repository = new TicketRepository(application);
        allTicket = repository.getAllTickets();
    }

    public void insert(Ticket ticket){
        repository.insert(ticket);
    }

    public void update(Ticket ticket){
        repository.update(ticket);
    }

    public void delete(Ticket ticket){
        repository.delete(ticket);
    }

    public void deleteAllTickets(){
        repository.getAllTickets();
    }

    public LiveData<List<Ticket>> getAllTickets(){
        return allTicket;
    }

    public Ticket getTicket(String numeroTicket){
        return repository.getTicket(numeroTicket);
    }


    public LiveData<List<Ticket>> getTicketByidGrupo(int idGrupo){
        return repository.getTicketByidGrupo(idGrupo);
    }

    public Ticket getTicketByIdGrupoAndNumeroticket(Integer idGrupo,String numeroTicket){
        return repository.getTicketByIdGrupoAndNumeroticket(idGrupo,numeroTicket);
    }

    public void deleteAllTicketByGrupo(Integer idGrupo){
        repository.deleteAllTicketByGrupo(idGrupo);
    }

    public  List<Ticket> getTicketsByGrupo(Integer idGrupo){
       return repository.getTicketsByGrupo(idGrupo);
    }



}

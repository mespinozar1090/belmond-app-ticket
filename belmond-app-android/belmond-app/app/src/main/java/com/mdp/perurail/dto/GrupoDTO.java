package com.mdp.perurail.dto;

import java.io.Serializable;

public class GrupoDTO implements Serializable {

    private Integer id;
    private String name;

    public GrupoDTO() {
    }

    public GrupoDTO(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

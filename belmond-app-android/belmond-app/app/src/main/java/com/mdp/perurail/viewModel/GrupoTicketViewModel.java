package com.mdp.perurail.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mdp.perurail.database.model.Grupo;
import com.mdp.perurail.database.repository.GrupoRepository;

import java.util.List;

public class GrupoTicketViewModel extends AndroidViewModel {
    private GrupoRepository repository;
    private LiveData<List<Grupo>> allGrupos;


    public GrupoTicketViewModel(@NonNull Application application) {
        super(application);
        repository = new GrupoRepository(application);
        allGrupos = repository.getAllGrupos();
    }

    public void insert(Grupo grupo){
        repository.insert(grupo);
    }

    public void update(Grupo grupo){
        repository.update(grupo);
    }

    public void delete(Grupo grupo){
        repository.delete(grupo);
    }

    public void deleteAllGrupos(){
        repository.getAllGrupos();
    }

    public LiveData<List<Grupo>> getAllGrupos(){
        return allGrupos;
    }

}

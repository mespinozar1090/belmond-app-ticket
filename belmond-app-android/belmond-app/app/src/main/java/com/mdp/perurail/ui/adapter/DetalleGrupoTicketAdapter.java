package com.mdp.perurail.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.TicketDTO;
import com.mdp.perurail.utilitary.methods.BelmondUtil;

import java.util.ArrayList;
import java.util.List;

public class DetalleGrupoTicketAdapter extends  RecyclerView.Adapter<DetalleGrupoTicketAdapter.ViewHolder> {
    private Context context;
    private List<TicketDTO> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public DetalleGrupoTicketAdapter(Context context, List<TicketDTO> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_grupo_detalle_ticket, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(context, lista.get(position));
    }

    @Override
    public int getItemCount() {
      return lista.size();
    }

    public void setTickets(List<TicketDTO> tickts){
       // this.lista = tickts;
        notifyDataSetChanged();
    }

    public TicketDTO getTicketAt(int position){
        return lista.get(position);
    }

    public List<TicketDTO> getLista(){
        return lista;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView txtNombre, txtRuta, txtTicket, txtHora, txtSalida, txtvigente;
        private ImageButton btnDetalle,btnEliminar;
        private ImageView imgQR;
        private LinearLayout estadoTicket;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtNombre = (TextView) view.findViewById(R.id.txtNombre);
            txtRuta = (TextView) view.findViewById(R.id.txtRuta);
            txtTicket = (TextView) view.findViewById(R.id.txtTicket);
            txtSalida = (TextView) view.findViewById(R.id.txtSalida);
            txtHora = (TextView) view.findViewById(R.id.txtHora);
            btnDetalle = (ImageButton) view.findViewById(R.id.btnDetalle);
            imgQR = (ImageView) view.findViewById(R.id.imgQR);
            btnEliminar = (ImageButton) view.findViewById(R.id.btnEliminar);
            estadoTicket = (LinearLayout) view.findViewById(R.id.estadoTicket);
            txtvigente = (TextView)view.findViewById(R.id.txtvigente);
        }

        public void bind(Context context, TicketDTO item) {
            txtNombre.setText(item.getNombrePasajero());
            txtRuta.setText(item.getRuta());
            txtTicket.setText(item.getNumeroTicket());
            txtSalida.setText(item.getFechaSalida());
            txtHora.setText(item.getHoraSalida());

            if(item != null){
                if(item.getCodigoHash()!=null){
                    if(!item.getCodigoHash().isEmpty()) {
                        imgQR.setVisibility(View.VISIBLE);
                        imgQR.setImageBitmap(BelmondUtil.createQRForAdpter(item.getCodigoHash()));
                        imgQR.setOnClickListener(this);
                    }else{
                        imgQR.setVisibility(View.INVISIBLE);
                    }
                }
            }

            btnDetalle.setOnClickListener(this);

            btnEliminar.setOnClickListener(this);

            if(item.getEsModifi() != null && item.getEsModifi().equals("S")){
                estadoTicket.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBemondPrimary));
                txtvigente.setText(R.string.vigente);
            }

            Log.e("DATA1"," : "+item.toString());
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }
    public interface ClickListener {
        void onItemClick(int position, View v);
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        DetalleGrupoTicketAdapter.clickListener = clickListener;
    }
    public void  setFilter(ArrayList<TicketDTO> newLista){
        lista = new ArrayList<>();
        lista.addAll(newLista);
        notifyDataSetChanged();
    }
}

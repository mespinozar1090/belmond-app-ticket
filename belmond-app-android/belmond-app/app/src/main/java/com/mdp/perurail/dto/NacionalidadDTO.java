package com.mdp.perurail.dto;

import java.io.Serializable;

public class NacionalidadDTO  implements Serializable {
    private String id;
    private String pais;

    public NacionalidadDTO(String id, String pais) {
        this.id = id;
        this.pais = pais;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        return pais;
    }
}

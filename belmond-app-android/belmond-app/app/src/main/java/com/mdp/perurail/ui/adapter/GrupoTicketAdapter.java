
package com.mdp.perurail.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.perurail.R;
import com.mdp.perurail.dto.GrupoDTO;
import com.mdp.perurail.dto.TicketDTO;

import java.util.ArrayList;
import java.util.List;

public class GrupoTicketAdapter extends  RecyclerView.Adapter<GrupoTicketAdapter.ViewHolder> {

    private Context context;
    private List<GrupoDTO> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public GrupoTicketAdapter(Context context, List<GrupoDTO> lista) {
        this.context = context;
        this.lista = lista;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_grupo_ticket, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(context, lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setGrupos(List<GrupoDTO> grupos){
        this.lista = grupos;
        notifyDataSetChanged();
    }

    public GrupoDTO getGrupoAt(int position){
        return lista.get(position);
    }

    public void setFilter(ArrayList<GrupoDTO> newLista) {
        lista = new ArrayList<>();
        lista.addAll(newLista);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView txtNombre;
        ImageButton btnDetalle, btnEliminar;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtNombre = (TextView) view.findViewById(R.id.txtNombre);
            btnDetalle = (ImageButton) view.findViewById(R.id.btnDetalle);
            btnEliminar = (ImageButton) view.findViewById(R.id.btnEliminar);

        }

        public void bind(Context context, GrupoDTO item) {
            txtNombre.setText(item.getName());
            btnDetalle.setOnClickListener(this);
            btnEliminar.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        GrupoTicketAdapter.clickListener = clickListener;
    }
}

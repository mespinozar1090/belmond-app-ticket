package com.mdp.perurail.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mdp.perurail.network.repository.AutenticacionRepository;
import com.mdp.perurail.network.response.AutentticacionResponse;

public class AutenticacionViewModel  extends ViewModel {

    private AutenticacionRepository autenticacionRepository;

    public MutableLiveData<AutentticacionResponse> autenticacion(String Uid){
        autenticacionRepository =  new AutenticacionRepository();
        return  autenticacionRepository.autenticacion(Uid);
    }

}

package com.mdp.perurail.ui.fragment.person;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.perurail.R;
import com.mdp.perurail.database.model.Ticket;
import com.mdp.perurail.dto.DinamicDTO;
import com.mdp.perurail.dto.NacionalidadDTO;
import com.mdp.perurail.dto.TicketDTO;
import com.mdp.perurail.dto.TipoDocumentoDTO;
import com.mdp.perurail.network.controller.TicketController;
import com.mdp.perurail.network.response.TicketResponse;
import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.ui.activity.HomeActivity;
import com.mdp.perurail.ui.activity.ListaDinamicaActivity;
import com.mdp.perurail.ui.activity.TipoDocumentoActivity;
import com.mdp.perurail.ui.adapter.MyTicketAdapter;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.utilitary.methods.Constans;
import com.mdp.perurail.viewModel.MyTicketsViewModel;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.mdp.perurail.ui.fragment.group.GroupDetailsTicketFragment.REQUEST_CODE;

public class MyTicketsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private MyTicketsViewModel myTicketsViewModel;
    private TicketController ticketController;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private MyTicketAdapter adaptador;
    private List<TicketDTO> lista;
    private TextInputLayout tilnumerodocumento;//declaracion tiet
    private TextInputEditText tietnumerodocumento;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String[] tipodocuemnto;
    private TextView textTipoDocumento;
    private ImageView btncodigoQR;
    private Button btnGrupos;
    private String codigoPais;
    private String codigoNacionalidad;
    private boolean isValidar = true;
    private String item;
    private int posicion;

    private int OBTENER_PAIS = 90;
    private int OBTENER_DOCUMENTO = 100;
    Button spNacionalidad;
    Button spTipoDocumento;

    public static MyTicketsFragment newInstance() {
        return new MyTicketsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_my_tickets, container, false);
        setToolbar(view);
        initView(view);
        ((HomeActivity) getActivity()).bottomBar.setVisibility(View.VISIBLE);
        return view;
    }

    public void obtenerTicketPorDocumento(String codPais, String tipDoc, String docIde, String tipAcc, AlertDialog dialog, TextInputEditText tietnumerodocumento) {
        BelmondUtil.showProgressDialogBelmond(getActivity(), getString(R.string.validando));
        APIService apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL, getContext()).create(APIService.class);
        Call<List<TicketResponse>> listCall = apiService.getListTicket(codPais, tipDoc, docIde, "", tipAcc, "belmond");
        listCall.enqueue(new Callback<List<TicketResponse>>() {
            @Override
            public void onResponse(Call<List<TicketResponse>> call, Response<List<TicketResponse>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().size() > 0) {
                        List<Ticket> numerosTickets = new ArrayList<>();
                        isValidar = false;
                        for (Ticket oticket : BelmondUtil.castTicketResponseToTickeDB(response.body())) {
                            Ticket data = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(0, oticket.getNumeroTicket());
                            if (null == data) {
                                numerosTickets.add(oticket);
                                myTicketsViewModel.insert(oticket);
                            } else {
                                Log.e("DATA", "EL numero ya se encuentra registrado no debe agregar : " + data.getNumeroTicket());
                            }
                        }
                        BelmondUtil.hideProgreesDialogBelmond();
                        dialog.dismiss(); // si todo esta bien va cerrar el dialog
                        descargarTickets(numerosTickets);
                    } else {// TODO  VALIDAR SI ENTRAR AQUI Y MOSTRAR UN MENSAJE DE NUMERO TDE TICKET INCORRECTO
                        Log.e("DATA", "Ocurrio un error " + response.body().size());
                        tilnumerodocumento.setError(getString(R.string.documeto_invaldo)); // Numero invalido
                        tietnumerodocumento.requestFocus();
                        BelmondUtil.hideProgreesDialogBelmond();
                    }
                } else {
                    Log.e("LOGGER", "" + response.code());
                    BelmondUtil.hideProgreesDialogBelmond();
                }
            }

            @Override
            public void onFailure(Call<List<TicketResponse>> call, Throwable t) {
                BelmondUtil.hideProgreesDialogBelmond();
                Toast.makeText(getActivity(), "Sin conexion con el servidor...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void descargarTickets(List<Ticket> numerosTickets) {
        if (numerosTickets.size() > 0) {
            for (Ticket oData : numerosTickets) {
                String nunticket = oData.getNumeroTicket();
                String[] ticketParts = nunticket.split("/");
                String numeroTicketSplit = ticketParts[0];
                Log.e("url",""+oData.getRutaArchivoOnboarding());
                if (oData.getRutaArchivoOnboarding() != null && !oData.getRutaArchivoOnboarding().equals("")) {
                    downloadFiledBoarding(numeroTicketSplit, oData);
                }
            }
        }
    }


    private void downloadFiledBoarding(String numeroTicketSplit, Ticket oTicket) {
        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoOnboarding());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Boarding-" + numeroTicketSplit);
                    Log.e("writtenToDisk",""+writtenToDisk);
                    Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(0, oTicket.getNumeroTicket());
                    oTicketDB.setRutaArchivoOnboarding(writtenToDisk);
                    myTicketsViewModel.update(oTicketDB);
                    if (null != oTicket.getRutaArchivoDocumentoElectronico() && !oTicket.getRutaArchivoDocumentoElectronico().equals("")) {
                        downloadFileDocumentoElectronico(numeroTicketSplit, oTicket);
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
            }
        });
    }

    private void downloadFileDocumentoElectronico(String numeroTicketSplit, Ticket oTicket) {
        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoDocumentoElectronico());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Documento-" + numeroTicketSplit);
                    Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(0, oTicket.getNumeroTicket());
                    oTicketDB.setRutaArchivoDocumentoElectronico(writtenToDisk);
                    myTicketsViewModel.update(oTicketDB);

                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
                Toast.makeText(getActivity(), "OCURRIO UN ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        recyclerview = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new MyTicketAdapter(getContext(), lista);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.amarillo, R.color.azul);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerview.setAdapter(adaptador);
        btncodigoQR = (ImageView) view.findViewById(R.id.btncodigoQR);
        onclickTable();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myTicketsViewModel = ViewModelProviders.of(this).get(MyTicketsViewModel.class);
        myTicketsViewModel.getTicketByidGrupo(0).observe(this, new Observer<List<Ticket>>() {
            @Override
            public void onChanged(List<Ticket> tickets) {
                if (tickets.size() > 0) {
                    List<TicketDTO> listTickets = BelmondUtil.castListToTicketModel(tickets);
                    if (isValidar) {
                        isValidar = false;
                        for (TicketDTO oTicketDTO : listTickets) {
                            obtenerEstadoTicket(oTicketDTO);
                        }
                    } else {
                        Log.e("VALIDAR", "NO DEBE VALIDAR");
                    }
                    adaptador.setTickets(listTickets);
                } else {
                    List<TicketDTO> lista = new ArrayList<>();
                    adaptador.setTickets(lista);
                    showModalCheckTicket();
                }
            }
        });
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.tvDescription);
        title.setText(getString(R.string.consultar));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_atras, null);
            drawable = DrawableCompat.wrap(drawable);
            //  DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorNegro));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.btnDetalle) {
                BelmondUtil.setFragment(getActivity(), new TicketDetailFragment().newInstance(adaptador.getTicketAt(positionL)), null);
            } else if (vL.getId() == R.id.imgQR) {
                BelmondUtil.showModalCodeQR(getActivity(), adaptador.getTicketAt(positionL).getCodigoHash());
            } else if (vL.getId() == R.id.btnEliminar) {
                showModelDeleteTicket(adaptador.getTicketAt(positionL));
            }
        });
    }

    private void showModelDeleteTicket(TicketDTO ticketDTO) {
        View view = getLayoutInflater().inflate(R.layout.dialog_detele_ticket, null);
        AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        TextView txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        TextView txtNroTicket = (TextView) view.findViewById(R.id.txtNroTicket);
        TextView txtRuta = (TextView) view.findViewById(R.id.txtRuta);
        Button btnAceptar = (Button) view.findViewById(R.id.btnAceptar);
        Button btnCancelar = (Button) view.findViewById(R.id.btnCancelar);

        txtNombre.setText(ticketDTO.getNombrePasajero());
        txtNroTicket.setText(ticketDTO.getNumeroTicket());
        txtRuta.setText(ticketDTO.getRuta());

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (BelmondUtil.getDeviceMetrics(getContext()).heightPixels * 0.8);
        dialog.show();

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidar = false;
                myTicketsViewModel.delete(BelmondUtil.castToTicket(ticketDTO));
                BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoDocumentoElectronico()));
                BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoOnboarding()));
                dialog.dismiss();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }



    private void showModalCheckTicket() {
        View view = getLayoutInflater().inflate(R.layout.dialog_checkticket, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        tilnumerodocumento = view.findViewById(R.id.tilnumerodocumento);
        tietnumerodocumento = view.findViewById(R.id.tietnumerodocumento); // completalo

        Button btnBuscar = view.findViewById(R.id.btnBuscar);
        ImageButton btnclose = view.findViewById(R.id.btnclose);
        spNacionalidad = view.findViewById(R.id.spNacionalidad);
        spTipoDocumento = view.findViewById(R.id.spTipoDocumento);
        spTipoDocumento.setText(R.string.seleccione_documento);
        spNacionalidad.setText(R.string.seleccione_pais);

        spNacionalidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListaDinamicaActivity.class);
                intent.putExtra("tipoAccion", 1);
                startActivityForResult(intent, OBTENER_PAIS);
            }
        });

        spTipoDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListaDinamicaActivity.class);
                intent.putExtra("tipoAccion", 2);
                startActivityForResult(intent, OBTENER_DOCUMENTO);
            }
        });


        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (BelmondUtil.getDeviceMetrics(getContext()).heightPixels * 0.8);
        dialog.show();

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isValidar = false;
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tietnumerodocumento.getText().toString().trim().isEmpty()) {
                    if(!BelmondUtil.validarNumeroDocumento(tietnumerodocumento.getText().toString())){
                        obtenerTicketPorDocumento(codigoPais, codigoNacionalidad, tietnumerodocumento.getText().toString(), "C", dialog, tietnumerodocumento);
                    }else{
                        tilnumerodocumento.setError(getString(R.string.caracter_especial));
                        tietnumerodocumento.requestFocus(); //focus
                    }
                } else {
                    tilnumerodocumento.setError(getString(R.string.documeto_invaldo)); // Numero invalido
                    tietnumerodocumento.requestFocus();
                }
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OBTENER_PAIS) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    DinamicDTO dinamicDTO = (DinamicDTO) data.getSerializableExtra("object");
                    codigoPais = dinamicDTO.getId();
                    spNacionalidad.setText(dinamicDTO.getNombre());
                }
            }
        } else if (requestCode == OBTENER_DOCUMENTO) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    DinamicDTO dinamicDTO = (DinamicDTO) data.getSerializableExtra("object");
                    codigoNacionalidad = dinamicDTO.getId();
                    spTipoDocumento.setText(dinamicDTO.getNombre());
                }
            }
        }
    }

    private void validarEstadoTicket(TicketResponse ticketResponse, TicketDTO oTicketDTO) {
        if (ticketResponse.getEstadoTicket().endsWith("M")) {
            obtenetTicketActualizado(ticketResponse.getNumTicket(), ticketResponse.getEstadoTicket(), getContext(), oTicketDTO);
        } else if (ticketResponse.getEstadoTicket().endsWith("I")) {
            myTicketsViewModel.delete(BelmondUtil.castToTicket(oTicketDTO));
            BelmondUtil.eliminaDocumento(oTicketDTO);
        } else if (ticketResponse.getEstadoTicket().endsWith("V")) {
            Log.e("ESTADO", " - TICKET VALIDO ORIGINAL" + ticketResponse.getNumTicket());
        }
    }


    // TODO - INCIO ESTOS METOS SERAN LLAMADADOS MAS ADELANTE DESDE OTRA CLASE
    private void obtenerEstadoTicket(TicketDTO oTicketDTO) {
        APIService apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL, getContext()).create(APIService.class);
        Call<TicketResponse> ticketEstado = apiService.getTicketEstado("", "", "", oTicketDTO.getNumeroTicket(), "V", "belmond");
        ticketEstado.enqueue(new Callback<TicketResponse>() {
            @Override
            public void onResponse(Call<TicketResponse> call, Response<TicketResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    validarEstadoTicket(response.body(), oTicketDTO);
                }
            }

            @Override
            public void onFailure(Call<TicketResponse> call, Throwable t) {

            }
        });
    }

    private void obtenetTicketActualizado(String numTic, String tipAcc, Context context, TicketDTO ticketDTO) {
        APIService apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL, getContext()).create(APIService.class);
        Call<List<TicketResponse>> listCall = apiService.getTicketActualiado("", "", "", numTic, "C", "belmond");
        listCall.enqueue(new Callback<List<TicketResponse>>() {
            @Override
            public void onResponse(Call<List<TicketResponse>> call, Response<List<TicketResponse>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    for (Ticket ticketResponse : BelmondUtil.castTicketResponseToTickeDB(response.body())) {
                        myTicketsViewModel.insert(ticketResponse);
                        descargarTickets(BelmondUtil.castTicketResponseToTickeDB(response.body()));
                    }
                    myTicketsViewModel.delete(BelmondUtil.castToTicket(ticketDTO));
                    BelmondUtil.eliminaDocumento(ticketDTO);
                }
            }

            @Override
            public void onFailure(Call<List<TicketResponse>> call, Throwable t) {

            }
        });
    }

    // TODO - FIN ESTOS METOS SERAN LLAMADADOS MAS ADELANTE DESDE OTRA CLASE
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_consulta, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            case R.id.action_search:
                showModalCheckTicket();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                List<Ticket> oLista = myTicketsViewModel.getTicketsByGrupo(0);
                for (TicketDTO ticket : BelmondUtil.castListToTicketModel(oLista)) {
                    obtenerEstadoTicket(ticket);
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }

}

package com.mdp.perurail.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.mdp.perurail.database.conexion.BelmondDatabase;
import com.mdp.perurail.database.dao.GrupoDao;
import com.mdp.perurail.database.model.Grupo;

import java.util.List;

public class GrupoRepository {

    private GrupoDao grupoDao;
    private LiveData<List<Grupo>> allGrupos;

    public GrupoRepository(Application application) {
        BelmondDatabase database = BelmondDatabase.getInstance(application);
        grupoDao = database.grupoDao();
        allGrupos = grupoDao.getAllGrupos();
    }

    public void insert(Grupo grupo){
        new InsertGrupoAsyncTask(grupoDao).execute(grupo);
    }

    public void update(Grupo grupo) {
        new UpdateGrupoAsyncTask(grupoDao).execute(grupo);
    }

    public void delete(Grupo grupo) {
        new DeleteTicketAsyncTask(grupoDao).execute(grupo);
    }

    public void deleteAllTickes() {
        new DeleteAllGrupoAsyncTask(grupoDao).execute();
    }

    public LiveData<List<Grupo>> getAllGrupos() {
        return allGrupos;
    }

    private static class InsertGrupoAsyncTask extends AsyncTask<Grupo, Void, Void> {
        private GrupoDao grupoDao;

        private InsertGrupoAsyncTask(GrupoDao grupoDao) {
            this.grupoDao = grupoDao;
        }

        @Override
        protected Void doInBackground(Grupo... grupos) {
            grupoDao.insert(grupos[0]);
            return null;
        }
    }


    private static class UpdateGrupoAsyncTask extends AsyncTask<Grupo, Void, Void> {
        private GrupoDao grupoDao;

        private UpdateGrupoAsyncTask(GrupoDao grupoDao) {
            this.grupoDao = grupoDao;
        }

        @Override
        protected Void doInBackground(Grupo... grupos) {
            grupoDao.update(grupos[0]);
            return null;
        }
    }

    private static class DeleteTicketAsyncTask extends AsyncTask<Grupo, Void, Void> {
        private GrupoDao grupoDao;

        private DeleteTicketAsyncTask(GrupoDao grupoDao) {
            this.grupoDao = grupoDao;
        }

        @Override
        protected Void doInBackground(Grupo... grupos) {
            grupoDao.delete(grupos[0]);
            return null;
        }
    }


    private static class DeleteAllGrupoAsyncTask extends AsyncTask<Void, Void, Void> {
        private GrupoDao grupoDao;

        private DeleteAllGrupoAsyncTask(GrupoDao grupoDao) {
            this.grupoDao = grupoDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            grupoDao.deleteAllGrupos();
            return null;
        }
    }

}

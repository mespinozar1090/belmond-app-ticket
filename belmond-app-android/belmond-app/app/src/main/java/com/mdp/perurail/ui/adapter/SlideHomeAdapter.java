package com.mdp.perurail.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

public class SlideHomeAdapter extends PagerAdapter {

    private Context mContext;
    private int[] mLayouts;

    public SlideHomeAdapter(Context context, int[] mLayouts) {
        this.mContext = context;
        this.mLayouts = mLayouts;
    }

    @Override
    public int getCount() {
        return mLayouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(mLayouts[position], null);


        container.addView(view, 0);
        //container.setTag(mData.get(position).getIdAbonado());
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //((ViewPager) container).removeView((LinearLayout) object);
    }

}

package com.mdp.perurail.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.navigation.NavigationView;
import com.mdp.perurail.R;
import com.mdp.perurail.network.response.AutentticacionResponse;
import com.mdp.perurail.ui.fragment.HomeFragment;
import com.mdp.perurail.ui.fragment.destino.DestinoRutasFragment;
import com.mdp.perurail.ui.fragment.group.GroupDetailsTicketFragment;
import com.mdp.perurail.ui.fragment.group.GroupPassengersFragment;
import com.mdp.perurail.ui.fragment.person.MyTicketsFragment;
import com.mdp.perurail.ui.fragment.person.TicketDetailFragment;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.utilitary.methods.Constans;
import com.mdp.perurail.viewModel.AutenticacionViewModel;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public BottomBar bottomBar;
    private FragmentManager fragmentManager;
    public static final int MULTIPLE_PERMISSIONS = 10;
    private AutenticacionViewModel autenticacionViewModel;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        initialize();
        checkPermissions();
        sharedPreferences = getSharedPreferences(Constans.ID_APP, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        generarToken();
    }


    private void generarToken() {
        String uid = "belmond";
        autenticacionViewModel = ViewModelProviders.of(this).get(AutenticacionViewModel.class);
        autenticacionViewModel.autenticacion(uid).observe(this, new Observer<AutentticacionResponse>() {
            @Override
            public void onChanged(AutentticacionResponse autentticacionResponse) {
                if (null != autentticacionResponse) {
                    editor.putString("token", autentticacionResponse.getToken());
                    editor.apply();
                    Log.e("TOKEN :", "" + autentticacionResponse.getToken());
                }
            }
        });
    }

    private void initialize() {
        bottomBar = (BottomBar) findViewById(R.id.bottombar);
        for (int i = 0; i < bottomBar.getTabCount(); i++) {
            bottomBar.getTabAtPosition(i).setGravity(Gravity.CENTER_VERTICAL);
        }

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int tabId) {
                switch (tabId) {
                    case R.id.home:
                        setFragment(new HomeFragment());
                        break;
                    case R.id.apps:
                        setFragment(new GroupPassengersFragment());
                        break;
                    case R.id.games:
                        setFragment(new MyTicketsFragment());
                        break;
                }
            }
        });
    }

    private boolean checkPermissions() {
        int result;
        //Recorro los permisos de la aplicacion
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(HomeActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                //Lo agrego a la lista de permisos
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            //Aqui valido si no se le dio los permisos a la aplicacion , se habre un modal (activity)
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0) {
                    Log.e("SUCCESS", "TODO DEBE ESTAR BIEN");
                } else {
                    getMessagePermission();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void getMessagePermission() {
        new AlertDialog.Builder(HomeActivity.this)
                .setMessage("Algunos permisos son requeridos por Ali Segurity para un funcionamiento óptimo, por favor acepta los permisos")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkPermissions();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create()
                .show();
    }

    public void setFragment(Fragment fragment) {
        if (null != fragment) {
            fragmentManager = getSupportFragmentManager();
            boolean existFragment = false;
            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments != null) {
                for (Fragment mFragment : fragments) {
                    if (mFragment != null && mFragment.isVisible()) {
                        if (mFragment.getClass().getName().equals(fragment.getClass().getSimpleName())) {
                            existFragment = true;
                        }
                    }
                }
            }

            if (!existFragment) {
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.fragment_container, fragment)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();
            } else {
                fragmentManager.beginTransaction().remove(fragment)
                        .replace(R.id.fragment_container, fragment)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();
            }
        }
    }


    @Override
    public void onBackPressed() {
        if (bottomBar.getId() == R.id.home) {
            super.onBackPressed();
        } else {
            bottomBar.selectTabAtPosition(0);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


}

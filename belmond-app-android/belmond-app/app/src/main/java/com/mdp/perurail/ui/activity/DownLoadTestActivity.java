package com.mdp.perurail.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mdp.perurail.R;
import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.utilitary.methods.BelmondUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DownLoadTestActivity extends AppCompatActivity {

    public static final int MULTIPLE_PERMISSIONS = 10;

    String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_down_load_test);

        checkPermissions();

        Button download = (Button) findViewById(R.id.btn_download);
        Button showPDF = (Button) findViewById(R.id.showPDF);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile();
            }
        });

        showPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DownLoadTestActivity.this,VisorPDFActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean checkPermissions() {
        int result;
        //Recorro los permisos de la aplicacion
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(DownLoadTestActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                //Lo agrego a la lista de permisos
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            //Aqui valido si no se le dio los permisos a la aplicacion , se habre un modal (activity)
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0) {
                    Log.e("SUCCESS","TODO DEBE ESTAR BIEN");
                } else {
                    getMessagePermission();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void getMessagePermission() {
        //Lanzo el mensaje solicitando los permisos
        new AlertDialog.Builder(DownLoadTestActivity.this)
                .setMessage("Algunos permisos son requeridos por Ali Segurity para un funcionamiento óptimo, por favor acepta los permisos")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkPermissions();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create()
                .show();
    }




   private  void downloadFile(){
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("http://www.google.com/");
        Retrofit retrofit  = builder.build();

        APIService apiService =  retrofit.create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync("http://www.sunat.gob.pe/orientacion/formularios/decl/i-119.pdf");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(DownLoadTestActivity.this, " Yes :)", Toast.LENGTH_SHORT).show();
                boolean writtenToDisk = writeResponseBodyToDisk(response.body(),"i-118");

                Log.d("DOWNLOAD", "file download was a success? " + writtenToDisk);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR",t.getMessage()+"");
                Toast.makeText(DownLoadTestActivity.this, " error :(", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String nombre) {
        try {


            File fileCreate = new File(BelmondUtil.getTicketDirectory());
            File futureStudioIconFile = new File(fileCreate, nombre+".pdf");

            Log.e("PATH :",futureStudioIconFile.getPath());
            Log.e("ABSOLITEPAH :",futureStudioIconFile.getAbsolutePath());
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.e("FILEDOWNLOAD", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

}




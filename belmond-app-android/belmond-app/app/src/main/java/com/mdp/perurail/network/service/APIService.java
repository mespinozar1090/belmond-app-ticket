package com.mdp.perurail.network.service;

import com.mdp.perurail.network.response.AutentticacionResponse;
import com.mdp.perurail.network.response.TicketResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIService {

    @GET("vyr_apirest/Api/DetalleTickets")
    Call<List<TicketResponse>> getListTicket(@Query("codPais") String codPais,
                                             @Query("tipDoc") String tipDoc,
                                             @Query("docIde") String docIde,
                                             @Query("numTic") String numTic,
                                             @Query("tipAcc") String tipAcc,
                                             @Query("Uid") String Uid);

    @GET("vyr_apirest/Api/DetalleTickets")
    Call<List<TicketResponse>> getTicketActualiado(@Query("codPais") String codPais,
                                         @Query("tipDoc") String tipDoc,
                                         @Query("docIde") String docIde,
                                         @Query("numTic") String numTic,
                                         @Query("tipAcc") String tipAcc,
                                                   @Query("Uid") String Uid);

    @GET("vyr_apirest/Api/DetalleTickets")
    Call<TicketResponse> getTicketEstado(@Query("codPais") String codPais,
                                         @Query("tipDoc") String tipDoc,
                                         @Query("docIde") String docIde,
                                         @Query("numTic") String numTic,
                                         @Query("tipAcc") String tipAcc,
                                         @Query("Uid") String Uid);

    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);

    @GET("vyr_apirest/Api/generarToken")
    Call<AutentticacionResponse> autenticacion(@Query("Uid")String Uid);
}

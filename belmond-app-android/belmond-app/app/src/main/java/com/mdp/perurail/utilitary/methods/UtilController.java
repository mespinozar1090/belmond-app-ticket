package com.mdp.perurail.utilitary.methods;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.ui.activity.DownLoadTestActivity;
import com.mdp.perurail.viewModel.MyTicketsViewModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UtilController {

    private static String writeResponseBodyToDisk(ResponseBody body, String nombre) {
        try {

            File fileCreate = new File(BelmondUtil.getTicketDirectory());
            File futureStudioIconFile = new File(fileCreate, nombre+".pdf");

            Log.e("ABSOLITEPAH :",futureStudioIconFile.getAbsolutePath());

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.e("FILEDOWNLOAD", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return futureStudioIconFile.getAbsolutePath();
            } catch (IOException e) {
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }
}

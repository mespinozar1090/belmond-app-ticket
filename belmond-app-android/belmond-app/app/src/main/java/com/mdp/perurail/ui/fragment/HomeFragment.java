package com.mdp.perurail.ui.fragment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mdp.perurail.R;
import com.mdp.perurail.ui.activity.HomeActivity;
import com.mdp.perurail.ui.fragment.destino.DestinoRutasFragment;
import com.mdp.perurail.ui.fragment.group.GroupPassengersFragment;
import com.mdp.perurail.ui.fragment.person.MyTicketsFragment;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.viewModel.HomeViewModel;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private HomeViewModel mViewModel;
    private ImageView cardViaje;
    private ImageView btn_grupo;
    private ImageView cardDestino;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        cardViaje = (ImageView) view.findViewById(R.id.cardViaje);
        btn_grupo = view.findViewById(R.id.btn_grupo);
        cardDestino = view.findViewById(R.id.imgDestino);
        btn_grupo.setOnClickListener(this);
        cardViaje.setOnClickListener(this);
        cardDestino.setOnClickListener(this);
        ((HomeActivity)getActivity()).bottomBar.setVisibility(View.VISIBLE );
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cardViaje:
                BelmondUtil.setFragment(getActivity(), new MyTicketsFragment(),null);
                break;
            case R.id.btn_grupo:
                //BelmondUtil.setFragment(getActivity(), new GroupPassengersFragment(),null);
                break;
            case R.id.imgDestino:
                BelmondUtil.setFragment(getActivity(), new GroupPassengersFragment(),null);

                break;
        }
    }


}

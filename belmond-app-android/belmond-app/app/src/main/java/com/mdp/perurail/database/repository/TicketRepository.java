package com.mdp.perurail.database.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.mdp.perurail.database.conexion.BelmondDatabase;
import com.mdp.perurail.database.dao.TicketDao;
import com.mdp.perurail.database.model.Ticket;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class TicketRepository {

    private TicketDao ticketDao;
    private LiveData<List<Ticket>> allTickets;

    public TicketRepository(Application application){
        BelmondDatabase database =  BelmondDatabase.getInstance(application);
        ticketDao = database.ticketDao();
        allTickets = ticketDao.getAllTicket();
    }

    public void insert(Ticket ticket){
         new InsertTicketAsyncTask(ticketDao).execute(ticket);
    }

    public void update(Ticket ticket) {
        new UpdateTicketAsyncTask(ticketDao).execute(ticket);
    }

    public void delete(Ticket ticket) {
        new DeleteTicketAsyncTask(ticketDao).execute(ticket);
    }

    public void deleteAllTickes() {
        new DeleteAllTickesAsyncTask(ticketDao).execute();
    }

    public void deleteAllTicketByGrupo(Integer idGrupo) {
        new DeleteAllTickesByGrupoAsyncTask(idGrupo,ticketDao).execute();
    }


    private static class DeleteAllTickesByGrupoAsyncTask extends AsyncTask<Void, Void, Void> {
        private TicketDao ticketDao;
        private Integer idGrupo;

        private DeleteAllTickesByGrupoAsyncTask(Integer idGrupo,TicketDao ticketDao) {
            this.ticketDao = ticketDao;
            this.idGrupo = idGrupo;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ticketDao.deleteAllTicketByGrupo(idGrupo);
            return null;
        }
    }


    public LiveData<List<Ticket>> getAllTickets() {
        return allTickets;
    }


    public LiveData<List<Ticket>> getTicketByidGrupo(Integer idGrupo) {
        return ticketDao.getTicketByidGrupo(idGrupo);
    }


    public Ticket getTicketByIdGrupoAndNumeroticket(Integer idGrupo,String numeroTicket) {
        try {
            return new QueryTicketAndNumeroAsyncTask(ticketDao,idGrupo,numeroTicket).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<Ticket> getTicketsByGrupo(Integer idGrupo){
        try {
            return new  GetTicketsByGrupoAsyncTask(ticketDao,idGrupo).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class GetTicketsByGrupoAsyncTask extends AsyncTask<Ticket, String, List<Ticket>> {
        private TicketDao ticketDao;
        private Integer idGrupo;

        public GetTicketsByGrupoAsyncTask(TicketDao ticketDao, Integer idGrupo) {
            this.ticketDao = ticketDao;
            this.idGrupo = idGrupo;
        }

        @Override
        protected List<Ticket> doInBackground(Ticket... tickets) {
            return ticketDao.getTicketsByidGrupo(idGrupo);
        }
    }


    public Ticket getTicket(String numeroTicket) {
        try {
            return  new QueryTicketAsyncTask(ticketDao,numeroTicket).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static class QueryTicketAndNumeroAsyncTask extends AsyncTask<Ticket, String, Ticket> {
        private TicketDao ticketDao;
        private String numeroTicket;
        private Integer idGrupo;

        private QueryTicketAndNumeroAsyncTask(TicketDao ticketDao,Integer idGrupo,String numeroTicket) {
            this.ticketDao = ticketDao;
            this.numeroTicket = numeroTicket;
            this.idGrupo = idGrupo;
        }

        @Override
        protected Ticket doInBackground(Ticket... tickets) {
            return ticketDao.getTicketByIdGrupoAndNumeroticket(idGrupo,numeroTicket);
        }

    }

    private static class QueryTicketAsyncTask extends AsyncTask<Ticket, String, Ticket> {
        private TicketDao ticketDao;
        private String numeroTicket;

        private QueryTicketAsyncTask(TicketDao ticketDao,String numeroTicket) {
            this.ticketDao = ticketDao;
            this.numeroTicket = numeroTicket;
        }


        @Override
        protected Ticket doInBackground(Ticket... tickets) {
            return ticketDao.getTicket(numeroTicket);
        }

    }

    private static class InsertTicketAsyncTask extends AsyncTask<Ticket, Void, Void> {
        private TicketDao ticketDao;

        private InsertTicketAsyncTask(TicketDao ticketDao) {
            this.ticketDao = ticketDao;
        }

        @Override
        protected Void doInBackground(Ticket... ticket) {
            ticketDao.insert(ticket[0]);
            return null;
        }
    }

    private static class UpdateTicketAsyncTask extends AsyncTask<Ticket, Void, Void> {
        private TicketDao ticketDao;

        private UpdateTicketAsyncTask(TicketDao ticketDao) {
            this.ticketDao = ticketDao;
        }

        @Override
        protected Void doInBackground(Ticket... tickets) {
            ticketDao.update(tickets[0]);
            return null;
        }
    }

    private static class DeleteTicketAsyncTask extends AsyncTask<Ticket, Void, Void> {
        private TicketDao ticketDao;

        private DeleteTicketAsyncTask(TicketDao ticketDao) {
            this.ticketDao = ticketDao;
        }

        @Override
        protected Void doInBackground(Ticket... tickets) {
            ticketDao.delete(tickets[0]);
            return null;
        }
    }

    private static class DeleteAllTickesAsyncTask extends AsyncTask<Void, Void, Void> {
        private TicketDao ticketDao;

        private DeleteAllTickesAsyncTask(TicketDao ticketDao) {
            this.ticketDao = ticketDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ticketDao.deleteAllTicket();
            return null;
        }
    }

}

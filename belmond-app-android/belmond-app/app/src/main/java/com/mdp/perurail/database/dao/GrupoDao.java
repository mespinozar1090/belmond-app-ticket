package com.mdp.perurail.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mdp.perurail.database.model.Grupo;

import java.util.List;

@Dao
public interface GrupoDao {

    @Insert
    void insert(Grupo grupo);

    @Update
    void update(Grupo grupo);

    @Delete
    void delete(Grupo grupo);

    @Query("DELETE FROM grupo_table")
    void deleteAllGrupos();

    @Query("SELECT * FROM grupo_table")
    LiveData<List<Grupo>> getAllGrupos();

}

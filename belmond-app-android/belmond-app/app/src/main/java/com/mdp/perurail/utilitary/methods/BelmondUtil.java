package com.mdp.perurail.utilitary.methods;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.mdp.perurail.R;
import com.mdp.perurail.database.model.Grupo;
import com.mdp.perurail.database.model.Ticket;
import com.mdp.perurail.dto.DestinoDTO;
import com.mdp.perurail.dto.GrupoDTO;
import com.mdp.perurail.dto.NacionalidadDTO;
import com.mdp.perurail.dto.TicketDTO;
import com.mdp.perurail.dto.TipoDocumentoDTO;
import com.mdp.perurail.network.response.TicketResponse;

import net.glxn.qrgen.android.QRCode;
import net.glxn.qrgen.core.image.ImageType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BelmondUtil {
    private static ProgressDialog progressDialog;


    public static void showProgressDialogBelmond(Activity activity, String message){
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_autenticando,null);
        TextView showMessage = (TextView) view.findViewById(R.id.tvAccion);
        showMessage.setText(message);

        progressDialog = getProgressDialog(activity, message);
        progressDialog.show();
        progressDialog.setContentView(view);
        progressDialog.setCancelable(false);
    }

    public static ProgressDialog getProgressDialog(Activity activity, String message) {
        ProgressDialog progressDialog = new ProgressDialog(activity, R.style.screenDialog);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public static void hideProgreesDialogBelmond(){
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    public static Retrofit getRetrofitBuilderToken(String url,Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constans.ID_APP, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60,TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest  = chain.request().newBuilder()
                                .addHeader("jwt",  token)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();

        return new Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static Retrofit getRetrofitBuilder(String url) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60,TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();

        return new Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static void showModalCodeQR(Activity activity,String texto){
        View viewAddProject = activity.getLayoutInflater().inflate(R.layout.dialog_codigo_qr, null);
        AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(activity);
        alertDialog.setCancelable(true);
        alertDialog.setView(viewAddProject);

        ImageButton tvCerrar =  viewAddProject.findViewById(R.id.tvCerrar);
        ImageView imageQR = viewAddProject.findViewById(R.id.imgQR);
        imageQR.setImageBitmap(BelmondUtil.createQR(texto));

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
       /* dialog.getWindow().getAttributes().height =
                (int) (getDeviceMetrics(activity).heightPixels*0.8);*/
        dialog.show();

        tvCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }

    public final static boolean validarNombreGrupo(String target) {
        return Pattern.compile(".*[^A-Za-z0-9/\\s].*").matcher(target).matches();
    }

    public final static boolean validarNumeroDocumento(String target) {
        return Pattern.compile(".*[^A-Za-z0-9].*").matcher(target).matches();
    }

    public static Bitmap createQR(String texto){
       return QRCode.from(texto).bitmap();
    }

    public static Bitmap createQRForAdpter(String texto){
        return QRCode.from(texto).withColor(0xFF000000, 0xFFF5F4F2).withSize(250, 250).to(ImageType.PNG).bitmap();
      ///  QRGEncoder qrgEncoder = new QRGEncoder()
    }



    public static void setFragment(FragmentActivity fragmentActivity, Fragment fragment, Bundle bundle) {
         FragmentManager fragmentManager = null;
        if (null != fragment) {
            if(bundle != null){
                fragment.setArguments(bundle);
            }
            fragmentManager = fragmentActivity.getSupportFragmentManager();
            boolean existFragment = false;
            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments != null) {
                for (Fragment mFragment : fragments) {
                    if (mFragment != null && mFragment.isVisible()) {
                        if (mFragment.getClass().getName().equals(fragment.getClass().getSimpleName())) {
                            existFragment = true;
                        }
                    }
                }
            }
            if (!existFragment) {
                fragmentManager.beginTransaction()
                    // .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.fragment_container, fragment)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();
            } else { fragmentManager.beginTransaction().remove(fragment)
                    .replace(R.id.fragment_container, fragment)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();
            }
        }
    }

    public static DisplayMetrics getDeviceMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }


    public static List<TicketDTO> castListToTicketModel(List<Ticket> tickets){
        List<TicketDTO> listTickeModel = new ArrayList<>();
        TicketDTO ticketDTO = null;
        for(Ticket ticket : tickets){
            ticketDTO = new TicketDTO();
            ticketDTO.setId(ticket.getId());
            ticketDTO.setEsModifi(ticket.getEsModifi());
            ticketDTO.setIdGrupo(ticket.getIdGrupo());
            ticketDTO.setNumeroTicket(ticket.getNumeroTicket());
            ticketDTO.setNombrePasajero(ticket.getNombrePasajero());
            ticketDTO.setRuta(ticket.getRuta());
            ticketDTO.setTipoTren(ticket.getTipoTren());
            ticketDTO.setVagon(ticket.getVagon());
            ticketDTO.setNumeroAsiento(ticket.getNumeroAsiento());
            ticketDTO.setFechaSalida(ticket.getFechaSalida());
            ticketDTO.setHoraSalida(ticket.getHoraSalida());
            ticketDTO.setHoraSalida(ticket.getHoraSalida());
            ticketDTO.setEstadoModificacion(ticket.getEstadoModificacion());
            ticketDTO.setRutaArchivoOnboarding(ticket.getRutaArchivoOnboarding());
            ticketDTO.setRutaArchivoDocumentoElectronico(ticket.getRutaArchivoDocumentoElectronico());
            ticketDTO.setCodigoHash(ticket.getCodigoHash());
            listTickeModel.add(ticketDTO);
        }
        return listTickeModel;
    }


    public static List<GrupoDTO> castListGroupModel(List<Grupo> grupos){
        List<GrupoDTO> listGroupModel = new ArrayList<>();
        GrupoDTO grupoDTO = null;
        for(Grupo grupo : grupos){
            grupoDTO = new GrupoDTO();
            grupoDTO.setId(grupo.getId());
            grupoDTO.setName(grupo.getNombreGrupo());
            listGroupModel.add(grupoDTO);
        }
        return  listGroupModel;
    }

    public  static  Grupo castToGrupo (GrupoDTO grupoDTO){ // recibe objeto dto y retora objeto grupo
        Grupo oGrupo = new Grupo();
        oGrupo.setId(grupoDTO.getId());
        oGrupo.setNombreGrupo(grupoDTO.getName());
        return oGrupo;
    }

    public static Ticket castToTicket(TicketDTO ticketDTO){

        Ticket ticket = new Ticket();
        ticket.setId(ticketDTO.getId());
        ticket.setIdGrupo(ticketDTO.getIdGrupo());
        ticket.setNumeroTicket(ticketDTO.getNumeroTicket());
        ticket.setNombrePasajero(ticketDTO.getNombrePasajero());
        ticket.setRuta(ticketDTO.getRuta());
        ticket.setTipoTren(ticketDTO.getTipoTren());
        ticket.setVagon(ticketDTO.getVagon());
        ticket.setNumeroAsiento(ticketDTO.getNumeroAsiento());
        ticket.setFechaSalida(ticketDTO.getFechaSalida());
        ticket.setHoraSalida(ticketDTO.getHoraSalida());
        ticket.setHoraSalida(ticketDTO.getHoraSalida());
        ticket.setEstadoModificacion(ticketDTO.getEstadoModificacion());
        ticket.setRutaArchivoOnboarding(ticketDTO.getRutaArchivoOnboarding());
        ticket.setRutaArchivoDocumentoElectronico(ticketDTO.getRutaArchivoDocumentoElectronico());
        ticket.setCodigoHash(ticketDTO.getCodigoHash());
        return ticket;
    }


    public  static  List<DestinoDTO> listDestinos(){
        List<DestinoDTO> destinoDTOS = new ArrayList<>();
        destinoDTOS.add(new DestinoDTO(1,"Machu Picchu"));
        destinoDTOS.add(new DestinoDTO(2,"Ollantaytambo"));
        destinoDTOS.add(new DestinoDTO(3,"Cusco"));
        destinoDTOS.add(new DestinoDTO(4,"Puno"));
        destinoDTOS.add(new DestinoDTO(5,"Urubamba"));
        destinoDTOS.add(new DestinoDTO(6,"Arequipa"));
        return  destinoDTOS;
    }


    public static  List<NacionalidadDTO> listaNacionalidad(){
        List<NacionalidadDTO> nacionalidadDTOS = new ArrayList<>();

        nacionalidadDTOS.add(new NacionalidadDTO("AFG","Afghanistan"));
        nacionalidadDTOS.add(new NacionalidadDTO("ALB","Albania"));
        nacionalidadDTOS.add(new NacionalidadDTO("ALE","Alemania"));
        nacionalidadDTOS.add(new NacionalidadDTO("AND","Andorra"));
        nacionalidadDTOS.add(new NacionalidadDTO("ANG","Angola"));
        nacionalidadDTOS.add(new NacionalidadDTO("AIA","Anguila"));
        nacionalidadDTOS.add(new NacionalidadDTO("ATA","Antártica"));
        nacionalidadDTOS.add(new NacionalidadDTO("ATG","Antigua y Barbuda"));
        nacionalidadDTOS.add(new NacionalidadDTO("AN","Antillas Neerlandesas"));
        nacionalidadDTOS.add(new NacionalidadDTO("ARA","Arabia Saudi"));
        nacionalidadDTOS.add(new NacionalidadDTO("DZA","Argelia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ARG","Argentina"));
        nacionalidadDTOS.add(new NacionalidadDTO("ARM","Armenia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ARU","Aruba"));
        nacionalidadDTOS.add(new NacionalidadDTO("AUS","Australia"));
        nacionalidadDTOS.add(new NacionalidadDTO("AUT","Austria"));
        nacionalidadDTOS.add(new NacionalidadDTO("AZE","Azerbaiyán"));
        nacionalidadDTOS.add(new NacionalidadDTO("BAH","Bahamas"));
        nacionalidadDTOS.add(new NacionalidadDTO("BAN","Bangladesh"));
        nacionalidadDTOS.add(new NacionalidadDTO("BAR","Barbados"));
        nacionalidadDTOS.add(new NacionalidadDTO("BHR","Baréin"));
        nacionalidadDTOS.add(new NacionalidadDTO("BEL","Bélgica"));
        nacionalidadDTOS.add(new NacionalidadDTO("BLZ","Belize"));
        nacionalidadDTOS.add(new NacionalidadDTO("BEN","Benin"));
        nacionalidadDTOS.add(new NacionalidadDTO("BER","Bermudas"));
        nacionalidadDTOS.add(new NacionalidadDTO("BIE","Bielorrusia"));
        nacionalidadDTOS.add(new NacionalidadDTO("BOL","Bolivia"));
        nacionalidadDTOS.add(new NacionalidadDTO("BIH","Bosnia y Herzegowina"));
        nacionalidadDTOS.add(new NacionalidadDTO("BWA","BotsUana"));
        nacionalidadDTOS.add(new NacionalidadDTO("BRA","Brasil"));
        nacionalidadDTOS.add(new NacionalidadDTO("BRN","Brunéi Darussalam"));
        nacionalidadDTOS.add(new NacionalidadDTO("BUL","Bulgaria"));
        nacionalidadDTOS.add(new NacionalidadDTO("BFA","Burkina Faso"));
        nacionalidadDTOS.add(new NacionalidadDTO("MMR","Burma"));
        nacionalidadDTOS.add(new NacionalidadDTO("BDI","Burundi"));
        nacionalidadDTOS.add(new NacionalidadDTO("BTN","Bután"));
        nacionalidadDTOS.add(new NacionalidadDTO("CPV","Cabo Verde"));
        nacionalidadDTOS.add(new NacionalidadDTO("KHM","Camboya"));
        nacionalidadDTOS.add(new NacionalidadDTO("CMR","Camerún"));
        nacionalidadDTOS.add(new NacionalidadDTO("CAN","Canadá"));
        nacionalidadDTOS.add(new NacionalidadDTO("TCD","Chad"));
        nacionalidadDTOS.add(new NacionalidadDTO("CHL","Chile"));
        nacionalidadDTOS.add(new NacionalidadDTO("CHI","China"));
        nacionalidadDTOS.add(new NacionalidadDTO("CHP","Chipre"));
        nacionalidadDTOS.add(new NacionalidadDTO("COL","Colombia"));
        nacionalidadDTOS.add(new NacionalidadDTO("COM","Comoras"));
        nacionalidadDTOS.add(new NacionalidadDTO("COG","Congo"));
        nacionalidadDTOS.add(new NacionalidadDTO("CRN","Corea del Norte"));
        nacionalidadDTOS.add(new NacionalidadDTO("CRS","Corea del Sur"));
        nacionalidadDTOS.add(new NacionalidadDTO("CIV","Costa de Marfil"));
        nacionalidadDTOS.add(new NacionalidadDTO("COS","Costa Rica"));
        nacionalidadDTOS.add(new NacionalidadDTO("CRO","Croacia"));
        nacionalidadDTOS.add(new NacionalidadDTO("CUB","Cuba"));
        nacionalidadDTOS.add(new NacionalidadDTO("DIN","Dinamarca"));
        nacionalidadDTOS.add(new NacionalidadDTO("DOM","Dominica"));
        nacionalidadDTOS.add(new NacionalidadDTO("ECU","Ecuador"));
        nacionalidadDTOS.add(new NacionalidadDTO("EGI","Egipto"));
        nacionalidadDTOS.add(new NacionalidadDTO("ELS","El Salvador"));
        nacionalidadDTOS.add(new NacionalidadDTO("EMI","Emiratos Arabes Unidos"));
        nacionalidadDTOS.add(new NacionalidadDTO("ERI","Eritrea"));
        nacionalidadDTOS.add(new NacionalidadDTO("ESQ","Eslovaquia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ESL","Eslovenia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ESP","España"));
        nacionalidadDTOS.add(new NacionalidadDTO("EUA","Estados Unidos"));
        nacionalidadDTOS.add(new NacionalidadDTO("EST","Estonia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ETI","Etiopía"));
        nacionalidadDTOS.add(new NacionalidadDTO("FIL","Filipinas"));
        nacionalidadDTOS.add(new NacionalidadDTO("FI","Finlandia"));
        nacionalidadDTOS.add(new NacionalidadDTO("FIY","Fiyi"));
        nacionalidadDTOS.add(new NacionalidadDTO("FRA","Francia"));
        nacionalidadDTOS.add(new NacionalidadDTO("GAB","Gabón"));
        nacionalidadDTOS.add(new NacionalidadDTO("GAM","Gambia"));
        nacionalidadDTOS.add(new NacionalidadDTO("GEO","Georgia"));
        nacionalidadDTOS.add(new NacionalidadDTO("GHA","Ghana"));
        nacionalidadDTOS.add(new NacionalidadDTO("GIB","Gibraltar"));
        nacionalidadDTOS.add(new NacionalidadDTO("GRA","Gran Bretaña"));
        nacionalidadDTOS.add(new NacionalidadDTO("GRD","Granada"));
        nacionalidadDTOS.add(new NacionalidadDTO("GRE","Grecia"));
        nacionalidadDTOS.add(new NacionalidadDTO("GRL","Groenlandia"));
        nacionalidadDTOS.add(new NacionalidadDTO("GLP","Guadalupe"));
        nacionalidadDTOS.add(new NacionalidadDTO("GUM","Guam"));
        nacionalidadDTOS.add(new NacionalidadDTO("GUA","Guatemala"));
        nacionalidadDTOS.add(new NacionalidadDTO("GNA","Guinea"));
        nacionalidadDTOS.add(new NacionalidadDTO("GEC","Guinea Ecuatorial"));
        nacionalidadDTOS.add(new NacionalidadDTO("GUI","Guinea-Bissau"));
        nacionalidadDTOS.add(new NacionalidadDTO("GUY","Guayana"));
        nacionalidadDTOS.add(new NacionalidadDTO("GYF","Guayana Francesa"));
        nacionalidadDTOS.add(new NacionalidadDTO("HAI","Haití"));
        nacionalidadDTOS.add(new NacionalidadDTO("HOL","Holanda"));
        nacionalidadDTOS.add(new NacionalidadDTO("HON","Honduras"));
        nacionalidadDTOS.add(new NacionalidadDTO("HKO","Hong Kong"));
        nacionalidadDTOS.add(new NacionalidadDTO("HUN","Hungría"));
        nacionalidadDTOS.add(new NacionalidadDTO("ID","India"));
        nacionalidadDTOS.add(new NacionalidadDTO("IDN","Indonesia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ING","Inglaterra"));
        nacionalidadDTOS.add(new NacionalidadDTO("IRN","Iran"));
        nacionalidadDTOS.add(new NacionalidadDTO("IRQ","Iraq"));
        nacionalidadDTOS.add(new NacionalidadDTO("IRL","Irlanda"));
        nacionalidadDTOS.add(new NacionalidadDTO("BVT","Isla Bouvet"));
        nacionalidadDTOS.add(new NacionalidadDTO("CXR","Isla de Navidad"));
        nacionalidadDTOS.add(new NacionalidadDTO("NIU","Isla Niue"));
        nacionalidadDTOS.add(new NacionalidadDTO("NFK","Isla Norfolk"));
        nacionalidadDTOS.add(new NacionalidadDTO("PCN","Isla Pitcairn "));
        nacionalidadDTOS.add(new NacionalidadDTO("ISL","Islandia"));
        nacionalidadDTOS.add(new NacionalidadDTO("CYM","Islas Caimán"));
        nacionalidadDTOS.add(new NacionalidadDTO("CCK","Islas Cocos (Keeling)"));
        nacionalidadDTOS.add(new NacionalidadDTO("COK","Islas Cook"));
        nacionalidadDTOS.add(new NacionalidadDTO("FRO","Islas Faroe"));
        nacionalidadDTOS.add(new NacionalidadDTO("HMD","Islas Heard y Mcdonald"));
        nacionalidadDTOS.add(new NacionalidadDTO("FLK","Islas Malvinas (Falkland)"));
        nacionalidadDTOS.add(new NacionalidadDTO("MNP","Islas Malvinas del Norte"));
        nacionalidadDTOS.add(new NacionalidadDTO("UMI","Islas Perifericas Menores de Estados Unidos"));
        nacionalidadDTOS.add(new NacionalidadDTO("SJM","Islas Svalbard y Jan Mayen"));
        nacionalidadDTOS.add(new NacionalidadDTO("TCA","Islas turcas y Caicos"));
        nacionalidadDTOS.add(new NacionalidadDTO("VGB","Islas Vírgenes Británicas"));
        nacionalidadDTOS.add(new NacionalidadDTO("VIR","Islas Vírgenes de los Estados Unidos"));
        nacionalidadDTOS.add(new NacionalidadDTO("WLF","Islas Wallis y Futuna"));
        nacionalidadDTOS.add(new NacionalidadDTO("ISR","Israel"));
        nacionalidadDTOS.add(new NacionalidadDTO("ITA","Italia"));
        nacionalidadDTOS.add(new NacionalidadDTO("JAM","Jamaica"));
        nacionalidadDTOS.add(new NacionalidadDTO("JAP","Japón"));
        nacionalidadDTOS.add(new NacionalidadDTO("JOR","Jordania"));
        nacionalidadDTOS.add(new NacionalidadDTO("KAZ","Kazajstán"));
        nacionalidadDTOS.add(new NacionalidadDTO("KEN","Kenia"));
        nacionalidadDTOS.add(new NacionalidadDTO("KRG","Kirguistán"));
        nacionalidadDTOS.add(new NacionalidadDTO("KRB","Kiribati"));
        nacionalidadDTOS.add(new NacionalidadDTO("KOR","Korea"));
        nacionalidadDTOS.add(new NacionalidadDTO("KUW","Kuwait"));
        nacionalidadDTOS.add(new NacionalidadDTO("COD","La República Democrática del Congo"));
        nacionalidadDTOS.add(new NacionalidadDTO("lAO","Laos"));
        nacionalidadDTOS.add(new NacionalidadDTO("LAT","Latvia"));
        nacionalidadDTOS.add(new NacionalidadDTO("LES","Leshoto"));
        nacionalidadDTOS.add(new NacionalidadDTO("LET","Letonia"));
        nacionalidadDTOS.add(new NacionalidadDTO("LIB","Líbano"));
        nacionalidadDTOS.add(new NacionalidadDTO("LBR","Liberia"));
        nacionalidadDTOS.add(new NacionalidadDTO("LBA","Libia"));
        nacionalidadDTOS.add(new NacionalidadDTO("LIE","Liechtenstein"));
        nacionalidadDTOS.add(new NacionalidadDTO("LIT","Lituania"));
        nacionalidadDTOS.add(new NacionalidadDTO("LUX","Luxemburgo"));
        nacionalidadDTOS.add(new NacionalidadDTO("MAC","Macao"));
        nacionalidadDTOS.add(new NacionalidadDTO("MCD","Macedonia"));
        nacionalidadDTOS.add(new NacionalidadDTO("MAD","Madagascar"));
        nacionalidadDTOS.add(new NacionalidadDTO("MAL","Malasia"));
        nacionalidadDTOS.add(new NacionalidadDTO("MLW","Malawi"));
        nacionalidadDTOS.add(new NacionalidadDTO("MLD","Maldivas"));
        nacionalidadDTOS.add(new NacionalidadDTO("MLI","Mali"));
        nacionalidadDTOS.add(new NacionalidadDTO("MLT","Malta"));
        nacionalidadDTOS.add(new NacionalidadDTO("MRR","Marruecos"));
        nacionalidadDTOS.add(new NacionalidadDTO("MRS","Marshall Islands"));
        nacionalidadDTOS.add(new NacionalidadDTO("MAR","Martinica"));
        nacionalidadDTOS.add(new NacionalidadDTO("MRC","Mauricio"));
        nacionalidadDTOS.add(new NacionalidadDTO("MRT","Mauritania"));
        nacionalidadDTOS.add(new NacionalidadDTO("MAY","Mayotte"));
        nacionalidadDTOS.add(new NacionalidadDTO("MEX","Mexico"));
        nacionalidadDTOS.add(new NacionalidadDTO("MIC","Micronesia"));
        nacionalidadDTOS.add(new NacionalidadDTO("MOL","Moldavia"));
        nacionalidadDTOS.add(new NacionalidadDTO("MON","Mónaco"));
        nacionalidadDTOS.add(new NacionalidadDTO("MNG","Mongolia"));
        nacionalidadDTOS.add(new NacionalidadDTO("MSR","Monserrat"));
        nacionalidadDTOS.add(new NacionalidadDTO("MOZ","Mozambique"));
        nacionalidadDTOS.add(new NacionalidadDTO("NAM","Namibia"));
        nacionalidadDTOS.add(new NacionalidadDTO("NRU","Nauru"));
        nacionalidadDTOS.add(new NacionalidadDTO("NEP","Nepal"));
        nacionalidadDTOS.add(new NacionalidadDTO("NGA","Nigeria"));
        nacionalidadDTOS.add(new NacionalidadDTO("NOR","Noruega"));
        nacionalidadDTOS.add(new NacionalidadDTO("NCL","Nueva Caledonia"));
        nacionalidadDTOS.add(new NacionalidadDTO("NUE","Nueva Zelanda"));
        nacionalidadDTOS.add(new NacionalidadDTO("OMN","Omán"));
        nacionalidadDTOS.add(new NacionalidadDTO("HOL","Países Bajos/Holanda"));
        nacionalidadDTOS.add(new NacionalidadDTO("PAK","Pakistan"));
        nacionalidadDTOS.add(new NacionalidadDTO("PAL","Palau"));
        nacionalidadDTOS.add(new NacionalidadDTO("PLE","Palestina"));
        nacionalidadDTOS.add(new NacionalidadDTO("PAN","Panamá"));
        nacionalidadDTOS.add(new NacionalidadDTO("PNG","Papúa Nueva Guinea"));
        nacionalidadDTOS.add(new NacionalidadDTO("PAR","Paraguay"));
        nacionalidadDTOS.add(new NacionalidadDTO("PER","Perú"));
        nacionalidadDTOS.add(new NacionalidadDTO("PYF","Polinesia Francesa"));
        nacionalidadDTOS.add(new NacionalidadDTO("POL","Polonia"));
        nacionalidadDTOS.add(new NacionalidadDTO("POR","Portugal"));
        nacionalidadDTOS.add(new NacionalidadDTO("PRI","Puerto Rico"));
        nacionalidadDTOS.add(new NacionalidadDTO("QAT","Qatar"));
        nacionalidadDTOS.add(new NacionalidadDTO("SYR","República Árabe Siria"));
        nacionalidadDTOS.add(new NacionalidadDTO("CAF","República Centroafricana"));
        nacionalidadDTOS.add(new NacionalidadDTO("RCH","Republica checa"));
        nacionalidadDTOS.add(new NacionalidadDTO("RDO","Republica Dominicana"));
        nacionalidadDTOS.add(new NacionalidadDTO("TZA","República Unida de Tanzanía"));
        nacionalidadDTOS.add(new NacionalidadDTO("EU","Reunión"));
        nacionalidadDTOS.add(new NacionalidadDTO("RWA","Ruanda"));
        nacionalidadDTOS.add(new NacionalidadDTO("RUM","Rumania"));
        nacionalidadDTOS.add(new NacionalidadDTO("RUS","Rusia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ESH","Sáhara Occidental"));
        nacionalidadDTOS.add(new NacionalidadDTO("KNA","Saint Kitts y Nevis"));
        nacionalidadDTOS.add(new NacionalidadDTO("WSM","Samoa"));
        nacionalidadDTOS.add(new NacionalidadDTO("ASM","Samoa Americana"));
        nacionalidadDTOS.add(new NacionalidadDTO("RSM","San Marino"));
        nacionalidadDTOS.add(new NacionalidadDTO("SMP","San Pedro y Miquelón"));
        nacionalidadDTOS.add(new NacionalidadDTO("VCT","San Vicente y Las Granadinas"));
        nacionalidadDTOS.add(new NacionalidadDTO("SHN","Santa Helena"));
        nacionalidadDTOS.add(new NacionalidadDTO("LCA","Santa Lucía"));
        nacionalidadDTOS.add(new NacionalidadDTO("STP","Santo Tomé y Príncipe"));
        nacionalidadDTOS.add(new NacionalidadDTO("SEN","Senegal"));
        nacionalidadDTOS.add(new NacionalidadDTO("SER","Serbia"));
        nacionalidadDTOS.add(new NacionalidadDTO("SLE","Sierra Leone"));
        nacionalidadDTOS.add(new NacionalidadDTO("SLE","Sierra Leone"));
        nacionalidadDTOS.add(new NacionalidadDTO("SIN","Singapur"));
        nacionalidadDTOS.add(new NacionalidadDTO("SOM","Somalia"));
        nacionalidadDTOS.add(new NacionalidadDTO("SRI","Sri Lanka"));
        nacionalidadDTOS.add(new NacionalidadDTO("SWZ","Suazilandia "));
        nacionalidadDTOS.add(new NacionalidadDTO("SDA","Sudáfrica"));
        nacionalidadDTOS.add(new NacionalidadDTO("ZAF","Sudáfrica"));
        nacionalidadDTOS.add(new NacionalidadDTO("SUE","Suecia"));
        nacionalidadDTOS.add(new NacionalidadDTO("SUI","Suiza"));
        nacionalidadDTOS.add(new NacionalidadDTO("CHE","Suiza"));
        nacionalidadDTOS.add(new NacionalidadDTO("TAI","Tailandia"));
        nacionalidadDTOS.add(new NacionalidadDTO("TAW","Taiwán"));
        nacionalidadDTOS.add(new NacionalidadDTO("TJK","Tayikistán"));
        nacionalidadDTOS.add(new NacionalidadDTO("IOT","Territorio Británico del Océano Índico"));
        nacionalidadDTOS.add(new NacionalidadDTO("TLS","Timor Oriental"));
        nacionalidadDTOS.add(new NacionalidadDTO("TGO","Togo"));
        nacionalidadDTOS.add(new NacionalidadDTO("TKL","Tokelau"));
        nacionalidadDTOS.add(new NacionalidadDTO("TON","Tonga"));
        nacionalidadDTOS.add(new NacionalidadDTO("TYT","Trinidad y Tobago"));
        nacionalidadDTOS.add(new NacionalidadDTO("TUN","Túnez"));
        nacionalidadDTOS.add(new NacionalidadDTO("TKM","Turkmenistán"));
        nacionalidadDTOS.add(new NacionalidadDTO("TUR","Turquía"));
        nacionalidadDTOS.add(new NacionalidadDTO("TUV","Tuvalu"));
        nacionalidadDTOS.add(new NacionalidadDTO("UCR","Ucrania"));
        nacionalidadDTOS.add(new NacionalidadDTO("UGA","Uganda"));
        nacionalidadDTOS.add(new NacionalidadDTO("URU","Uruguay"));
        nacionalidadDTOS.add(new NacionalidadDTO("UZB","Uzbekistán"));
        nacionalidadDTOS.add(new NacionalidadDTO("VUT","Vanuatu"));
        nacionalidadDTOS.add(new NacionalidadDTO("VAT","Vaticano"));
        nacionalidadDTOS.add(new NacionalidadDTO("VEN","Venezuela"));
        nacionalidadDTOS.add(new NacionalidadDTO("VIE","Vietnam"));
        nacionalidadDTOS.add(new NacionalidadDTO("YEM","Yemen"));
        nacionalidadDTOS.add(new NacionalidadDTO("YIB","Yibuti"));
        nacionalidadDTOS.add(new NacionalidadDTO("YUG","Yugoslavia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ZAM","Zambia"));
        nacionalidadDTOS.add(new NacionalidadDTO("ZIM","Zimbabue"));

        return nacionalidadDTOS;
    }

    public static void eliminarFichero(File fichero) {
        if (!fichero.exists()) {
           Log.e("FICHERO :","El archivo data no existe.");
        } else {
            fichero.delete();
            Log.e("FICHERO :","El archivo fue eliminado con exito.");
        }
    }

    public static List<Ticket> castTicketResponseToTickeDB(List<TicketResponse> tickets){
        List<Ticket> listTickeModel = new ArrayList<>();
        Ticket ticketDTO = null;
        for(TicketResponse ticket : tickets){
            ticketDTO = new Ticket();
            ticketDTO.setIdGrupo(0);
            ticketDTO.setEsModifi(ticket.getEsModifi());
            ticketDTO.setNumeroTicket(ticket.getNumTicket());
            ticketDTO.setNombrePasajero(ticket.getNomPasaj());
            ticketDTO.setRuta(ticket.getRuta());
            ticketDTO.setTipoTren(ticket.getTipTren());
            ticketDTO.setVagon(ticket.getVagon());
            ticketDTO.setNumeroAsiento(ticket.getAsiento());
            ticketDTO.setFechaSalida(ticket.getFecSalida());
            ticketDTO.setHoraSalida(ticket.getHorSalida());
            ticketDTO.setEstadoModificacion(ticket.getEsModifi());
            ticketDTO.setRutaArchivoOnboarding(ticket.getUrlTicket());
            ticketDTO.setRutaArchivoDocumentoElectronico(ticket.getUrlDocu());
            ticketDTO.setCodigoHash(ticket.getCodHash());
            listTickeModel.add(ticketDTO);
        }
        return listTickeModel;
    }

    public static  List<TipoDocumentoDTO> listaTipoDocumento(){
        List<TipoDocumentoDTO> list =  new ArrayList<>();
        list.add(new TipoDocumentoDTO("DNI","Documento Nacional de Identidad"));
        list.add(new TipoDocumentoDTO("PAS","Pasaporte"));
        list.add(new TipoDocumentoDTO("CEX","Carnet de Extranjería"));
        list.add(new TipoDocumentoDTO("PNA","Partida de Nacimiento"));
        list.add(new TipoDocumentoDTO("CID","Carnet de Identidad"));
        list.add(new TipoDocumentoDTO("RUC","RUC"));
        return list;
    }


    public static String getTicketDirectory() {
        return checkDirectory(getDirApp() + Constans.DOCUMENTO_DIRECTORY);
    }

    public static String checkDirectory(String directory) {
        File dirDB = new File(directory);
        if (!dirDB.exists()) {
            dirDB.mkdirs();
        }
        return directory;
    }


    public static String getDirApp() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Constans.APP_DIRECTORY;
    }

    public static String writeResponseBodyToDisk(ResponseBody body, String nombre) {
        try {

            File fileCreate = new File(BelmondUtil.getTicketDirectory());
            File futureStudioIconFile = new File(fileCreate, nombre+".pdf");

            Log.e("ABSOLITEPAH :",futureStudioIconFile.getAbsolutePath());

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.e("FILEDOWNLOAD", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return futureStudioIconFile.getAbsolutePath();
            } catch (IOException e) {
                Log.e("IOException", ""+e.getMessage());
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            Log.e("IOException", ""+e.getMessage());
            return null;

        }
    }

    public static String codTipoDocumento(int position){
        String codigoNacionalidad=null;
        switch (position){
            case 1: codigoNacionalidad = "DNI"; break;
            case 2: codigoNacionalidad = "PAS"; break;
            case 3: codigoNacionalidad = "CEX"; break;
            case 4: codigoNacionalidad = "PNA"; break;
            case 5: codigoNacionalidad = "CID"; break;
            case 6: codigoNacionalidad = "RUC"; break;
        }
        return codigoNacionalidad;
    }

    public  static String codPais(int position){
       String codigoPais=null;
        switch (position){
            case 1: codigoPais="AFG"; break;
            case 2: codigoPais="ALB"; break;
            case 3: codigoPais="ALE"; break;
            case 4: codigoPais="AND"; break;
            case 5: codigoPais="ANG"; break;
            case 6: codigoPais="AIA"; break;
            case 7: codigoPais="ATA"; break;
            case 8: codigoPais="ATG"; break;
            case 9: codigoPais="AN"; break;
            case 10: codigoPais="ARA"; break;
            case 11: codigoPais="DZA"; break;
            case 12: codigoPais="ARG"; break;
            case 13: codigoPais="ARM"; break;
            case 14: codigoPais="ARU"; break;
            case 15: codigoPais="AUS"; break;
            case 16: codigoPais="AUT"; break;
            case 17: codigoPais="AZE"; break;
            case 18: codigoPais="BAH"; break;
            case 19: codigoPais="BAN"; break;
            case 20: codigoPais="BAR"; break;
            case 21: codigoPais="BHR"; break;
            case 22: codigoPais="BEL"; break;
            case 23: codigoPais="BLZ"; break;
            case 24: codigoPais="BEN"; break;
            case 25: codigoPais="BER"; break;
            case 26: codigoPais="BIE"; break;
            case 27: codigoPais="BOL"; break;
            case 28: codigoPais="BIH"; break;
            case 29: codigoPais="BWA"; break;
            case 30: codigoPais="BRA"; break;
            case 31: codigoPais="BRN"; break;
            case 32: codigoPais="BUL"; break;
            case 33: codigoPais="BFA"; break;
            case 34: codigoPais="MMR"; break;
            case 35: codigoPais="BDI"; break;
            case 36: codigoPais="BTN"; break;
            case 37: codigoPais="CPV"; break;
            case 38: codigoPais="KHM"; break;
            case 39: codigoPais="CMR"; break;
            case 40: codigoPais="CAN"; break;
            case 41: codigoPais="TCD"; break;
            case 42: codigoPais="CHL"; break;
            case 43: codigoPais="CHI"; break;
            case 44: codigoPais="CHP"; break;
            case 45: codigoPais="COL"; break;
            case 46: codigoPais="COM"; break;
            case 47: codigoPais="COG"; break;
            case 48: codigoPais="CRN"; break;
            case 49: codigoPais="CRS"; break;
            case 50: codigoPais="CIV"; break;
            case 51: codigoPais="COS"; break;
            case 52: codigoPais="CRO"; break;
            case 53: codigoPais="CUB"; break;
            case 54: codigoPais="DIN"; break;
            case 55: codigoPais="DOM"; break;
            case 56: codigoPais="ECU"; break;
            case 57: codigoPais="EGI"; break;
            case 58: codigoPais="ELS"; break;
            case 59: codigoPais="EMI"; break;
            case 60: codigoPais="ERI"; break;
            case 61: codigoPais="ESQ"; break;
            case 62: codigoPais="ESL"; break;
            case 63: codigoPais="ESP"; break;
            case 64: codigoPais="EUA"; break;
            case 65: codigoPais="EST"; break;
            case 66: codigoPais="ETI"; break;
            case 67: codigoPais="FIL"; break;
            case 68: codigoPais="FI"; break;
            case 69: codigoPais="FIY"; break;
            case 70: codigoPais="FRA"; break;
            case 71: codigoPais="GAB"; break;
            case 72: codigoPais="GAM"; break;
            case 73: codigoPais="GEO"; break;
            case 74: codigoPais="GHA"; break;
            case 75: codigoPais="GIB"; break;
            case 76: codigoPais="GRA"; break;
            case 77: codigoPais="GRD"; break;
            case 78: codigoPais="GRE"; break;
            case 79: codigoPais="GRL"; break;
            case 80: codigoPais="GLP"; break;
            case 81: codigoPais="GUM"; break;
            case 82: codigoPais="GUA"; break;
            case 83: codigoPais="GNA"; break;
            case 84: codigoPais="GEC"; break;
            case 85: codigoPais="GUI"; break;
            case 86: codigoPais="GUY"; break;
            case 87: codigoPais="GYF"; break;
            case 88: codigoPais="HAI"; break;
            case 89: codigoPais="HOL"; break;
            case 90: codigoPais="HON"; break;
            case 91: codigoPais="HKO"; break;
            case 92: codigoPais="HUN"; break;
            case 93: codigoPais="ID"; break;
            case 94: codigoPais="IDN"; break;
            case 95: codigoPais="ING"; break;
            case 96: codigoPais="IRN"; break;
            case 97: codigoPais="IRQ"; break;
            case 98: codigoPais="IRL"; break;
            case 99: codigoPais="BVT"; break;
            case 100: codigoPais="CXR"; break;
            case 101: codigoPais="NIU"; break;
            case 102: codigoPais="NFK"; break;
            case 103: codigoPais="PCN"; break;
            case 104: codigoPais="ISL"; break;
            case 105: codigoPais="CYM"; break;
            case 106: codigoPais="CCK"; break;
            case 107: codigoPais="COK"; break;
            case 108: codigoPais="FRO"; break;
            case 109: codigoPais="HMD"; break;
            case 110: codigoPais="FLK"; break;
            case 111: codigoPais="MNP"; break;
            case 112: codigoPais="UMI"; break;
            case 113: codigoPais="SJM"; break;
            case 114: codigoPais="TCA"; break;
            case 115: codigoPais="VGB"; break;
            case 116: codigoPais="VIR"; break;
            case 117: codigoPais="WLF"; break;
            case 118: codigoPais="ISR"; break;
            case 119: codigoPais="ITA"; break;
            case 120: codigoPais="JAM"; break;
            case 121: codigoPais="JAP"; break;
            case 122: codigoPais="JOR"; break;
            case 123: codigoPais="KAZ"; break;
            case 124: codigoPais="KEN"; break;
            case 125: codigoPais="KRG"; break;
            case 126: codigoPais="KRB"; break;
            case 127: codigoPais="KOR"; break;
            case 128: codigoPais="KUW"; break;
            case 129: codigoPais="COD"; break;
            case 130: codigoPais="LAO"; break;
            case 131: codigoPais="LAT"; break;
            case 132: codigoPais="LES"; break;
            case 133: codigoPais="LET"; break;
            case 134: codigoPais="LIB"; break;
            case 135: codigoPais="LBR"; break;
            case 136: codigoPais="LBA"; break;
            case 137: codigoPais="LIE"; break;
            case 138: codigoPais="LIT"; break;
            case 139: codigoPais="LUX"; break;
            case 140: codigoPais="MAC"; break;
            case 141: codigoPais="MCD"; break;
            case 142: codigoPais="MAD"; break;
            case 143: codigoPais="MAL"; break;
            case 144: codigoPais="MLW"; break;
            case 145: codigoPais="MLD"; break;
            case 146: codigoPais="MLI"; break;
            case 147: codigoPais="MLT"; break;
            case 148: codigoPais="MRR"; break;
            case 149: codigoPais="MRS"; break;
            case 150: codigoPais="MAR"; break;
            case 151: codigoPais="MRC"; break;
            case 152: codigoPais="MRT"; break;
            case 153: codigoPais="MAY"; break;
            case 154: codigoPais="MEX"; break;
            case 155: codigoPais="MIC"; break;
            case 156: codigoPais="MOL"; break;
            case 157: codigoPais="MON"; break;
            case 158: codigoPais="MNG"; break;
            case 159: codigoPais="MRS"; break;
            case 160: codigoPais="MOZ"; break;
            case 161: codigoPais="NAM"; break;
            case 162: codigoPais="NRU"; break;
            case 163: codigoPais="NEP"; break;
            case 164: codigoPais="NIC"; break;
            case 165: codigoPais="NER"; break;
            case 166: codigoPais="NGA"; break;
            case 167: codigoPais="NOR"; break;
            case 168: codigoPais="NCL"; break;
            case 169: codigoPais="NUE"; break;
            case 170: codigoPais="OMN"; break;
            case 171: codigoPais="NLD"; break;
            case 172: codigoPais="PAK"; break;
            case 173: codigoPais="PAL"; break;
            case 174: codigoPais="PLE"; break;
            case 175: codigoPais="PAN"; break;
            case 176: codigoPais="PNG"; break;
            case 177: codigoPais="PAR"; break;
            case 178: codigoPais="PER"; break;
            case 179: codigoPais="PYF"; break;
            case 180: codigoPais="POL"; break;
            case 181: codigoPais="POR"; break;
            case 182: codigoPais="PRI"; break;
            case 183: codigoPais="QAT"; break;
            case 184: codigoPais="SYR"; break;
            case 185: codigoPais="CAF"; break;
            case 186: codigoPais="RCH"; break;
            case 187: codigoPais="RDO"; break;
            case 188: codigoPais="TZA"; break;
            case 189: codigoPais="EU"; break;
            case 190: codigoPais="RWA"; break;
            case 191: codigoPais="RUM"; break;
            case 192: codigoPais="RUS"; break;
            case 193: codigoPais="ESH"; break;
            case 194: codigoPais="KNA"; break;
            case 195: codigoPais="WSM"; break;
            case 196: codigoPais="ASM"; break;
            case 197: codigoPais="RSM"; break;
            case 198: codigoPais="SMP"; break;
            case 199: codigoPais="VCT"; break;
            case 200: codigoPais="SHN"; break;
            case 201: codigoPais="LCA"; break;
            case 202: codigoPais="STP"; break;
            case 203: codigoPais="SEN"; break;
            case 204: codigoPais="SER"; break;
            case 205: codigoPais="SLE"; break;
            case 206: codigoPais="SIN"; break;
            case 207: codigoPais="SOM"; break;
            case 208: codigoPais="SRI"; break;
            case 209: codigoPais="SWZ"; break;
            case 210: codigoPais="SDA"; break;
            case 211: codigoPais="ZAF"; break;
            case 212: codigoPais="SDN"; break;
            case 213: codigoPais="SUE"; break;
            case 214: codigoPais="SUI"; break;
            case 215: codigoPais="CHE"; break;
            case 216: codigoPais="TAI"; break;
            case 217: codigoPais="TAW"; break;
            case 218: codigoPais="TJK"; break;
            case 219: codigoPais="IOT"; break;
            case 220: codigoPais="TLS"; break;
            case 221: codigoPais="TGO"; break;
            case 222: codigoPais="TKL"; break;
            case 223: codigoPais="TON"; break;
            case 224: codigoPais="TYT"; break;
            case 225: codigoPais="TUN"; break;
            case 226: codigoPais="TKM"; break;
            case 227: codigoPais="TUR"; break;
            case 228: codigoPais="TUV"; break;
            case 229: codigoPais="UCR"; break;
            case 230: codigoPais="UGA"; break;
            case 231: codigoPais="URU"; break;
            case 232: codigoPais="UZB"; break;
            case 233: codigoPais="VUT"; break;
            case 234: codigoPais="VAT"; break;
            case 235: codigoPais="VEN"; break;
            case 236: codigoPais="VIE"; break;
            case 237: codigoPais="YEM"; break;
            case 238: codigoPais="YIB"; break;
            case 239: codigoPais="YUG"; break;
            case 240: codigoPais="ZAM"; break;
            case 241: codigoPais="ZIM"; break;
        }
        return codigoPais;
    }


    public static void eliminaDocumento(TicketDTO ticketDTO) {
        if (null != ticketDTO.getRutaArchivoOnboarding() && !ticketDTO.getRutaArchivoOnboarding().equals("")) {
            BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoOnboarding()));
        }
        if (null != ticketDTO.getRutaArchivoDocumentoElectronico() && !ticketDTO.getRutaArchivoDocumentoElectronico().equals("")) {
            BelmondUtil.eliminarFichero(new File(ticketDTO.getRutaArchivoDocumentoElectronico()));
        }
    }
}

package com.mdp.perurail.dto;


public class DestinoDTO {
    private Integer id;
    private String ruta;
    private String destino;

    public DestinoDTO(Integer id, String destino) {
        this.id = id;
        this.destino = destino;
    }

    public DestinoDTO(Integer id,String destino,String ruta) {
        this.id = id;
        this.destino = destino;
        this.ruta = ruta;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Override
    public String toString() {
        return destino;
    }
}

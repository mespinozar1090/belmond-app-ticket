package com.mdp.perurail.network.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mdp.perurail.network.response.TicketResponse;
import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.utilitary.methods.Constans;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketRespositoryWs {

    private final String LOGGER = "TicketRespositoryWs";
    private APIService apiService;

    private static TicketRespositoryWs instancia;

    public TicketRespositoryWs() {
    }

    public static TicketRespositoryWs getInstance() {
        if (instancia == null) {
            instancia = new TicketRespositoryWs();
        }
        return instancia;
    }

    public List<TicketResponse> listaTicketPorDocumento (String codPais, String tipDoc, String docIde, String tipAcc, Context context){
        List<TicketResponse> listMutableLiveData = new ArrayList<>();
        apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL,context).create(APIService.class);
        Call<List<TicketResponse>> listCall = apiService.getListTicket(codPais,tipDoc,docIde,"",tipAcc,"belmond");

        listCall.enqueue(new Callback<List<TicketResponse>>() {
            @Override
            public void onResponse(Call<List<TicketResponse>> call, Response<List<TicketResponse>> response) {
              if(response.isSuccessful() && response.body() != null){
                  Log.e(LOGGER,""+response.code());
                  listMutableLiveData.addAll(response.body());
              }else{
                  Log.e(LOGGER,""+response.code());
              }

            }

            @Override
            public void onFailure(Call<List<TicketResponse>> call, Throwable t) {
                Log.e(LOGGER, "listaTicketPorDocumento-onFailure"+t.getMessage());
            }
        });
        return listMutableLiveData;
    }

}

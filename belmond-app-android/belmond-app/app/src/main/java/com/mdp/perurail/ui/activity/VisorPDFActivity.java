package com.mdp.perurail.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.fonts.Font;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.mdp.perurail.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class VisorPDFActivity extends AppCompatActivity {

    private PDFView pdfView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor_pdf);
        initView();
    }

    private void initView(){

        String name = getIntent().getStringExtra("name");
        String url = getIntent().getStringExtra("url");
        setToolbar(name);
       // descripcion.setText(name);
        pdfView = (PDFView)findViewById(R.id.pdfView);
        File file = new File(url);
        Log.e("PATH", file.getAbsolutePath());
        pdfView.fromFile(file).load();

      /* String nombreRuta = (url.equals("1")? "boleta_ticket.pdf" : "ticket.pdf");
        pdfView.fromAsset(nombreRuta).load(); */

    }

    private void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTitle(title);
        TextView titulo = (TextView) toolbar.findViewById(R.id.tvDescription);
        titulo.setText(title);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_atras, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

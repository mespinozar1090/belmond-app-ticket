package com.mdp.perurail.ui.fragment.check;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.perurail.R;
import com.mdp.perurail.database.model.Ticket;
import com.mdp.perurail.dto.DinamicDTO;
import com.mdp.perurail.dto.GrupoDTO;
import com.mdp.perurail.dto.NacionalidadDTO;
import com.mdp.perurail.dto.TipoDocumentoDTO;
import com.mdp.perurail.network.controller.TicketController;
import com.mdp.perurail.network.response.TicketResponse;
import com.mdp.perurail.network.service.APIService;
import com.mdp.perurail.ui.activity.CheckTicketActivity;
import com.mdp.perurail.ui.activity.ListaDinamicaActivity;
import com.mdp.perurail.ui.fragment.group.GroupDetailsTicketFragment;
import com.mdp.perurail.utilitary.methods.BelmondUtil;
import com.mdp.perurail.utilitary.methods.Constans;
import com.mdp.perurail.viewModel.MyTicketsViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckTicketDocumentFragment extends Fragment implements View.OnClickListener {

    private static final String OBJECT_GRUPODTO = "grupoDTO";

    private TextInputEditText tietnumerodocumento;
    private TextInputLayout tilnumerodocumento;

    private Button btnBuscar, spNacionalidad, spTipoDocumento;
    private MyTicketsViewModel myTicketsViewModel;
    private TicketController ticketController;
    private GrupoDTO grupoDTO;

    private String codigoPais;
    private String codigoNacionalidad;

    private int OBTENER_PAIS = 90;
    private int OBTENER_DOCUMENTO = 100;

    public static CheckTicketDocumentFragment newInstance(GrupoDTO grupoDTO) {
        CheckTicketDocumentFragment fragment = new CheckTicketDocumentFragment();
        Bundle args = new Bundle();
        args.putSerializable(OBJECT_GRUPODTO, grupoDTO);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            grupoDTO = (GrupoDTO) getArguments().getSerializable(OBJECT_GRUPODTO);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_check_ticket_document, container, false);
        tietnumerodocumento = view.findViewById(R.id.tietnumerodocumento);
        tilnumerodocumento = view.findViewById(R.id.tilnumerodocumento);
        btnBuscar = view.findViewById(R.id.btnBuscar);
        btnBuscar.setOnClickListener(this);
        loadSpinner(view);
        return view;
    }

    private void loadSpinner(View view) {
        spNacionalidad = view.findViewById(R.id.spNacionalidad);
        spTipoDocumento = view.findViewById(R.id.spTipoDocumento);
        spTipoDocumento.setText(R.string.seleccione_documento);
        spNacionalidad.setText(R.string.seleccione_pais);

        spTipoDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListaDinamicaActivity.class);
                intent.putExtra("tipoAccion", 2);
                startActivityForResult(intent, OBTENER_DOCUMENTO);
            }
        });

        spNacionalidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListaDinamicaActivity.class);
                intent.putExtra("tipoAccion", 1);
                startActivityForResult(intent, OBTENER_PAIS);
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myTicketsViewModel = ViewModelProviders.of(this).get(MyTicketsViewModel.class);
    }

    private void consultarTicketWs(String codPais, String tipDoc, String docIde, String tipAcc) {
        BelmondUtil.showProgressDialogBelmond(getActivity(), getString(R.string.validando));
        APIService apiService = BelmondUtil.getRetrofitBuilderToken(Constans.BASE_URL, getContext()).create(APIService.class);
        Call<List<TicketResponse>> listCall = apiService.getListTicket(codPais, tipDoc, docIde, "", tipAcc, "belmond");
        listCall.enqueue(new Callback<List<TicketResponse>>() {
            @Override
            public void onResponse(Call<List<TicketResponse>> call, Response<List<TicketResponse>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Ticket> numerosTickets = new ArrayList<>();
                    if (response.body().size() > 0) {
                        for (Ticket olista : BelmondUtil.castTicketResponseToTickeDB(response.body())) {
                            Ticket data = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), olista.getNumeroTicket());
                            if (null == data) {
                                olista.setIdGrupo(grupoDTO.getId());
                                myTicketsViewModel.insert(olista);
                                numerosTickets.add(olista);
                            }
                        }
                        descargarTickets(numerosTickets);
                        BelmondUtil.hideProgreesDialogBelmond();
                    } else {
                        BelmondUtil.hideProgreesDialogBelmond();
                        tilnumerodocumento.setError(getString(R.string.documeto_invaldo));
                        tietnumerodocumento.requestFocus();
                    }
                } else {
                    BelmondUtil.hideProgreesDialogBelmond();
                    ((CheckTicketActivity) getActivity()).sendData(null);
                }
            }

            @Override
            public void onFailure(Call<List<TicketResponse>> call, Throwable t) {
                BelmondUtil.hideProgreesDialogBelmond();
                Toast.makeText(getActivity(), "Sin conexion con el servidor...", Toast.LENGTH_SHORT).show();
                Log.e("LOGGER", "listaTicketPorDocumento-onFailure" + t.getMessage());
            }
        });
    }

    private void descargarTickets(List<Ticket> numerosTickets) {
        if (numerosTickets.size() > 0) {
            for (Ticket oData : numerosTickets) {
                String nunticket = oData.getNumeroTicket();
                String[] ticketParts = nunticket.split("/");
                String numeroTicketSplit = ticketParts[0];
                if (oData.getRutaArchivoOnboarding() != null && !oData.getRutaArchivoOnboarding().equals("")) {
                    downloadFiledBoarding(numeroTicketSplit, oData);
                }
            }

            ((CheckTicketActivity) getActivity()).sendData(null);
        }
    }

    private void downloadFiledBoarding(String numeroTicketSplit, Ticket oTicket) {
        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoOnboarding());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Boarding-grupo" + grupoDTO.getId() + "-" + numeroTicketSplit);
                    Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), oTicket.getNumeroTicket());
                    oTicketDB.setRutaArchivoOnboarding(writtenToDisk);
                    myTicketsViewModel.update(oTicketDB);
                    if (null != oTicket.getRutaArchivoDocumentoElectronico() && !oTicket.getRutaArchivoDocumentoElectronico().equals("")) {
                        downloadFileDocumentoElectronico(numeroTicketSplit, oTicket);
                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
            }
        });
    }

    private void downloadFileDocumentoElectronico(String numeroTicketSplit, Ticket oTicket) {
        Log.e("DOWNLOAD", " DEBE CAER EN ESTOS DOS " + oTicket.getRutaArchivoDocumentoElectronico());
        APIService apiService = BelmondUtil.getRetrofitBuilder(Constans.BASE_URL).create(APIService.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(oTicket.getRutaArchivoDocumentoElectronico());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (null != response.body()) {
                    String writtenToDisk = BelmondUtil.writeResponseBodyToDisk(response.body(), "Documento-grupo" + grupoDTO.getId() + "-" + numeroTicketSplit);
                    Ticket oTicketDB = myTicketsViewModel.getTicketByIdGrupoAndNumeroticket(grupoDTO.getId(), oTicket.getNumeroTicket());
                    oTicketDB.setRutaArchivoDocumentoElectronico(writtenToDisk);
                    myTicketsViewModel.update(oTicketDB);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ERROR", t.getMessage() + "");
                Toast.makeText(getActivity(), "OCURRIO UN ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OBTENER_PAIS) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    DinamicDTO dinamicDTO = (DinamicDTO) data.getSerializableExtra("object");
                    codigoPais = dinamicDTO.getId();
                    spNacionalidad.setText(dinamicDTO.getNombre());
                }
            }
        } else if (requestCode == OBTENER_DOCUMENTO) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    DinamicDTO dinamicDTO = (DinamicDTO) data.getSerializableExtra("object");
                    codigoNacionalidad = dinamicDTO.getId();
                    spTipoDocumento.setText(dinamicDTO.getNombre());
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBuscar:
                if (!tietnumerodocumento.getText().toString().trim().isEmpty()) {
                    if(!BelmondUtil.validarNumeroDocumento(tietnumerodocumento.getText().toString())){
                        consultarTicketWs(codigoPais, codigoNacionalidad, tietnumerodocumento.getText().toString(), "C");
                    }else{
                        tilnumerodocumento.setError(getString(R.string.caracter_especial));
                        tietnumerodocumento.requestFocus();
                    }
                } else {
                    tilnumerodocumento.setError(getString(R.string.documeto_invaldo));
                    tietnumerodocumento.requestFocus();
                }
                break;
        }
    }
}

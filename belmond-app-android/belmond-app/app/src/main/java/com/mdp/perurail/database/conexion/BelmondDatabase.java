package com.mdp.perurail.database.conexion;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.mdp.perurail.database.dao.GrupoDao;
import com.mdp.perurail.database.dao.TicketDao;
import com.mdp.perurail.database.model.Grupo;
import com.mdp.perurail.database.model.Ticket;

@Database(entities = {Ticket.class,Grupo.class}, version = 1,exportSchema = false)
public abstract class BelmondDatabase extends RoomDatabase {

    public static final String DATABASE_NAME= "belmond_database";
    private static BelmondDatabase instance;

    public abstract TicketDao ticketDao();
    public abstract GrupoDao grupoDao();


    public static synchronized BelmondDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    BelmondDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                   // .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            Log.e("OnCreateDB","entro a crear grupos");
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private GrupoDao grupoDao;
        private TicketDao ticketDao;

        private PopulateDbAsyncTask(BelmondDatabase db) {
            grupoDao = db.grupoDao();
            ticketDao = db.ticketDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ticketDao.insert(new Ticket(1,"24652226/","MARIN ESPINOZA RAMIREZ","CUSCO","EXPEDITION","C","121",
                    "12/05/2401","12:45","M",null,
                    null,"CASA123"));
            return null;
        }
    }
}
